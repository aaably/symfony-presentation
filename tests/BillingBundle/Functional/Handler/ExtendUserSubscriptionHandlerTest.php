<?php

namespace Tests\BillingBundle\Functional\Handler;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;
use Fitatu\ApiClientBundle\Service\UserCacheCleanerService;
use Fitatu\DatabaseBundle\Entity\Auth\Plan;
use Fitatu\DatabaseBundle\Entity\Auth\PlanGroup;
use Fitatu\DatabaseBundle\Entity\Auth\Subscription;
use Fitatu\DatabaseBundle\Entity\Auth\User\User;
use Fitatu\Local\Repository\Auth\SubscriptionRepository;
use Mockery;
use Psr\Log\LoggerInterface;
use Symetria\ApiServerBundle\Tests\AbstractApiWebTestCase;
use Symetria\BillingBundle\Handler\Subscription\ExtendUserSubscriptionCommand;
use Symetria\BillingBundle\Handler\Subscription\ExtendUserSubscriptionHandler;
use Symetria\SecurityBundle\Service\RolesService;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class ExtendUserSubscriptionHandlerTest extends AbstractApiWebTestCase
{
    /**
     * @var EntityManagerInterface|MockInterface
     */
    private $entityManager;

    /**
     * @var SubscriptionRepository|MockInterface
     */
    private $subscriptionRepository;

    /**
     * @var UserCacheCleanerService|MockInterface
     */
    private $userCacheCleanerService;

    /**
     * @var LoggerInterface|MockInterface
     */
    private $logger;

    /**
     * @var RolesService|MockInterface
     */
    private $rolesService;

    /**
     * @var ExtendUserSubscriptionHandler|MockInterface
     */
    private $handler;

    /**
     * @var Connection|MockInterface
     */
    private $connection;

    public function setUp()
    {
        parent::setUp();
        $this->entityManager = Mockery::mock(EntityManagerInterface::class);
        $this->subscriptionRepository = Mockery::spy(SubscriptionRepository::class);
        $this->rolesService = Mockery::mock(RolesService::class);
        $this->logger = Mockery::mock(LoggerInterface::class);
        $this->userCacheCleanerService = Mockery::mock(UserCacheCleanerService::class);
        $this->connection = Mockery::mock(Connection::class);

        $this->handler = new ExtendUserSubscriptionHandler(
            $this->entityManager,
            $this->subscriptionRepository,
            $this->rolesService,
            $this->logger,
            $this->userCacheCleanerService
        );

        $this->getEntityManagerBaseExpectations();
    }

    /**
    * @test
    */
    public function it_it_extends_user_subscription()
    {
        $date = new \DateTime();
        $subscription = Mockery::mock(Subscription::class)->makePartial();
        $planGroup = Mockery::mock(PlanGroup::class)->makePartial();
        $planGroup->shouldReceive('getRoles')
                  ->andReturn(new ArrayCollection());

        $plan = Mockery::mock(Plan::class);
        $plan->shouldReceive('getPlanGroup')
             ->andReturn($planGroup);

        $user = (new User())->setId(1);

        $subscription
            ->shouldReceive([
                'getUser' => $user,
                'getPlan' => $plan
            ]);

        $this->rolesService->shouldReceive('removePremiumRoles');
        $this->rolesService->shouldReceive('assignRoles');
        $this->subscriptionRepository->shouldReceive('persist');

        $this->entityManager
            ->shouldReceive('commit');

        $this->handler->handle(
            new ExtendUserSubscriptionCommand($subscription, $date)
        );
    }


    private function getEntityManagerBaseExpectations()
    {
        $this->entityManager
            ->shouldReceive('getConnection')
            ->andReturn($this->connection);

        $this->connection
            ->shouldReceive('beginTransaction');
    }
}
