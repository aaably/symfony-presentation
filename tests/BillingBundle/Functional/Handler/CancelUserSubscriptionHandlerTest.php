<?php

namespace Tests\BillingBundle\Functional\Handler;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;
use Fitatu\ApiClientBundle\Service\UserCacheCleanerService;
use Fitatu\DatabaseBundle\Entity\Auth\Plan;
use Fitatu\DatabaseBundle\Entity\Auth\PlanGroup;
use Fitatu\DatabaseBundle\Entity\Auth\Subscription;
use Fitatu\DatabaseBundle\Entity\Auth\User\User;
use Fitatu\Local\Repository\Auth\SubscriptionRepository;
use Mockery;
use Mockery\MockInterface;
use Psr\Log\LoggerInterface;
use Symetria\ApiServerBundle\Tests\AbstractApiWebTestCase;
use Symetria\BillingBundle\Handler\Subscription\CancelUserSubscriptionCommand;
use Symetria\BillingBundle\Handler\Subscription\CancelUserSubscriptionHandler;
use Symetria\SecurityBundle\Service\RolesService;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class CancelUserSubscriptionHandlerTest extends AbstractApiWebTestCase
{
    /**
     * @var EntityManagerInterface|MockInterface
     */
    private $entityManager;

    /**
     * @var SubscriptionRepository|MockInterface
     */
    private $subscriptionRepository;

    /**
     * @var UserCacheCleanerService|MockInterface
     */
    private $userCacheCleanerService;

    /**
     * @var LoggerInterface|MockInterface
     */
    private $logger;

    /**
     * @var RolesService|MockInterface
     */
    private $rolesService;

    /**
     * @var CancelUserSubscriptionHandler|MockInterface
     */
    private $handler;

    /**
     * @var Connection|MockInterface
     */
    private $connection;

    public function setUp()
    {
        parent::setUp();
        $this->entityManager = Mockery::mock(EntityManagerInterface::class);
        $this->subscriptionRepository = Mockery::spy(SubscriptionRepository::class);
        $this->rolesService = Mockery::mock(RolesService::class);
        $this->logger = Mockery::mock(LoggerInterface::class);
        $this->userCacheCleanerService = Mockery::mock(UserCacheCleanerService::class);
        $this->connection = Mockery::mock(Connection::class);

        $this->handler = new CancelUserSubscriptionHandler(
            $this->entityManager,
            $this->subscriptionRepository,
            $this->rolesService,
            $this->logger,
            $this->userCacheCleanerService
        );

        $this->getEntityManagerBaseExpectations();
    }

    /**
     * @test
     */
    public function it_cancels_user_subscription()
    {
        $date = new \DateTime();
        $reason = 1;
        $subscription = Mockery::mock(Subscription::class)->makePartial();
        $secondSubscription = Mockery::mock(Subscription::class);

        $user = (new User())->setId(1);
        $planGroup = Mockery::mock(PlanGroup::class)->makePartial();
        $planGroup->shouldReceive('getRoles')
            ->andReturn(new ArrayCollection());

        $plan = Mockery::mock(Plan::class);
        $plan->shouldReceive('getPlanGroup')
             ->andReturn($planGroup);

        $subscription
            ->shouldReceive([
                'getUser' => $user,
                'getId'   => 1,
                'getPlan' => $plan,
            ]);

        $secondSubscription
            ->shouldReceive([
                'getId'   => 2,
                'getPlan' => $plan,
            ]);

        $subscription
            ->shouldReceive('setCancelledAt')
            ->with($date)
            ->andReturn($subscription);
        $subscription
            ->shouldReceive('setCancelReason')
            ->with($reason);

        $this->subscriptionRepository
            ->shouldReceive('findActiveForUser', 1)
            ->andReturn([
                $subscription,
                $secondSubscription,
            ]);

        $this->rolesService->shouldReceive('removePremiumRoles');
        $this->rolesService->shouldReceive('assignRoles');

        $this->entityManager
            ->shouldReceive('commit');

        $this->handler->handle(
            new CancelUserSubscriptionCommand($subscription, $date, $reason)
        );
    }

    /**
     * @test
     */
    public function it_rolls_back_error_transaction()
    {
        $date = new \DateTime();
        $reason = 1;
        $subscription = Mockery::mock(Subscription::class)->makePartial();

        $user = (new User())->setId(1);
        $planGroup = Mockery::mock(PlanGroup::class)->makePartial();
        $planGroup->shouldReceive('getRoles')
                  ->andReturn(new ArrayCollection());

        $plan = Mockery::mock(Plan::class);
        $plan->shouldReceive('getPlanGroup')
             ->andReturn($planGroup);

        $subscription
            ->shouldReceive([
                'getUser' => $user,
                'getId'   => 1,
                'getPlan' => $plan,
            ]);

        $this->connection
            ->shouldReceive('rollBack');

        $this->logger->shouldReceive('critical');

        $this->handler->handle(
            new CancelUserSubscriptionCommand($subscription, $date, $reason)
        );
    }

    private function getEntityManagerBaseExpectations()
    {
        $this->entityManager
            ->shouldReceive('getConnection')
            ->andReturn($this->connection);

        $this->connection
            ->shouldReceive('beginTransaction');
    }
}
