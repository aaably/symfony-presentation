<?php

namespace Tests\BillingBundle\Repository;


use Fitatu\BillingBundle\DataFixtures\ORM\PlanFixtures;
use Fitatu\BillingBundle\DataFixtures\ORM\PlanGroupFixtures;
use Fitatu\DatabaseBundle\Entity\Auth\Plan;
use Fitatu\DatabaseBundle\Entity\Auth\PlanGroup;
use Fitatu\Local\Repository\Auth\PlanGroupRepository;
use Fitatu\Local\Repository\Auth\PlanRepository;
use Symetria\ApiServerBundle\Tests\AbstractApiWebTestCase;
use Symetria\SharedBundle\Mapper\UserSystemMapper;
use Symetria\UserBundle\DataFixtures\ORM\User\RoleFixtures;
use Symetria\UserBundle\DataFixtures\ORM\User\UserFixtures;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class PlanRepositoryTest extends AbstractApiWebTestCase
{
    const LOCALE = 'pl_PL';
    const PROVIDER = UserSystemMapper::IOS_ID;
    /**
     * @var PlanRepository
     */
    private $planRepository;

    /**
     * @var
     */
    private $planGroupRepository;

    public function setUp()
    {
        parent::setUp();
        $this->loadFixtures([
            RoleFixtures::class,
            UserFixtures::class,
            PlanGroupFixtures::class,
            PlanFixtures::class,
        ]);

        $this->planRepository = $this->getContainer()->get(PlanRepository::class);
        $this->planGroupRepository = $this->getContainer()->get(PlanGroupRepository::class);
    }

    /**
    * @test
    */
    public function it_finds_one_by_group_and_locale()
    {
        $planGroup = $this->planGroupRepository->findOneBy([
            'name' => '1 Monthly Diet'
        ]);
        $this->assertInstanceOf(PlanGroup::class, $planGroup);

        $plan = $this->planRepository->findOneByGroupAndLocale(
            $planGroup->getId(),
            static::LOCALE
        );

        $this->assertInstanceOf(Plan::class, $plan);
        $this->assertEquals(9900, $plan->getAmount());
    }

    /**
    * @test
    */
    public function it_finds_plans_by_provider_and_locale()
    {
        $plans = $this->planRepository->getByLocaleAndProvider(
            static::LOCALE,
            static::PROVIDER,
            PlanGroup::TYPE_DIET
        );

        $this->assertCount(2, $plans);
        /** @var Plan $longest */
        $shortest = array_pop($plans);

        $this->assertEquals('1 Monthly Diet', $shortest->getPlanGroup()->getName());
        $this->assertEquals(9900, $shortest->getAmount());
    }
}
