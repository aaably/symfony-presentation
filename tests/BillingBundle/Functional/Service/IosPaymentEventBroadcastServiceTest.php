<?php

namespace BillingBundle\Functional\Service;

use Fitatu\ApiServerBundle\Util\Env;
use GuzzleHttp\Client;
use Mockery\MockInterface;
use Psr\Log\LoggerInterface;
use Symetria\ApiServerBundle\Tests\AbstractApiWebTestCase;
use Symetria\BillingBundle\Service\IosPaymentEventBroadcastService;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class IosPaymentEventBroadcastServiceTest extends AbstractApiWebTestCase
{
    private const ENDPOINT_MASTER = 'mas.fitatu.com';
    private const ENDPOINT_DEVELOP = 'dev.fitatu.com';
    private const ENDPOINTS = self::ENDPOINT_MASTER."|".self::ENDPOINT_DEVELOP;
    private const PARAMETERS_ARRAY = [
        'receipt' => 'paragon',
    ];

    /**
     * @var Client|MockInterface
     */
    private $guzzleClient;

    /**
     * @var IosPaymentEventBroadcastService|MockInterface
     */
    private $service;

    /**
     * @var LoggerInterface|MockInterface
     */
    private $logger;

    public function setUp()
    {
        parent::setUp();

        $this->guzzleClient = \Mockery::mock(Client::class)->makePartial();
        $this->logger = \Mockery::mock(LoggerInterface::class)->makePartial();
        $this->service = $this->getService();
    }

    /**
     * @return IosPaymentEventBroadcastService
     */
    public function getService(): IosPaymentEventBroadcastService
    {
        return new IosPaymentEventBroadcastService(
            $this->guzzleClient,
            $this->logger
        );
    }

    /**
     * @test
     */
    public function it_guards_broadcasting()
    {
        $this->assertEquals(IosPaymentEventBroadcastService::APP_ENV_DEFAULT_VALUE, Env::get(IosPaymentEventBroadcastService::APP_ENV_KEY));
        $this->assertFalse($this->service->shouldBroadcast());

        Env::put(IosPaymentEventBroadcastService::APP_ENV_KEY, IosPaymentEventBroadcastService::APP_ENV_APPROVED_VALUE);
        $this->service = $this->getService();
        $this->assertTrue($this->service->shouldBroadcast());
        $this->assertEquals(IosPaymentEventBroadcastService::APP_ENV_APPROVED_VALUE, Env::get(IosPaymentEventBroadcastService::APP_ENV_KEY));
    }

    /**
     * @test
     */
    public function it_converts_string_endpoints_to_array()
    {
        $this->assertInternalType('array', $this->service->getEndpoints());
        $this->assertEmpty($this->service->getEndpoints());

        Env::put(IosPaymentEventBroadcastService::APP_ENV_ENDPOINTS_KEY, static::ENDPOINTS);

        $this->assertInternalType('array', $this->service->getEndpoints());
        $this->assertCount(2, $this->service->getEndpoints());
    }

    /**
     * @test
     */
    public function it_broadcasts_events()
    {
        $this->markTestSkipped();
        Env::put(IosPaymentEventBroadcastService::APP_ENV_ENDPOINTS_KEY, static::ENDPOINTS);

        $this->guzzleClient
            ->shouldReceive('postAsync')
            ->with(static::ENDPOINT_MASTER, static::PARAMETERS_ARRAY);

        $this->guzzleClient
            ->shouldReceive('postAsync')
            ->with(static::ENDPOINT_DEVELOP, static::PARAMETERS_ARRAY);

        $this->service->broadcast(static::PARAMETERS_ARRAY);
    }
}