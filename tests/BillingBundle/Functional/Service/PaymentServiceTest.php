<?php

namespace Tests\BillingBundle\Functional\Service;

use Cassandra\Session as Client;
use Fitatu\BillingBundle\DataFixtures\ORM\PlanFixtures;
use Fitatu\BillingBundle\DataFixtures\ORM\PlanGroupFixtures;
use Fitatu\BillingBundle\Exception\TransactionDetailsNotProvidedException;
use Fitatu\BillingBundle\Exception\TransactionTypeNotProvidedExeption;
use Fitatu\BillingBundle\Service\PaymentService;
use Fitatu\BillingBundle\Service\SubscriptionService;
use Fitatu\Cassandra\QueryBuilder;
use Fitatu\Local\Repository\Auth\User\UserRepository;
use Fitatu\SharedUserBundle\Exception\UserNotFoundException;
use Fitatu\SharedUserBundle\Exception\UserNotSetException;
use Symetria\ApiServerBundle\Tests\AbstractApiWebTestCase;
use Symetria\UserBundle\DataFixtures\ORM\User\RoleFixtures;
use Symetria\UserBundle\DataFixtures\ORM\User\UserFixtures;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class PaymentServiceTest extends AbstractApiWebTestCase
{
    const USER_ID = 1;
    const VALID_PAYMENT_PAYLOAD = [
        'id' => 'uniqueProviderId',
        'transaction' => [
            'type' =>  'android-playstore'
        ],
        'price' => 12300000, /// in micros
        'vaid' => true
    ];
    const PLAN_DURATION = PlanGroupFixtures::PLAN_DURATION;
    const INVALID_PAYMENT_PAYLOAD = [
        'id' => 'sss',
        'transaction' => [
            'type' =>  'android-playstore'
        ],
        'vaid' => true
    ];

    /**
     * @var Client
     */
    protected $cassandra;

    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * @var SubscriptionService
     */
    protected $subscriptionService;

    /**
     * @var PaymentService
     */
    protected $paymentService;

    /**
     * @var Client
     */
    protected $client;

    public function setUp()
    {
        parent::setUp();

        $this->userRepository = $this->getContainer()->get(UserRepository::class);
        $this->subscriptionService = $this->getContainer()->get('fitatu.billing.subscription_service');
        $this->paymentService = $this->getContainer()->get('fitatu.billing.payment');

        $this->client = $this->getMockBuilder(Client::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->cassandra = new QueryBuilder($this->client);

        $this->loadFixtures([
            RoleFixtures::class,
            UserFixtures::class,
            PlanGroupFixtures::class,
            PlanFixtures::class,
        ]);
    }

    /**
    * @test
    */
    public function it_sets_related_users_transaction_and_productId()
    {
        $this->expectException(UserNotFoundException::class);
        $this->paymentService->setUser(11000000000);

        $this->markTestIncomplete('Brak danych z systemu platności');
    }

    /**
    * @test
    */
    public function it_guards_validation_process()
    {
        $this->expectException(UserNotSetException::class);
        $this->paymentService->validate();

        $this->paymentService->setUser(UserFixtures::USER_ID);
        $this->expectException(TransactionDetailsNotProvidedException::class);
        $this->paymentService->validate();

        $this->paymentService->setTransaction('test');
        $this->expectException(TransactionTypeNotProvidedExeption::class);
        $this->paymentService->validate();

        $this->paymentService->setTransaction(['type' => 'android-playstore' ]);
        $this->paymentService->validate();
    }
}
