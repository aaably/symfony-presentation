<?php

namespace Tests\BillingBundle\Functional\AMQPConsumer;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;
use Fitatu\ApiClientBundle\Service\UserCacheCleanerService;
use Fitatu\BillingBundle\Service\SubscriptionService;
use Fitatu\DatabaseBundle\Entity\Auth\Plan;
use Fitatu\DatabaseBundle\Entity\Auth\PlanGroup;
use Fitatu\DatabaseBundle\Entity\Auth\Role;
use Fitatu\DatabaseBundle\Entity\Auth\Subscription;
use Fitatu\DatabaseBundle\Entity\Auth\User\User;
use Fitatu\Local\Repository\Auth\PaymentRepository;
use Fitatu\Local\Repository\Auth\PlanRepository;
use Fitatu\Local\Repository\Auth\Settings\DietGeneratorRepository;
use Fitatu\Local\Repository\Auth\Settings\SettingsPremiumRepository;
use Fitatu\Local\Repository\Auth\SubscriptionRepository;
use Fitatu\Local\Repository\Auth\User\UserRepository;
use Fitatu\SharedUserBundle\Model\Role\RoleInterface;
use Mockery;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;
use Mockery\MockInterface;
use Monolog\Logger;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use Symetria\ApiServerBundle\Tests\AbstractApiWebTestCase;
use Symetria\BillingBundle\AMQPConsumer\SubscribeUserToPlanConsumer;
use Symetria\SecurityBundle\Service\RoleHierarchyService;
use Symetria\SecurityBundle\Service\RolesService;
use Symetria\SharedBundle\Event\Subscription\SubscribeUserToPlanEvent;
use Symetria\UserBundle\Service\SettingsPremiumService;

/**
 * @author    Sebastian Błaszczak
 * @copyright Fitatu Sp. z o.o.
 */
class SubscribeUserToPlanConsumerTest extends AbstractApiWebTestCase
{
    use MockeryPHPUnitIntegration;

    /**
     * @var SubscribeUserToPlanConsumer|MockInterface
     */
    private $consumerMock;

    /**
     * @var PaymentRepository|MockInterface
     */
    private $paymentRepositoryMock;

    /**
     * @var SubscriptionService|MockInterface
     */
    private $subscriptionServiceMock;

    /**
     * @var UserCacheCleanerService|MockInterface
     */
    private $userCacheCleanerServiceMock;

    /**
     * @var UserRepository|MockInterface
     */
    private $userRepositoryMock;

    /**
     * @var PlanRepository|MockInterface
     */
    private $planRepositoryMock;

    /**
     * @var Logger|MockInterface
     */
    private $loggerMock;

    /**
     * @var EntityManagerInterface|MockInterface
     */
    private $entityManagerInterfaceMock;

    /**
     * @var Connection|MockInterface
     */
    private $connectionMock;

    /**
     * @var DietGeneratorRepository|MockInterface
     */
    private $dietGeneratorRepositoryMock;

    /**
     * @var SettingsPremiumRepository|MockInterface
     */
    private $settingsPremiumService;

    /**
     * @var RolesService|MockInterface
     */
    private $rolesService;

    /**
     * @var RoleHierarchyService|MockInterface
     */
    private $roleHierarchyService;

    /**
     * @var SubscriptionRepository
     */
    private $subscriptionRepositoryMock;

    /**
     * {@inheritDoc}
     */
    public function setUp()
    {
        parent::setUp();
        $this->paymentRepositoryMock = Mockery::mock(PaymentRepository::class);
        $this->userRepositoryMock = Mockery::mock(UserRepository::class);
        $this->planRepositoryMock = Mockery::mock(PlanRepository::class);
        $this->subscriptionServiceMock = Mockery::mock(SubscriptionService::class);
        $this->subscriptionRepositoryMock = $this->getSubscriptionRepositoryMock();
        $this->userCacheCleanerServiceMock = Mockery::mock(UserCacheCleanerService::class);
        $this->loggerMock = Mockery::mock(Logger::class);
        $this->connectionMock = $this->getConnectionMock();
        $this->roleHierarchyService = Mockery::mock(RoleHierarchyService::class);
        $this->entityManagerInterfaceMock = $this->getEntityManagerInterfaceMock();
        $this->dietGeneratorRepositoryMock = $this->getDietGeneratorMock();
        $this->settingsPremiumService = $this->getSettingsPremiumServiceMock();
        $this->rolesService = Mockery::mock(RolesService::class);
        $this->consumerMock = $this->getConsumerMock();
    }

    /**
     * @test
     */
    public function it_executes_method_run()
    {
        $consumerMock = $this->getConsumerMock();
        $consumerMock->shouldReceive('run')->once();
        $AMQPMessageMock = Mockery::mock(AMQPMessage::class);

        $consumerMock->run($AMQPMessageMock);
    }

    /**
     * @test
     */
    public function it_rejects_when_message_broken()
    {
        $consumerMock = $this->getConsumerMock()->makePartial();

        $AMQPMessageMock = Mockery::mock(AMQPMessage::class);
        $AMQPMessageMock->shouldReceive('getBody')->andReturn('"DateTime":0');

        $this->assertSame(ConsumerInterface::MSG_REJECT, $consumerMock->run($AMQPMessageMock));
    }

    /**
     * @test
     */
    public function it_subscribe_user_to_plan_with_diet_generator()
    {
        $this->subscribe(function () {
            $this->roleHierarchyService->shouldReceive('roleContains')->andReturn(true);

            $this->dietGeneratorRepositoryMock->shouldReceive('createOrReset')->once();
        });
    }

    /**
     * @test
     */
    public function it_sets_matching_process_to_true_when_needed()
    {
        $this->subscribe(function () {
            $this->roleHierarchyService->shouldReceive('roleContains')->andReturn(true);
            $this->dietGeneratorRepositoryMock->shouldReceive('createOrReset')->once();

            $consumerMock = $this->getConsumerMock()->shouldAllowMockingProtectedMethods()->makePartial();
            $consumerMock->shouldReceive('shouldShowMatchingProcess')
                         ->with([], [RoleInterface::ROLE_PREMIUM])
                         ->andReturn(true);
        });

        $this->subscribe(function () {
            $this->roleHierarchyService->shouldReceive('roleContains')->andReturn(true);
            $this->dietGeneratorRepositoryMock->shouldReceive('createOrReset')->once();

            $consumerMock = $this->getConsumerMock()->shouldAllowMockingProtectedMethods()->makePartial();
            $consumerMock->shouldReceive('shouldShowMatchingProcess')
                         ->with(new ArrayCollection([RoleInterface::ROLE_PREMIUM_WITHOUT_DIET]), new ArrayCollection([RoleInterface::ROLE_PREMIUM]))
                         ->andReturn(true);
        });
    }

    /**
     * @test
     */
    public function it_sets_matching_process_to_false_when_needed()
    {
        $this->subscribe(function () {
            $this->roleHierarchyService->shouldReceive('roleContains')->andReturn(true);
            $this->dietGeneratorRepositoryMock->shouldReceive('createOrReset')->once();

            $consumerMock = $this->getConsumerMock()->shouldAllowMockingProtectedMethods()->makePartial();
            $consumerMock->shouldReceive('shouldShowMatchingProcess')
                         ->with(new ArrayCollection([RoleInterface::ROLE_PREMIUM]), new ArrayCollection([RoleInterface::ROLE_PREMIUM]))
                         ->andReturn(false);
        });
    }

    /**
     * @test
     */
    public function it_does_not_create_diet_getnerator_entity_if_not_needed()
    {
        $this->subscribe(function () {
            $this->roleHierarchyService->shouldReceive('roleContains')->andReturn(false);

            $this->dietGeneratorRepositoryMock->shouldReceive('createOrReset')->never();
        });
    }

    /**
     * @test
     */
    public function it_will_not_subscribe_user_to_plan_because_of_user_load_error()
    {
        /** @var SubscribeUserToPlanConsumer|MockInterface $consumerMock */
        $consumerMock = $this->getConsumerMock()->shouldAllowMockingProtectedMethods()->makePartial();
        $consumerMock->shouldReceive('loadUser')->andThrow(new \Exception('User load error'));

        $this->loggerMock->shouldReceive('critical')->once();

        /** @var AMQPMessage|MockInterface $AMQPMessageMock */
        $AMQPMessageMock = Mockery::mock(AMQPMessage::class);
        $AMQPMessageMock->shouldReceive('getBody')->andReturn(serialize($this->getSubscribeUserToPlanEvent()));

        $this->assertSame(ConsumerInterface::MSG_ACK, $consumerMock->run($AMQPMessageMock));
    }

    /**
     * @test
     */
    public function it_will_not_subscribe_user_to_plan_because_of_plan_load_error()
    {
        /** @var SubscribeUserToPlanConsumer|MockInterface $consumerMock */
        $consumerMock = $this->getConsumerMock()->shouldAllowMockingProtectedMethods()->makePartial();
        $consumerMock->shouldReceive('loadPlan')->andThrow(new \Exception('Plan load error'));

        /** @var User|MockInterface $userMock */
        $userMock = Mockery::mock(User::class)->makePartial();
        $userMock->shouldReceive('getId')->andReturn(999999);
        $consumerMock->shouldReceive('loadUser')->andReturn($userMock);

        $this->loggerMock->shouldReceive('critical')->once();

        /** @var AMQPMessage|MockInterface $AMQPMessageMock */
        $AMQPMessageMock = Mockery::mock(AMQPMessage::class);
        $AMQPMessageMock->shouldReceive('getBody')->andReturn(serialize($this->getSubscribeUserToPlanEvent()));

        $this->assertSame(ConsumerInterface::MSG_ACK, $consumerMock->run($AMQPMessageMock));
    }

    /**
     * @test
     */
    public function it_will_not_subscribe_user_to_plan_because_of_recording_payment_error()
    {
        /** @var SubscribeUserToPlanConsumer|MockInterface $consumerMock */
        $consumerMock = $this->getConsumerMock()->shouldAllowMockingProtectedMethods()->makePartial();
        $consumerMock->shouldReceive('recordPayment')->andThrow(new \Exception('Recording payment error'));

        /** @var User|MockInterface $userMock */
        $userMock = Mockery::mock(User::class)->makePartial();
        $userMock->shouldReceive('getId')->andReturn(999999);
        $consumerMock->shouldReceive('loadUser')->andReturn($userMock);

        /** @var Plan|MockInterface $planMock */
        $planMock = Mockery::mock(Plan::class)->makePartial();
        $planMock->shouldReceive('getId')->andReturn(888888);
        $planMock->shouldReceive('getName');
        $planMock->shouldReceive('getPlanGroup')->andReturn($this->getPlanGroupMock());
        $consumerMock->shouldReceive('loadPlan')->andReturn($planMock);

        $this->loggerMock->shouldReceive('critical')->once();

        /** @var AMQPMessage|MockInterface $AMQPMessageMock */
        $AMQPMessageMock = Mockery::mock(AMQPMessage::class);
        $AMQPMessageMock->shouldReceive('getBody')->andReturn(serialize($this->getSubscribeUserToPlanEvent()));

        $this->assertSame(ConsumerInterface::MSG_ACK, $consumerMock->run($AMQPMessageMock));
    }

    /**
     * @test
     */
    public function it_will_not_subscribe_user_to_plan_because_of_subscribing_service_error()
    {
        /** @var SubscribeUserToPlanConsumer|MockInterface $consumerMock */
        $consumerMock = $this->getConsumerMock()->shouldAllowMockingProtectedMethods()->makePartial();
        $this->subscriptionServiceMock->shouldReceive('subscribeUserToPlan')->andThrow(
            new \Exception('Subscribing service error')
        );

        /** @var User|MockInterface $userMock */
        $userMock = Mockery::mock(User::class)->makePartial();
        $userMock->shouldReceive('getId')->andReturn(999999);
        $consumerMock->shouldReceive('loadUser')->andReturn($userMock);

        /** @var Plan|MockInterface $planMock */
        $planMock = Mockery::mock(Plan::class)->makePartial();
        $planMock->shouldReceive('getId')->andReturn(888888);
        $planMock->shouldReceive('getName');
        $planMock->shouldReceive('getPlanGroup')->andReturn($this->getPlanGroupMock());
        $consumerMock->shouldReceive('loadPlan')->andReturn($planMock);

        /** @var AMQPMessage|MockInterface $AMQPMessageMock */
        $AMQPMessageMock = Mockery::mock(AMQPMessage::class);
        $AMQPMessageMock->shouldReceive('getBody')->andReturn(serialize($this->getSubscribeUserToPlanEvent()));

        $this->loggerMock->shouldReceive('critical')->once();

        /** @var AMQPMessage|MockInterface $AMQPMessageMock */
        $AMQPMessageMock = Mockery::mock(AMQPMessage::class);
        $AMQPMessageMock->shouldReceive('getBody')->andReturn(serialize($this->getSubscribeUserToPlanEvent()));

        $this->assertSame(ConsumerInterface::MSG_ACK, $consumerMock->run($AMQPMessageMock));
    }

    /**
     * @param callable $callback
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function subscribe(callable $callback)
    {
        /** @var SubscribeUserToPlanConsumer|MockInterface $consumerMock */
        $consumerMock = $this->getConsumerMock()->shouldAllowMockingProtectedMethods()->makePartial();
        $consumerMock->shouldReceive('consoleLog')->once();

        /** @var User|MockInterface $userMock */
        $userMock = Mockery::mock(User::class)->makePartial();
        $userMock->shouldReceive('getId')->andReturn(999999);
        $userMock->shouldReceive('getLocale')->andReturn('pl_PL');

        $consumerMock->shouldReceive('loadUser')->andReturn($userMock);

        $planGroupMock = Mockery::mock(PlanGroup::class)->makePartial();
        $planGroupMock->shouldReceive('getRoles')->andReturn(new ArrayCollection([
            new Role(RoleInterface::ROLE_PREMIUM),
        ]));

        /** @var Plan|MockInterface $planMock */
        $planMock = Mockery::mock(Plan::class)->makePartial();
        $planMock->shouldReceive('getId')->andReturn(888888);
        $planMock->shouldReceive('getName');
        $planMock->shouldReceive('getPlanGroup')->andReturn($planGroupMock);
        $consumerMock->shouldReceive('loadPlan')->andReturn($planMock);

        /** @var AMQPMessage|MockInterface $AMQPMessageMock */
        $AMQPMessageMock = Mockery::mock(AMQPMessage::class);
        $AMQPMessageMock->shouldReceive('getBody')->andReturn(serialize($this->getSubscribeUserToPlanEvent()));
        $this->paymentRepositoryMock->shouldReceive('create')->once();

        /** @var Subscription|MockInterface $subscriptionMock */
        $subscriptionMock = Mockery::mock(Subscription::class)->makePartial();
        $subscriptionMock->shouldReceive([
            'getId'   => 123,
            'getPlan' => $planMock,
        ]);
        $this->subscriptionServiceMock->shouldReceive('subscribeUserToPlan')->andReturn($subscriptionMock)->once();

        $this->loggerMock->shouldReceive('critical')->never();
        $this->rolesService->shouldReceive('removePremiumRoles')->once();
        $this->rolesService->shouldReceive('assignRoles')->once();

        $callback();

        $this->userCacheCleanerServiceMock->shouldReceive('flush')->once();

        $this->entityManagerInterfaceMock->shouldReceive('commit');
        $this->assertSame(ConsumerInterface::MSG_ACK, $consumerMock->run($AMQPMessageMock));
    }

    /**
     * @return MockInterface
     */
    public function getEntityManagerInterfaceMock(): MockInterface
    {
        $this->entityManagerInterfaceMock = Mockery::mock(EntityManagerInterface::class);
        $this->entityManagerInterfaceMock->shouldReceive('getConnection')->andReturn($this->connectionMock);

        return $this->entityManagerInterfaceMock;
    }

    /**
     * @return MockInterface
     */
    public function getSubscriptionRepositoryMock(): MockInterface
    {
        $subscriptionRepositoryMock = Mockery::mock(SubscriptionRepository::class);
        $subscriptionRepositoryMock->shouldReceive('findActiveForUser');

        return $subscriptionRepositoryMock;
    }

    /**
     * @return MockInterface
     */
    public function getPlanGroupMock(): MockInterface
    {
        $planGroupMock = Mockery::mock(PlanGroup::class)->makePartial();
        $planGroupMock->shouldReceive('getRoles')->andReturn(new ArrayCollection());

        return $planGroupMock;
    }

    /**
     * @return MockInterface
     */
    public function getDietGeneratorMock(): MockInterface
    {
        $dietGeneratorRepositoryMock = Mockery::mock(DietGeneratorRepository::class);

        return $dietGeneratorRepositoryMock;
    }

    /**
     * @return MockInterface
     */
    public function getSettingsPremiumServiceMock(): MockInterface
    {
        $settingsRepositoryMock = Mockery::mock(SettingsPremiumService::class);
        $settingsRepositoryMock->shouldReceive('update');

        return $settingsRepositoryMock;
    }

    /**
     * @return MockInterface
     */
    public function getConnectionMock(): MockInterface
    {
        $connectionMock = Mockery::mock(Connection::class);
        $connectionMock->shouldReceive('rollBack');
        $connectionMock->shouldReceive('beginTransaction');
        $connectionMock->shouldReceive('commit');

        return $connectionMock;
    }

    /**
     * @return MockInterface
     */
    public function getConsumerMock(): MockInterface
    {
        return Mockery::mock(
            SubscribeUserToPlanConsumer::class,
            [
                $this->paymentRepositoryMock,
                $this->userRepositoryMock,
                $this->planRepositoryMock,
                $this->subscriptionServiceMock,
                $this->subscriptionRepositoryMock,
                $this->userCacheCleanerServiceMock,
                $this->entityManagerInterfaceMock,
                $this->loggerMock,
                $this->dietGeneratorRepositoryMock,
                $this->settingsPremiumService,
                $this->rolesService,
                $this->roleHierarchyService,
            ]
        );
    }

    /**
     * @return SubscribeUserToPlanEvent
     */
    public function getSubscribeUserToPlanEvent(): SubscribeUserToPlanEvent
    {
        return new SubscribeUserToPlanEvent(
            999999,
            888888,
            collect(
                [
                    'id'          => 'diet_one_month',
                    'transaction' => [
                        'type' => 'android-playstore',
                        'id'   => 'TEST_TRANSACTION'.rand(0, 99999999),
                    ],
                    'price'       => 1,
                    'vaid'        => true,
                    'currency'    => 'EUR',
                ]
            )
        );
    }
}