<?php

namespace Tests\BillingBundle\Functional\Controller;

use Symetria\ApiClientBundle\Util\UriGenerator\Billing\PremiumLocalesUriGenerator;
use Symetria\ApiServerBundle\Tests\AbstractApiWebTestCase;
use Symetria\SharedBundle\Mapper\PremiumLocaleMapper;
use Symetria\UserBundle\DataFixtures\ORM\User\RoleFixtures;
use Symetria\UserBundle\DataFixtures\ORM\User\UserFixtures;
use Symfony\Component\HttpFoundation\Response;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class LocalesControllerTest extends AbstractApiWebTestCase
{
    const USER_ID = 1;

    public function setUp()
    {
        parent::setUp();

        $this->loadFixtures(
            [
                RoleFixtures::class,
                UserFixtures::class,
            ]
        );
    }

    /**
     * @test
     */
    public function it_gets_a_list_of_available_locales()
    {
        $client = $this->createFakeAuthenticatedClient(static::USER_ID);
        // older version (in tests we have v1), fallback locale should be taken
        $client->request(
            'GET',
            (new PremiumLocalesUriGenerator())->get(),
            [
                'version' => '2.3.12',
            ]
        );
        $response = $client->getResponse();
        $response = json_decode($response->getContent(), true);

        $locales = PremiumLocaleMapper::get(PremiumLocaleMapper::FALLBACK_VERSION); // TODO Seba Sz.

        $this->assertCount(count($locales), $response);

        // current version
        $version = '2.4.17';
        $client->request(
            'GET',
            (new PremiumLocalesUriGenerator())->get(),
            [
                'version' => $version,
            ]
        );
        $response = $client->getResponse();
        $this->assertJsonResponse($response, Response::HTTP_OK, true);
        $response = json_decode($response->getContent(), true);

        $locales = PremiumLocaleMapper::get($version);

        $this->assertCount(count($locales), $response);
    }
}
