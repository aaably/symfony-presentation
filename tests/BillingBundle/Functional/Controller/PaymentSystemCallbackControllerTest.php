<?php

namespace Tests\BillingBundle\Functional\Controller;

use Symetria\ApiClientBundle\Util\UriGenerator\Billing\BillingCallbackUriGenerator;
use Symetria\ApiClientBundle\Util\UriGenerator\Billing\BillingLogUriGenerator;
use Symetria\ApiServerBundle\Tests\AbstractApiWebTestCase;
use Fitatu\BillingBundle\DataFixtures\ORM\PlanFixtures;
use Fitatu\BillingBundle\DataFixtures\ORM\PlanGroupFixtures;
use Symetria\UserBundle\DataFixtures\ORM\User\RoleFixtures;
use Symetria\UserBundle\DataFixtures\ORM\User\UserFixtures;
use Symfony\Component\HttpFoundation\Response;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class PaymentSystemCallbackControllerTest extends AbstractApiWebTestCase
{
    const USER_ID = 1;

    const VALID_PAYLOAD = [
        'id'          => 'testowy-produkt',
        'additionalData' => [
            'developerPayload' => self::USER_ID
        ],  
        'alias'       => 'test',
        'transaction' => [
            'type' => 'android-playstore'
        ],
        'price'       => 12334,
        'valid'       => true
    ];

    const CORRUPTED_PAYLOAD = [
        'id'          => 'testowy-produkt',
        'additionalData' => [
            'developerPayload' => 1234
        ],
        'alias'       => 'test',
        'transaction' => [
            'type' => 'android-playstore'
        ],
        'price'       => 12334,
        'valid'       => true
    ];

    const NULL_TRANSACTION_PAYLOAD = [
        'id'          => 'testowy-produkt',
        'alias'       => 'test',
        'transaction' => null,
        'price'       => 12334,
        'valid'       => true
    ];

    public function setUp()
    {
        parent::setUp();
        $this->loadFixtures([
            RoleFixtures::class,
            UserFixtures::class,
            PlanGroupFixtures::class,
            PlanFixtures::class,
        ]);
    }

    /**
     * @test
     */
    public function it_access_opened_endpoint_for_payment_system_callbacks()
    {
        $client = static::createClient();
        $client->request('POST', (new BillingCallbackUriGenerator(static::USER_ID))->get());
        $response = $client->getResponse();

        $this->assertJsonResponse($response, Response::HTTP_FORBIDDEN, true);
    }

    /**
     * @test
     */
    public function it_does_not_let_a_random_request_to_pass_validation()
    {
        $response = $this->sendPostRequestWithValidPayload(
            (new BillingCallbackUriGenerator(static::USER_ID))->get()
        );

        $this->assertJsonResponse($response, Response::HTTP_OK, true);
        $response = json_decode($response->getContent(), true);

        $this->assertArrayHasKey('ok', $response);
        $this->assertFalse($response['ok']);
    }

    /**
    * @test
    */
    public function it_checks_if_received_user_id_is_equal_to_provided_user_id()
    {
        $client = $this->createFakeAuthenticatedClient(static::USER_ID);
        $client->request('POST',
            (new BillingCallbackUriGenerator(static::USER_ID))->get(),
            static::CORRUPTED_PAYLOAD
        );
        $response = $client->getResponse();

        $this->assertJsonResponse($response, Response::HTTP_FORBIDDEN, true);
    }

    /**
     * @test
     */
    public function it_logs_transaction_details()
    {
        $response = $this->sendPostRequestWithValidPayload(
            (new BillingLogUriGenerator(static::USER_ID))->get()
        );

        $this->assertJsonResponse($response, Response::HTTP_OK, true);
        $response = json_decode($response->getContent(), true);

        $this->assertEmpty($response);
    }

    /**
    * @test
    */
    public function it_logs_cancelled_transactions()
    {
        $client = $this->createFakeAuthenticatedClient(static::USER_ID);
        $client->request('POST',
            (new BillingLogUriGenerator(static::USER_ID))->get(),
            static::NULL_TRANSACTION_PAYLOAD
        );
        $response = $client->getResponse();

        $this->assertJsonResponse($response, Response::HTTP_OK, true);
        $response = json_decode($response->getContent(), true);

        $this->assertEmpty($response);
    }

    /**
     * @param string $url
     * @return Response
     */
    private function sendPostRequestWithValidPayload(string $url): Response
    {
        $client = $this->createFakeAuthenticatedClient(static::USER_ID);
        $client->request('POST',
            $url,
            static::VALID_PAYLOAD
        );
        return $client->getResponse();
    }
}
