<?php

namespace Tests\BillingBundle\Functional\Controller;

use Fitatu\BillingBundle\DataFixtures\ORM\PlanFixtures;
use Fitatu\BillingBundle\DataFixtures\ORM\PlanGroupFixtures;
use Fitatu\BillingBundle\DataFixtures\ORM\SubscriptionFixtures;
use Fitatu\BillingBundle\Service\PlanFormatter;
use Fitatu\Local\Repository\Auth\PlanRepository;
use Symetria\ApiClientBundle\Util\UriGenerator\Billing\BillingPlansUriGenerator;
use Symetria\ApiClientBundle\Util\UriGenerator\Billing\BillingPlanUriGenerator;
use Symetria\ApiServerBundle\Tests\AbstractApiWebTestCase;
use Symetria\UserBundle\DataFixtures\ORM\User\RoleFixtures;
use Symetria\UserBundle\DataFixtures\ORM\User\UserFixtures;
use Symfony\Component\HttpFoundation\Response;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 * @group     functional
 */
class PlansControllerTest extends AbstractApiWebTestCase
{
    const USER_ID = 1;
    const PLAN_GROUP_ID = 1;


    /**
     * @var PlanRepository
     */
    private $planRepository;

    public function setUp()
    {
        parent::setUp();

        $this->loadFixtures([
            RoleFixtures::class,
            UserFixtures::class,
            PlanGroupFixtures::class,
            PlanFixtures::class,
            SubscriptionFixtures::class,
        ]);

        $this->planRepository = $this->getContainer()->get(PlanRepository::class);
    }

    /**
    * @test
    */
    public function it_gets_a_list_of_available_plans()
    {
        $client = $this->createFakeAuthenticatedClient(static::USER_ID);

        $client->request('GET', (new BillingPlansUriGenerator())->get(), [
            'locale' => 'en_GB'
        ]);
        $response = $client->getResponse();
        $this->assertJsonResponse($response, Response::HTTP_OK, true);
        $response = json_decode($response->getContent(), true);

        $this->assertCount(1 ,$response);

        tap($response[0], function($plan) {
            $this->assertArrayHasKey('name', $plan);
            $this->assertArrayHasKey('amount', $plan);
            $this->assertArrayHasKey('currency', $plan);
            $this->assertArrayHasKey('duration', $plan);
            $this->assertArrayHasKey('type', $plan);
            $this->assertArrayHasKey('promoAmount', $plan);
            $this->assertArrayHasKey('isBestseller', $plan);
        });

        $client->request('GET', (new BillingPlansUriGenerator())->get(), [
            'locale' => 'pl_PL'
        ]);
        $response = $client->getResponse();
        $this->assertJsonResponse($response, Response::HTTP_OK, true);

        $response = json_decode($response->getContent(), true);

        $this->assertCount(2 ,$response);
        tap($response[0], function($plan) {
            $this->assertArrayHasKey('name', $plan);
            $this->assertArrayHasKey('amount', $plan);
            $this->assertArrayHasKey('currency', $plan);
            $this->assertArrayHasKey('duration', $plan);
            $this->assertArrayHasKey('type', $plan);
            $this->assertArrayHasKey('promoAmount', $plan);
            $this->assertArrayHasKey('isBestseller', $plan);
        });
    }

    /**
     * @test
     */
    public function it_gets_plan_group_with_given_id()
    {
        $plan = $this->planRepository->findOneBy([
            'providerId' => PlanFixtures::PROVIDER_1_ID
        ]);

        $client = $this->createFakeAuthenticatedClient(static::USER_ID);
        $client->request('GET', (new BillingPlanUriGenerator($plan->getId()))->get(), [
            'locale' => 'pl_PL'
        ]);

        $response = $client->getResponse();
        $this->assertJsonResponse($response, Response::HTTP_OK, true);
        $response = json_decode($response->getContent(), true);

        $this->assertArrayHasKey('name', $response);
        $this->assertArrayHasKey('amount', $response);
        $this->assertArrayHasKey('currency', $response);
        $this->assertArrayHasKey('duration', $response);

        $this->assertSame(PlanFormatter::format($plan), $response);
    }
}
