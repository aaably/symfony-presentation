<?php

namespace Tests\BillingBundle\Functional\Command;

use Doctrine\ORM\EntityManagerInterface;
use Symetria\ApiServerBundle\Tests\AbstractApiWebTestCase;
use Symetria\BillingBundle\Command\SubscribeUserCommand;
use Fitatu\BillingBundle\DataFixtures\ORM\PlanFixtures;
use Fitatu\BillingBundle\DataFixtures\ORM\PlanGroupFixtures;
use Symetria\UserBundle\DataFixtures\ORM\User\RoleFixtures;
use Symetria\UserBundle\DataFixtures\ORM\User\UserFixtures;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class SubscribeUserCommandTest extends AbstractApiWebTestCase
{
    const COMMAND = SubscribeUserCommand::COMMAND;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @param string|null $name
     * @param array       $data
     * @param string      $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
    }

    public function setUp()
    {
        if (is_null($this->em)) {
            $this->em = $this->getContainer()
                ->get('doctrine')
                ->getManager();
        }
    }

    /**
     * @test
     */
    public function it_runs_subscriptions_command()
    {
        $this->loadFixtures([
            RoleFixtures::class,
            UserFixtures::class,
            PlanGroupFixtures::class,
            PlanFixtures::class,
        ]);

        $output = $this->runCommand(self::COMMAND, [
            'user' => 1,
            '--plan' => 1
        ]);
        $this->assertContains('Subscribe User', $output);
        $this->assertContains('Total users subscribed: 1', $output);

        $output = $this->runCommand(self::COMMAND, [
            'user' => 1,
            '--plan' => 1
        ]);
        $this->assertContains('Subscribe User', $output);
        $this->assertContains('Total users subscribed: 1', $output);
    }

    /**
     * @test
     */
    public function it_throws_an_error_when_failed()
    {
        $this->loadFixtures([
            RoleFixtures::class,
            UserFixtures::class,
            PlanGroupFixtures::class,
            PlanFixtures::class,
        ]);

        $output = $this->runCommand(self::COMMAND, [
            'user' => 1,
            '--plan' => 1234
        ]);
        $this->assertContains('Subscribe User', $output);
        $this->assertContains('Total errors: 1', $output);
    }

    /**
     * @test
     */
    public function it_runs_command_for_multiple_users()
    {
        $this->loadFixtures([
            RoleFixtures::class,
            UserFixtures::class,
            PlanGroupFixtures::class,
            PlanFixtures::class,
        ]);

        $output = $this->runCommand(self::COMMAND, [
            'user' => '1,2,3',
            '--plan' => 1
        ]);
        $this->assertContains('Subscribe User', $output);
        $this->assertContains('Total users subscribed: 3', $output);
    }

    /**
    * @test
    */
    public function it_runs_command_for_user_email()
    {
        $this->loadFixtures([
            RoleFixtures::class,
            UserFixtures::class,
            PlanGroupFixtures::class,
            PlanFixtures::class,
        ]);

        $output = $this->runCommand(self::COMMAND, [
            'user' => UserFixtures::USER_EMAIL,
            '--plan' => 1
        ]);
        $this->assertContains('Subscribe User', $output);
        $this->assertContains('Total users subscribed: 1', $output);
    }
}
