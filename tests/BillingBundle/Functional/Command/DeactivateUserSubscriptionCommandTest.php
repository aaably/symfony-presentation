<?php

namespace Tests\BillingBundle\Functional\Command;

use Doctrine\ORM\EntityManagerInterface;
use Symetria\ApiServerBundle\Tests\AbstractApiWebTestCase;
use Symetria\BillingBundle\Command\DeactivateUserSubscriptionCommand;
use Fitatu\BillingBundle\DataFixtures\ORM\PlanFixtures;
use Fitatu\BillingBundle\DataFixtures\ORM\PlanGroupFixtures;
use Fitatu\BillingBundle\DataFixtures\ORM\SubscriptionFixtures;
use Symetria\UserBundle\DataFixtures\ORM\User\RoleFixtures;
use Symetria\UserBundle\DataFixtures\ORM\User\UserFixtures;

/**
 * @author Lukasz Domanski
 * @copyright Fitatu Sp. z o.o.
 */
class DeactivateUserSubscriptionCommandTest extends AbstractApiWebTestCase
{
    const COMMAND = DeactivateUserSubscriptionCommand::COMMAND;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @param string|null $name
     * @param array       $data
     * @param string      $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
    }

    public function setUp()
    {
        if (is_null($this->em)) {
            $this->em = $this->getContainer()
                ->get('doctrine')
                ->getManager();
        }
    }

    /**
     * @test
     * @group functional
     */
    public function it_runs_deactivate_subscription_command()
    {
        $this->loadFixtures([
            RoleFixtures::class,
            UserFixtures::class,
            PlanGroupFixtures::class,
            PlanFixtures::class,
            SubscriptionFixtures::class,
        ]);

        $output = $this->runCommand(self::COMMAND);
        $this->assertContains('Deactivated subscriptions: 1', $output);
    }

    /**
     * @test
     * @group functional
     */
    public function it_runs_deactivate_subscription_for_one_user_command()
    {
        $this->loadFixtures([
            RoleFixtures::class,
            UserFixtures::class,
            PlanGroupFixtures::class,
            PlanFixtures::class,
            SubscriptionFixtures::class,
        ]);

        $output = $this->runCommand(self::COMMAND, ['--user' => UserFixtures::USER_ID]);
        $this->assertContains('Deactivated subscriptions: 1', $output);
    }

    /**
     * @test
     * @group functional
     */
    public function it_runs_deactivate_subscription_with_no_subscriptions_command()
    {
        $this->loadFixtures([
            RoleFixtures::class,
            UserFixtures::class,
            PlanGroupFixtures::class,
            PlanFixtures::class,
            SubscriptionFixtures::class,
        ]);

        $output = $this->runCommand(self::COMMAND, ['--user' => 12345]); // user subscriptions not exist
        $this->assertContains('Deactivated subscriptions: 0', $output);
    }
}
