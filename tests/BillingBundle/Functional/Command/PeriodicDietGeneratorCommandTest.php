<?php

namespace Tests\BillingBundle\Functional\Command;

use Doctrine\ORM\EntityManagerInterface;
use Symetria\ApiServerBundle\Tests\AbstractApiWebTestCase;
use Symetria\BillingBundle\Command\PeriodicDietGeneratorCommand;
use Fitatu\BillingBundle\DataFixtures\ORM\PlanFixtures;
use Fitatu\BillingBundle\DataFixtures\ORM\PlanGroupFixtures;
use Fitatu\BillingBundle\DataFixtures\ORM\SubscriptionFixtures;
use Symetria\UserBundle\DataFixtures\ORM\User\DietGeneratorFixtures;
use Symetria\UserBundle\DataFixtures\ORM\User\DietGeneratorSettingsDietFixtures;
use Symetria\UserBundle\DataFixtures\ORM\User\RoleFixtures;
use Symetria\UserBundle\DataFixtures\ORM\User\UserFixtures;

/**
 * @author Lukasz Domanski
 * @copyright Fitatu Sp. z o.o.
 */
class PeriodicDietGeneratorCommandTest extends AbstractApiWebTestCase
{
    const COMMAND = PeriodicDietGeneratorCommand::COMMAND;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @param string|null $name
     * @param array       $data
     * @param string      $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
    }

    public function setUp()
    {
        if (is_null($this->em)) {
            $this->em = $this->getContainer()
                ->get('doctrine')
                ->getManager();
        }
    }

    /**
     * @test
     * @group functional
     */
    public function it_runs_periodic_diet_generator_command()
    {
        $this->loadFixtures([
            RoleFixtures::class,
            UserFixtures::class,
            PlanGroupFixtures::class,
            PlanFixtures::class,
            SubscriptionFixtures::class,
            DietGeneratorFixtures::class,
            DietGeneratorSettingsDietFixtures::class,
        ]);

        $output = $this->runCommand(self::COMMAND, ['--dry-run' => true]);
        $this->assertContains('diet(s) to diet generator', $output);
        $this->assertContains('User ids:', $output);
    }
}
