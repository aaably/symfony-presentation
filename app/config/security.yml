# To get started with security, check out the documentation:
# http://symfony.com/doc/current/book/security.html

# !!! role_hierarchy is stored in Database and managed by RoleHierarchyProvider in SharedBundle

security:
    encoders:
        FOS\UserBundle\Model\UserInterface: bcrypt

    providers:
        chain_provider:
            chain:
                providers: [ fos_userbundle ]

        fos_userbundle:
            id: fos_user.user_provider.username_email

    firewalls:
        # disables authentication for assets and the profiler, adapt it according to your needs
        dev:
            pattern: ^/(_(profiler|wdt)|css|images|js)/
            security: false

        # supports requesting a new JSON Web Token at /api/login_check
        login:
            pattern:   ^/api/login$
            stateless: true
            anonymous: true
            form_login:
                check_path:                 /api/login
                require_previous_session:   false
                username_parameter:         _username
                password_parameter:         _password
                success_handler:            lexik_jwt_authentication.handler.authentication_success
                failure_handler:            symetria.security.authentication_failure_handler

        # Registration and activation is opened for anonymous users
        user_registration:
            pattern: ^/api/users$
            methods: [POST]
            stateless: true
            anonymous: true

        user_activation:
            pattern: ^/api/users/\d+/action/activate/
            methods: [PUT]
            stateless: true
            anonymous: true

        user_deletion:
            pattern: ^/api/users/\d+/action/delete/
            methods: [DELETE]
            stateless: true
            anonymous: true

        user_email_change_confirmation:
            pattern: ^/api/users/\d+/action/confirm-email-change/
            methods: [PUT]
            stateless: true
            anonymous: true

        user_login:
            pattern: ^/api/login
            methods: [POST, GET]
            stateless: true
            anonymous: true

        user_reset_password:
            pattern: ^/api/users/action/reset-password
            methods: [GET, POST]
            stateless: true
            anonymous: true

        user_login_demo:
            pattern: ^/api/login-demo
            methods: [POST]
            stateless: true
            anonymous: true

        api_ping:
            pattern: ^/api/ping
            methods: [HEAD, GET]
            stateless: true
            anonymous: true

        api_errors:
            pattern: ^/api/errors$
            methods: [POST]
            stateless: true
            anonymous: true

        api_roles_hierarchy:
            pattern: ^/api/roles/hierarchy$
            methods: [GET]
            stateless: true
            anonymous: true

        api_billing_plans:
            pattern: ^/api/billing/plans
            methods: [GET]
            stateless: true
            anonymous: true

        api_billing_callbacks:
            pattern: ^/api/billing/\d+/callback
            methods: [POST]
            stateless: true
            anonymous: true

        api_user_cache:
            pattern: ^/api/users/\d+/cache$
            methods: [DELETE]
            stateless: true
            anonymous: true

        api_diet_generator_stats:
            pattern: ^/api/diet-generator/stats$
            methods: [GET]
            stateless: true
            anonymous: true

        # validates a JSON Web Token passed in calls to /api/* (except /api/usernamelogin)
        api:
            pattern:   ^/v?api
            stateless: true
            lexik_jwt:
                authorization_header:
                    enabled:    true
                    prefix:     Bearer
                query_parameter:
                    enabled:    true
                    name:       bearer

        main:
            pattern:    ^/
            form_login:
                csrf_token_generator:  security.csrf.token_manager
            logout:     true
            anonymous:  true

    access_control:
        - { path: ^/login$,                                      role: IS_AUTHENTICATED_ANONYMOUSLY }
        - { path: ^/register$,                                   role: IS_AUTHENTICATED_ANONYMOUSLY }
        - { path: ^/resetting$,                                  role: IS_AUTHENTICATED_ANONYMOUSLY }
        - { path: ^/api/login-demo,                              role: IS_AUTHENTICATED_ANONYMOUSLY }
        - { path: ^/api/login-internal,                          role: IS_AUTHENTICATED_ANONYMOUSLY }
        - { path: ^/api/login-facebook,                          role: IS_AUTHENTICATED_ANONYMOUSLY }
        - { path: ^/api/users/action/reset-password,             role: IS_AUTHENTICATED_ANONYMOUSLY }
        - { path: ^/api/users/\d+/action/activate/,              role: IS_AUTHENTICATED_ANONYMOUSLY, methods: [PUT] }
        - { path: ^/api/users/\d+/action/delete/,                role: IS_AUTHENTICATED_ANONYMOUSLY, methods: [DELETE] }
        - { path: ^/api/users/\d+/action/confirm-email-change/,  role: IS_AUTHENTICATED_ANONYMOUSLY, methods: [PUT] }
        - { path: ^/api/users/\d+/cache$,                        role: IS_AUTHENTICATED_ANONYMOUSLY, methods: [DELETE] }
        - { path: ^/api/users$,                                  role: IS_AUTHENTICATED_ANONYMOUSLY, methods: [POST] }
        - { path: ^/api/ping,                                    role: IS_AUTHENTICATED_ANONYMOUSLY, methods: [HEAD, GET] }
        - { path: ^/api/errors$,                                 role: IS_AUTHENTICATED_ANONYMOUSLY, methods: [POST] }
        - { path: ^/api/roles/hierarchy$,                        role: IS_AUTHENTICATED_ANONYMOUSLY, methods: [GET] }
        - { path: ^/api/billing/plans,                           role: IS_AUTHENTICATED_ANONYMOUSLY, methods: [GET] }
        - { path: ^/api/billing/ios/callbacks,                   role: IS_AUTHENTICATED_ANONYMOUSLY, methods: [POST] }
        - { path: ^/api/billing/\d+/callback,                    role: IS_AUTHENTICATED_ANONYMOUSLY, methods: [POST] }
        - { path: ^/api/diet-generator/stats,                    role: IS_AUTHENTICATED_ANONYMOUSLY, methods: [GET] }
        - { path: ^/api,                                         role: IS_AUTHENTICATED_FULLY }
        - { path: ^/admin/users,                                 role: ROLE_SUPER_ADMIN }
        - { path: ^/admin,                                       role: ROLE_ADMIN }
