<?php

use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\HttpKernel\Kernel;

class AppKernel extends Kernel
{
    /**
     * @return array
     */
    public function registerBundles()
    {
        $bundles = [
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),

            // Dependencies
            new JMS\SerializerBundle\JMSSerializerBundle(),
            new FOS\UserBundle\FOSUserBundle(),
            new FOS\RestBundle\FOSRestBundle(),
            new Lexik\Bundle\JWTAuthenticationBundle\LexikJWTAuthenticationBundle(),
            new Nelmio\ApiDocBundle\NelmioApiDocBundle(),
            new League\Tactician\Bundle\TacticianBundle(),
            new OldSound\RabbitMqBundle\OldSoundRabbitMqBundle(),
            new Snc\RedisBundle\SncRedisBundle(),
            new Liip\ThemeBundle\LiipThemeBundle(),
            new Liuggio\ExcelBundle\LiuggioExcelBundle(),
            new Doctrine\Bundle\MigrationsBundle\DoctrineMigrationsBundle(),
            new JMS\TranslationBundle\JMSTranslationBundle(),
            new M6Web\Bundle\CassandraBundle\M6WebCassandraBundle(),

            // Symetria
            new Symetria\SharedBundle\SymetriaSharedBundle(),
            new Fitatu\SharedUserBundle\FitatuSharedUserBundle(),
            new Fitatu\DatabaseBundle\FitatuDatabaseBundle(),
            new Symetria\ApiServerBundle\SymetriaApiServerBundle(),
            new Symetria\SecurityBundle\SymetriaSecurityBundle(),
            new Symetria\UserBundle\SymetriaUserBundle(),
            new Symetria\EmailBundle\SymetriaEmailBundle(),
            new Symetria\ApiClientBundle\SymetriaApiClientBundle(),
            new Symetria\FitatuTranslationBundle\FitatuTranslationBundle(),
            new Symetria\LogBundle\SymetriaLogBundle(),
            new Symetria\BillingBundle\SymetriaBillingBundle(),
            new Fitatu\BillingBundle\FitatuBillingBundle(),
        ];

        if (in_array($this->getEnvironment(), ['dev', 'test'])) {
            $bundles[] = new Symfony\Bundle\DebugBundle\DebugBundle();
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
        }

        if (in_array($this->getEnvironment(), ['test'])) {
            $bundles[] = new Liip\FunctionalTestBundle\LiipFunctionalTestBundle();
        }

        return $bundles;
    }

    /**
     * @param LoaderInterface $loader
     */
    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load($this->getRootDir().'/config/config_'.$this->getEnvironment().'.yml');
    }

    /**
     * @return string
     */
    public function getRootDir()
    {
        return __DIR__;
    }

    /**
     * @return string
     */
    public function getCacheDir()
    {
        return dirname(__DIR__).'/var/cache/'.$this->environment;
    }

    /**
     * @return string
     */
    public function getLogDir()
    {
        return dirname(__DIR__).'/var/logs';
    }
}
