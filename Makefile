.PHONY: help install test report spec-test unit-test unit-report functional-test functional-report

help:
	@echo "Available commands:"
	@echo ""
	@echo "  help                  list of available make commands"
	@echo "  install               installs all dependencies (composer, npm, bower)"
	@echo "  test                  launch all tests (spec, unit, functional)"
	@echo "  report                generate code coverage reports for unit and functional tests"
	@echo "  spec-test             launch PhpSpec tests"
	@echo "  unit-test             launch PHPUnit unit tests"
	@echo "  unit-report           launch PHPUnit unit tests with code coverage reports"
	@echo "  functional-test       launch PHPUnit functional tests"
	@echo "  functional-report     launch PHPUnit functional tests with code coverage reports"
	@echo ""

install:
	composer update
	npm update
	bower update

test: spec-test unit-test functional-test

report: unit-report functional-report

spec-test:
	@echo "PHPSPEC"
	bin/phpspec run -f pretty

unit-test:
	@echo "PHPUNIT -- UNIT TESTS"
	bin/phpunit -c build/phpunit_unit_reports.xml --debug -vvv --no-coverage

unit-report:
	@echo "PHPUNIT -- UNIT TESTS -- WITH REPORT"
	bin/phpunit -c build/phpunit_unit_reports.xml --debug -vvv

functional-test:
	@echo "flushall \n quit" | nc redis 6379
	@echo "PHPUNIT -- FUNCTIONAL TESTS"
	php bin/console doctrine:database:drop --env=test --force --connection=auth -q
	php bin/console doctrine:database:drop --env=test --force --connection=shared -q
	php bin/console doctrine:database:create --env=test --connection=auth -q
	php bin/console doctrine:database:create --env=test --connection=shared -q
	php bin/console doctrine:schema:create --env=test --em=auth -q
	php bin/console doctrine:schema:create --env=test --em=shared -q
	bin/phpunit -c build/phpunit_functional_reports.xml --debug -vvv --no-coverage --stop-on-failure

functional-report:
	@echo "PHPUNIT -- FUNCTIONAL TESTS -- WITH REPORT AND TAP"
	php bin/console doctrine:database:drop --env=test --force --connection=auth -q
	php bin/console doctrine:database:drop --env=test --force --connection=shared -q
	php bin/console doctrine:database:create --env=test --connection=auth -q
	php bin/console doctrine:database:create --env=test --connection=shared -q
	php bin/console doctrine:schema:create --env=test --em=auth -q
	php bin/console doctrine:schema:create --env=test --em=shared -q
	bin/phpunit -c build/phpunit_functional_reports.xml --debug -vvv
