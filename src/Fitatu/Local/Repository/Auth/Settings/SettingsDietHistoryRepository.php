<?php

namespace Fitatu\Local\Repository\Auth\Settings;

use DateTime;
use DateTimeInterface;
use Fitatu\DatabaseBundle\Entity\Auth\Settings\SettingsDiet;
use Fitatu\DatabaseBundle\Entity\Auth\Settings\SettingsDietHistory;
use Fitatu\DatabaseBundle\Entity\Auth\User\User;
use Fitatu\DatabaseBundle\Repository\Auth\Settings\AbstractSettingsDietHistoryRepository;
use Fitatu\SharedUserBundle\Model\Settings\SettingsDietInterface;

/**
 * @author    Lukasz Domanski
 * @copyright Fitatu Sp. z o.o.
 */
class SettingsDietHistoryRepository extends AbstractSettingsDietHistoryRepository
{
    /**
     * @param SettingsDietHistory $settings
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function persist($settings)
    {
        $this->getEntityManager()->persist($settings);
        $this->getEntityManager()->flush();
    }

    /**
     * @param int               $userId
     * @param DateTimeInterface $date
     *
     * @return SettingsDietHistory|null
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findSettings(int $userId, DateTimeInterface $date)
    {
        $updatedAt = $date->format('Y-m-d 23:59:59');

        /** @var SettingsDietInterface $settings */
        $settings = $this->createQueryBuilder('h')
            ->andWhere("h.userId = :userId AND h.updatedAt <= '{$updatedAt}'")
            ->setParameters(['userId' => $userId])
            ->orderBy('h.updatedAt', 'DESC')
            ->getQuery()
            ->setFirstResult(0)
            ->setMaxResults(1)
            ->getOneOrNullResult();

        if (!$settings instanceof SettingsDietInterface) {
            $settings = $this->findNearestDietSettings($userId, $date);
        }

        return $settings;
    }

    /**
     * @param int               $userId
     * @param DateTimeInterface $date
     *
     * @return SettingsDietHistory|null
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    private function findNearestDietSettings(int $userId, DateTimeInterface $date)
    {
        $updatedAt = $date->format('Y-m-d 00:00:00');

        $settings = $this->createQueryBuilder('h')
            ->andWhere("h.userId = :userId AND h.updatedAt >= '{$updatedAt}'")
            ->setParameters(['userId' => $userId])
            ->orderBy('h.updatedAt', 'ASC')
            ->getQuery()
            ->setFirstResult(0)
            ->setMaxResults(1)
            ->getOneOrNullResult();

        if ($settings instanceof SettingsDietInterface) {
            $updatedAt = $settings->getUpdatedAt()->format('Y-m-d 23:59:59');
            $settings = $this->createQueryBuilder('h')
                ->andWhere("h.userId = :userId AND h.updatedAt <= '{$updatedAt}'")
                ->setParameters(['userId' => $userId])
                ->orderBy('h.updatedAt', 'DESC')
                ->getQuery()
                ->setFirstResult(0)
                ->setMaxResults(1)
                ->getOneOrNullResult();
        }

        return $settings;
    }

    /**
     * Add diet settings history
     *
     * @param SettingsDiet $settingsDiet
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function add(SettingsDiet $settingsDiet)
    {
        $settingsHistory = new SettingsDietHistory();
        $settingsHistory->setUser($settingsDiet->getUser());
        $settingsHistory->setUserId($settingsDiet->getUser()->getId());
        $settingsHistory->setUpdatedAt(new DateTime());
        $settingsHistory->setActivityBase($settingsDiet->getActivityBase());
        $settingsHistory->setActivityTraining($settingsDiet->getActivityTraining());
        $settingsHistory->setActivityEnergyInclusionMode($settingsDiet->getActivityEnergyInclusionMode());
        $settingsHistory->setWeightChangeSpeed($settingsDiet->getWeightChangeSpeed());
        $settingsHistory->setWeightChangeSpeedUnit($settingsDiet->getWeightChangeSpeedUnit());
        $settingsHistory->setWeightChangeSpeedKg($settingsDiet->getWeightChangeSpeedKg());
        $settingsHistory->setWeightChangeDirection($settingsDiet->getWeightChangeDirection());
        $settingsHistory->setManualEnergyTarget($settingsDiet->getManualEnergyTarget());
        $settingsHistory->setEnergy($settingsDiet->getEnergy());
        $settingsHistory->setProteinPercentage($settingsDiet->getProteinPercentage());
        $settingsHistory->setProteinWeight($settingsDiet->getProteinWeight());
        $settingsHistory->setFatPercentage($settingsDiet->getFatPercentage());
        $settingsHistory->setFatWeight($settingsDiet->getFatWeight());
        $settingsHistory->setCarbohydratePercentage($settingsDiet->getCarbohydratePercentage());
        $settingsHistory->setCarbohydrateWeight($settingsDiet->getCarbohydrateWeight());
        $settingsHistory->setMealSchema($settingsDiet->getMealSchema());
        $settingsHistory->setSettingsDietNutritionGroup($settingsDiet->getSettingsDietNutritionGroup());

        $this->getEntityManager()->persist($settingsHistory);
        $this->getEntityManager()->flush();
    }

    /**
     * @param User $user
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function removeByUser(User $user)
    {
        $settings = $this->findBy(['user' => $user]);

        if (!$settings) {
            return;
        }

        foreach ($settings as $setting) {
            $this->getEntityManager()->remove($setting);
        }

        $this->getEntityManager()->flush();
    }
}
