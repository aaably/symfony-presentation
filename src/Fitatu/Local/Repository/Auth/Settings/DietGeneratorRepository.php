<?php

namespace Fitatu\Local\Repository\Auth\Settings;

use Fitatu\DatabaseBundle\Entity\Auth\Settings\DietGenerator;
use Fitatu\DatabaseBundle\Entity\Auth\Subscription;
use Fitatu\DatabaseBundle\Entity\Auth\User\User;
use Fitatu\DatabaseBundle\Repository\Auth\Settings\AbstractDietGeneratorRepository;
use Fitatu\SharedBundle\Model\Date\DateRange;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class DietGeneratorRepository extends AbstractDietGeneratorRepository
{
    /**
     * @param DietGenerator $dietGenerator
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function persist($dietGenerator)
    {
        $this->getEntityManager()->persist($dietGenerator);
        $this->getEntityManager()->flush();
    }

    /**
     * Add or update diet settings
     *
     * @param DietGenerator $dietGenerator
     *
     * @return DietGenerator
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function addOrUpdate(DietGenerator $dietGenerator): DietGenerator
    {
        $dietGenerator->setUpdatedAt(new \DateTime());
        $this->getEntityManager()->persist($dietGenerator);
        $this->getEntityManager()->flush();

        return $dietGenerator;
    }

    /**
     * @param User $user
     *
     * @return DietGenerator
     */
    public function findOrMake(User $user): DietGenerator
    {
        $entity = $this->findOneBy(['user' => $user->getId()]);

        if (empty($entity)) {
            $entity = DietGenerator::create([
                'user'   => $user,
                'locale' => $user->getLocale()
            ]);
        }

        return $entity;
    }

    /**
     * @param User  $user
     * @param array $attributes
     *
     * @return DietGenerator
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function create(User $user, array $attributes = []): DietGenerator
    {
        $attributes = array_merge([
            'user'   => $user,
            'locale' => $user->getLocale(),
        ], $attributes);

        $entity = DietGenerator::create($attributes);
        $this->persist($entity);

        return $entity;
    }

    /**
     * @param User  $user
     * @param array $attributes
     *
     * @return DietGenerator
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function createOrReset(User $user, array $attributes = []): DietGenerator
    {
        $dietGenerator = $this->findOneBy(['user' => $user]);

        if ($dietGenerator instanceof DietGenerator) {
            return $this->resetToDefault($dietGenerator);
        }

        return $this->create($user, $attributes);
    }

    /**
     * @param DietGenerator $dietGenerator
     *
     * @return DietGenerator
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function resetToDefault(DietGenerator $dietGenerator): DietGenerator
    {
        $dietGenerator->setIsReviewed(0)
            ->setFromDate(new \DateTime())
            ->setToDate(null)
            ->setLastActivatedAt(null)
            ->setLastGeneratedAt(null)
            ->setLastRequestedAt(null)
            ->setRequestAt(null);

        $this->persist($dietGenerator);

        return $dietGenerator;
    }

    /**
     * @param User $user
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function deleteByUser(User $user)
    {
        $dietGenerator = $this->findOneBy([
            'user' => $user
        ]);

        if ($dietGenerator instanceof DietGenerator) {
            $this->getEntityManager()->remove($dietGenerator);
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @return int
     *
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function countQueued(): int
    {
        return (int)$this->createQueryBuilder('dg')
            ->select('COUNT(DISTINCT dg)')
            ->innerJoin(Subscription::class, 'subscription', 'WITH', 'dg.userId = IDENTITY(subscription.user)')
            ->where('subscription.active = 1')
            ->andWhere('subscription.startDate <= :now OR subscription.startDate IS NULL')
            ->andWhere('subscription.endDate >= :now OR subscription.endDate IS NULL')
            ->andWhere('dg.lastRequestedAt IS NOT NULL')
            ->andWhere('dg.lastGeneratedAt IS NULL OR dg.lastGeneratedAt < dg.lastRequestedAt')
            ->setParameter('now', new \DateTime())
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @return int
     *
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function countExpired(): int
    {
        return (int)$this->createQueryBuilder('dg')
            ->select('COUNT(DISTINCT dg)')
            ->innerJoin(Subscription::class, 'subscription', 'WITH', 'dg.userId = IDENTITY(subscription.user)')
            ->where('subscription.active = 1')
            ->andWhere('subscription.startDate <= :now OR subscription.startDate IS NULL')
            ->andWhere('subscription.endDate >= :now OR subscription.endDate IS NULL')
            ->andWhere('dg.requestAt < :now')
            ->andWhere('dg.lastGeneratedAt IS NULL OR dg.lastGeneratedAt < dg.lastRequestedAt OR dg.lastActivatedAt < dg.lastRequestedAt OR dg.lastActivatedAt < dg.requestAt')
            ->setParameter('now', new \DateTime())
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @param DateRange $dateRange
     *
     * @return int
     *
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function countGeneratedInDateRange(DateRange $dateRange)
    {
        return (int)$this->createQueryBuilder('dg')
            ->select('COUNT(dg)')
            ->where('dg.lastGeneratedAt >= :from')
            ->andWhere('dg.lastGeneratedAt <= :to')
            ->setParameter('from', $dateRange->getFrom())
            ->setParameter('to', $dateRange->getTo())
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @param DateRange $dateRange
     *
     * @return int
     *
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function countActivatedInDateRange(DateRange $dateRange)
    {
        return (int)$this->createQueryBuilder('dg')
            ->select('COUNT(dg)')
            ->where('dg.lastActivatedAt >= :from')
            ->andWhere('dg.lastActivatedAt <= :to')
            ->setParameter('from', $dateRange->getFrom())
            ->setParameter('to', $dateRange->getTo())
            ->getQuery()
            ->getSingleScalarResult();
    }
}
