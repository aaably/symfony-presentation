<?php

namespace Fitatu\Local\Repository\Auth\Settings;

use Fitatu\DatabaseBundle\Entity\Auth\Settings\SettingsPremium;
use Fitatu\DatabaseBundle\Entity\Auth\User\User;
use Fitatu\DatabaseBundle\Repository\Auth\Settings\AbstractSettingsPremiumRepository;
use Symetria\SharedBundle\Model\UserInterface;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class SettingsPremiumRepository extends AbstractSettingsPremiumRepository
{
    /**
     * Add or update diet settings
     *
     * @param SettingsPremium $settingsPremium
     *
     * @return SettingsPremium
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function addOrUpdate(SettingsPremium $settingsPremium): SettingsPremium
    {
        $existingSettingsPremium = $this->find($settingsPremium->getUserId());

        $settingsPremium = $existingSettingsPremium ?? $settingsPremium;
        $settingsPremium->setUpdatedAt(new \DateTime());

        return $this->persist($settingsPremium);
    }

    /**
     * @param User $user
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function removeByUser(User $user)
    {
        $settings = $this->findBy(['user' => $user]);

        if (!$settings) {
            return;
        }

        foreach ($settings as $setting) {
            $this->getEntityManager()->remove($setting);
        }

        $this->getEntityManager()->flush();
    }

    /**
     * @param UserInterface $user
     *
     * @return SettingsPremium
     */
    public function findOrMake(UserInterface $user): SettingsPremium
    {
        $entity = $this->findOneBy(['user' => $user]);

        if (!$entity instanceof SettingsPremium) {
            $entity = SettingsPremium::create([
                'locale' => $user->getLocale(),
                'user'   => $user,
            ]);
        }

        return $entity;
    }
}
