<?php

namespace Fitatu\Local\Repository\Auth\Settings;

use Fitatu\DatabaseBundle\Entity\Auth\User\User;
use Fitatu\DatabaseBundle\Repository\Auth\Settings\AbstractSettingsDietRepository;
use Fitatu\SharedUserBundle\Model\Settings\AbstractSettingsDiet;

/**
 * @author Lukasz Domanski
 * @copyright Fitatu Sp. z o.o.
 */
class SettingsDietRepository extends AbstractSettingsDietRepository
{
    /**
     * Add or update diet settings
     *
     * @param AbstractSettingsDiet $settingsDiet
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function addOrUpdate(AbstractSettingsDiet $settingsDiet)
    {
        $this->getEntityManager()->persist($settingsDiet);
        $this->getEntityManager()->flush();
    }

    /**
     * @param User $user
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function removeByUser(User $user)
    {
        $settings = $this->findBy(['user' => $user]);

        if (!$settings) {
            return;
        }

        foreach ($settings as $setting) {
            $this->getEntityManager()->remove($setting);
        }

        $this->getEntityManager()->flush();
    }
}
