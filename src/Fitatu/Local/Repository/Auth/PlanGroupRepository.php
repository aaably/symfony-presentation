<?php

namespace Fitatu\Local\Repository\Auth;

use Fitatu\DatabaseBundle\Entity\Auth\PlanGroup;
use Fitatu\DatabaseBundle\Repository\Auth\AbstractPlanGroupRepository;

/**
 * @author    Nikodem Osmialowski
 * @copyright Fitatu Sp. z o.o.
 */
class PlanGroupRepository extends AbstractPlanGroupRepository
{
    /**
     * @param PlanGroup $planGroup
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function persist($planGroup)
    {
        $this->getEntityManager()->persist($planGroup);
        $this->getEntityManager()->flush();
    }
}
