<?php

namespace Fitatu\Local\Repository\Auth;

use Fitatu\DatabaseBundle\Repository\Auth\AbstractSubscriptionRepository;

/**
 * @author    Nikodem Osmialowski
 * @copyright Fitatu Sp. z o.o.
 */
class SubscriptionRepository extends AbstractSubscriptionRepository
{
}
