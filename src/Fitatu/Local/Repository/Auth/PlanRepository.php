<?php

namespace Fitatu\Local\Repository\Auth;

use Fitatu\DatabaseBundle\Entity\Auth\Plan;
use Fitatu\DatabaseBundle\Repository\Auth\AbstractPlanRepository;

/**
 * @author    Nikodem Osmialowski
 * @copyright Fitatu Sp. z o.o.
 */
class PlanRepository extends AbstractPlanRepository
{
    /**
     * @param Plan $plan
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function persist($plan)
    {
        $this->getEntityManager()->persist($plan);
        $this->getEntityManager()->flush();
    }
}
