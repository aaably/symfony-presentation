<?php

namespace Fitatu\Local\Repository\Auth;

use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Fitatu\DatabaseBundle\Entity\Auth\Review;
use Fitatu\DatabaseBundle\Entity\Auth\User\User;
use Fitatu\DatabaseBundle\Repository\Auth\AbstractReviewRepository;
use Symetria\SharedBundle\Model\CommonResourceTypes;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class ReviewRepository extends AbstractReviewRepository
{
    /**
     * @param string      $type
     * @param string|null $locale
     * @param int|null    $rate
     * @param bool|null   $visible
     *
     * @return int
     *
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Exception
     */
    public function countByType(string $type, string $locale = null, int $rate = null, bool $visible = null): int
    {
        $query = $this->createQueryBuilder('r')
                      ->select('COUNT(r.id)')
                      ->where('r.isVisible = 1');

        return (int)$this->getLocaleTypeAndVisibilityQueryFilter($query, $type, $locale, $rate, $visible)
                         ->getQuery()
                         ->useQueryCache(true)
                         ->useResultCache(true, 3600)
                         ->getSingleScalarResult();
    }

    /**
     * @param string|null $type
     * @param int         $page
     * @param int         $limit
     * @param string      $locale
     * @param bool|null   $visible
     *
     * @return Paginator
     *
     * @throws \Exception
     */
    public function paginate(string $type = null, int $page = 1, int $limit = 20, string $locale = null, bool $visible = null): Paginator
    {
        $query = $this->getLocaleTypeAndVisibilityQueryFilter(
            $this->createQueryBuilder('r'), $type, $locale, null, $visible
        )->getQuery();
        $paginator = new Paginator($query);

        $paginator->getQuery()->setFirstResult(($page - 1) * $limit)
                  ->setMaxResults($limit);

        return $paginator;
    }

    /**
     * @param QueryBuilder $query
     * @param string|null  $type
     * @param string|null  $locale
     * @param int|null     $rate
     * @param bool|null    $visible
     *
     * @return QueryBuilder
     *
     * @throws \Exception
     */
    public function getLocaleTypeAndVisibilityQueryFilter(
        QueryBuilder $query,
        string $type = null,
        string $locale = null,
        int $rate = null,
        bool $visible = null
    ): QueryBuilder {
        if ($type) {
            $typeId = CommonResourceTypes::getTypeByName(strtoupper($type));

            $query->where('r.typeId = :typeId')
                  ->setParameter('typeId', $typeId);
        }

        if ($locale) {
            $query->andWhere('r.locale = :locale')
                  ->setParameter('locale', $locale);
        }

        if ($rate) {
            $query->andWhere('r.rate = :rate')
                  ->setParameter('rate', $rate);
        }

        if (null !== $visible) {
            $query
                ->andWhere($query->expr()->eq('r.isVisible', ':isVisible'))
                ->setParameter('isVisible', $visible);
        }

        return $query;
    }

    /**
     * @param User     $user
     * @param int|null $typeId
     *
     * @return bool
     */
    public function existsForUser(User $user, ?int $typeId = null): bool
    {
        $criteria = [
            'user' => $user,
        ];

        if ($typeId) {
            $criteria['typeId'] = $typeId;
        }

        return $this->findOneBy($criteria) instanceof Review;
    }
}
