<?php

namespace Fitatu\Local\Repository\Auth;

use Fitatu\DatabaseBundle\Entity\Auth\Payment;
use Fitatu\DatabaseBundle\Repository\Auth\AbstractPaymentRepository;

/**
 * @author    Nikodem Osmialowski
 * @copyright Fitatu Sp. z o.o.
 */
class PaymentRepository extends AbstractPaymentRepository
{
    /**
     * @param Payment $payment
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function persist($payment)
    {
        $this->getEntityManager()->persist($payment);
        $this->getEntityManager()->flush();
    }
}
