<?php

namespace Symetria\BillingBundle\Service;

use Fitatu\ApiServerBundle\Util\Env;
use GuzzleHttp\Client;
use Psr\Log\LoggerInterface;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class IosPaymentEventBroadcastService
{
    public const APP_ENV_KEY = 'APP_ENV';
    public const APP_ENV_DEFAULT_VALUE = 'DEV';
    public const APP_ENV_APPROVED_VALUE = 'PROD';
    public const APP_ENV_ENDPOINTS_KEY = 'APP_BROADCAST_ENDPOINTS';

    /**
     * @var string
     */
    private $env;

    /**
     * @var Client
     */
    private $client;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param Client          $client
     * @param LoggerInterface $logger
     */
    public function __construct(Client $client, LoggerInterface $logger)
    {
        $this->env = Env::get(static::APP_ENV_KEY, static::APP_ENV_DEFAULT_VALUE);
        $this->client = $client;
        $this->logger = $logger;
    }

    /**
     * @return bool
     */
    public function shouldBroadcast(): bool
    {
        return $this->env == static::APP_ENV_APPROVED_VALUE;
    }

    /**
     * @return string[]
     */
    public function getEndpoints(): array
    {
        return array_filter(explode("|", Env::get(static::APP_ENV_ENDPOINTS_KEY)));
    }

    /**
     * @param array $parameters
     */
    public function broadcast(array $parameters)
    {
        if (!$this->shouldBroadcast()) {
            return;
        }
        foreach ($this->getEndpoints() as $endpoint) {
            try {
                $this->client->postAsync($endpoint, $parameters);
                $this->logRequest($parameters, $endpoint);
            } catch (\Exception $e) {
                $this->logErrorRequest($parameters, $endpoint, $e);
            }
        }
    }

    /**
     * @param array  $parameters
     * @param string $entpoint
     */
    private function logRequest(array $parameters, string $entpoint): void
    {
        $this->logger->info(sprintf('Sent iOS payment event to: %s | Request: %s', $entpoint, collect($parameters)));
    }

    /**
     * @param array      $parameters
     * @param string     $entpoint
     * @param \Exception $e
     */
    private function logErrorRequest(array $parameters, string $entpoint, \Exception $e): void
    {
        $this->logger->critical(sprintf(
            'Error sending iOS payment event to: %s | Message: %s | Request: %s | Stacktrace: %s',
            $entpoint,
            $e->getMessage(),
            collect($parameters),
            $e->getTraceAsString()
        ));
    }
}