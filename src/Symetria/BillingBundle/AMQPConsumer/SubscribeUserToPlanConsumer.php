<?php

namespace Symetria\BillingBundle\AMQPConsumer;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection as DoctrineCollection;
use Doctrine\ORM\EntityManagerInterface;
use Fitatu\ApiClientBundle\Service\UserCacheCleanerService;
use Fitatu\BillingBundle\Factory\PaymentFactory;
use Fitatu\BillingBundle\Service\SubscriptionService;
use Fitatu\DatabaseBundle\Entity\Auth\Plan;
use Fitatu\DatabaseBundle\Entity\Auth\Settings\SettingsPremium;
use Fitatu\DatabaseBundle\Entity\Auth\Subscription;
use Fitatu\DatabaseBundle\Entity\Auth\User\User;
use Fitatu\Local\Repository\Auth\PaymentRepository;
use Fitatu\Local\Repository\Auth\PlanRepository;
use Fitatu\Local\Repository\Auth\Settings\DietGeneratorRepository;
use Fitatu\Local\Repository\Auth\Settings\SettingsPremiumRepository;
use Fitatu\Local\Repository\Auth\SubscriptionRepository;
use Fitatu\Local\Repository\Auth\User\UserRepository;
use Fitatu\SharedUserBundle\Model\Role\RoleInterface;
use Illuminate\Support\Collection;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use Psr\Log\LoggerInterface;
use Symetria\SecurityBundle\Service\RoleHierarchyService;
use Symetria\SecurityBundle\Service\RolesService;
use Symetria\SharedBundle\AMQPConsumer\AbstractConsumer;
use Symetria\SharedBundle\Event\Subscription\SubscribeUserToPlanEvent;
use Symetria\UserBundle\Service\SettingsPremiumService;

/**
 * @author    Sebastian Błaszczak
 * @copyright Fitatu Sp. z o.o.
 */
class SubscribeUserToPlanConsumer extends AbstractConsumer
{
    /**
     * @var PaymentRepository
     */
    private $paymentRepository;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var PlanRepository
     */
    private $planRepository;

    /**
     * @var SubscriptionService
     */
    private $subscriptionService;

    /**
     * @var UserCacheCleanerService
     */
    private $userCacheCleanerService;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var DietGeneratorRepository
     */
    private $dietGeneratorRepository;

    /**
     * @var RolesService
     */
    private $rolesService;

    /**
     * @var RoleHierarchyService
     */
    private $roleHierarchyService;

    /**
     * @var SettingsPremiumService
     */
    private $settingsPremiumService;

    /**
     * @var SubscriptionRepository
     */
    private $subscriptionRepository;

    /**
     * @param PaymentRepository       $paymentRepository
     * @param UserRepository          $userRepository
     * @param PlanRepository          $planRepository
     * @param SubscriptionService     $subscriptionService
     * @param SubscriptionRepository  $subscriptionRepository
     * @param UserCacheCleanerService $userCacheCleanerService
     * @param EntityManagerInterface  $entityManager
     * @param LoggerInterface         $logger
     * @param DietGeneratorRepository $dietGeneratorRepository
     * @param SettingsPremiumService  $settingsPremiumService
     * @param RolesService            $rolesService
     * @param RoleHierarchyService    $roleHierarchyService
     */
    public function __construct(
        PaymentRepository $paymentRepository,
        UserRepository $userRepository,
        PlanRepository $planRepository,
        SubscriptionService $subscriptionService,
        SubscriptionRepository $subscriptionRepository,
        UserCacheCleanerService $userCacheCleanerService,
        EntityManagerInterface $entityManager,
        LoggerInterface $logger,
        DietGeneratorRepository $dietGeneratorRepository,
        SettingsPremiumService $settingsPremiumService,
        RolesService $rolesService,
        RoleHierarchyService $roleHierarchyService
    ) {
        $this->paymentRepository = $paymentRepository;
        $this->userRepository = $userRepository;
        $this->planRepository = $planRepository;
        $this->subscriptionService = $subscriptionService;
        $this->userCacheCleanerService = $userCacheCleanerService;
        $this->entityManager = $entityManager;
        $this->logger = $logger;
        $this->dietGeneratorRepository = $dietGeneratorRepository;
        $this->rolesService = $rolesService;
        $this->roleHierarchyService = $roleHierarchyService;
        $this->settingsPremiumService = $settingsPremiumService;
        $this->subscriptionRepository = $subscriptionRepository;
    }

    /**
     * @param AMQPMessage $msg
     *
     * @return int
     *
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function run(AMQPMessage $msg): int
    {
        if (true === $this->isBrokenMessage($msg->getBody())) {
            return ConsumerInterface::MSG_REJECT;
        }

        $transaction = new Collection();
        try {
            $this->entityManager->getConnection()->beginTransaction();

            /** @var SubscribeUserToPlanEvent $event */
            $event = $this->loadEvent($msg->getBody());
            /** @var Collection $transaction */
            $transaction = $event->getTransaction();
            /** @var User $user */
            $user = $this->loadUser($event->getUserId());
            /** @var Plan $plan */
            $plan = $this->loadPlan($event->getPlanId());

            $roles = $plan->getPlanGroup()->getRoles();

            $subscriptions = $this->subscriptionRepository->findActiveForUser($user->getId());
            $showMatchingProcess = $this->shouldShowMatchingProcess($subscriptions, $roles);

            /** @var Subscription $subscription */
            $subscription = $this->subscriptionService->subscribeUserToPlan($user, $plan);

            if (!empty($transaction->toArray())) {
                $this->recordPayment($user, $subscription, $transaction);
            }

            $this->rolesService->removePremiumRoles($user);
            $this->rolesService->assignRoles(
                $user,
                $roles
            );

            if ($this->shouldCreateDietGenerator($roles)) {
                $this->dietGeneratorRepository->createOrReset($user);
            }

            $this->settingsPremiumService->update($user->getId(), [
                    'locale'          => $user->getLocale(),
                    'matchingProcess' => $showMatchingProcess,
            ], $shouldSendToGeneration = false);

            $this->entityManager->commit();
            $this->userCacheCleanerService->flush($user->getId());
            $this->logToConsole($user, $plan, $subscription);
        } catch (\Exception $e) {
            $this->entityManager->getConnection()->rollBack();
            $this->logToLogger($e, $transaction);
        }

        return ConsumerInterface::MSG_ACK;
    }

    /**
     * @param string $body
     * @return SubscribeUserToPlanEvent
     */
    protected function loadEvent(string $body): SubscribeUserToPlanEvent
    {
        return unserialize($body);
    }

    /**
     * @param User         $user
     * @param Subscription $subscription
     * @param Collection   $transaction
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    protected function recordPayment(User $user, Subscription $subscription, Collection $transaction): void
    {
        $this->paymentRepository->create(
            PaymentFactory::create($user, $subscription, $transaction)
        );
    }

    /**
     * @param int $userId
     * @return null|User
     */
    protected function loadUser(int $userId): ?User
    {
        return $this->userRepository->find($userId);
    }

    /**
     * @param int $planId
     * @return null|Plan
     */
    protected function loadPlan(int $planId): ?Plan
    {
        return $this->planRepository->find($planId);
    }

    /**
     * @param Subscriptions[]    $subscriptions
     * @param DoctrineCollection $roles
     *
     * @return bool
     */
    protected function shouldShowMatchingProcess(array $subscriptions, DoctrineCollection $roles): bool
    {
        if (empty($subscriptions)) {
            return true;
        }

        /** @var ArrayCollection $currentRoles */
        $currentRoles = $subscriptions[0]->getPlan()->getPlanGroup()->getRoles();

        return !!count(array_diff(
            $currentRoles->toArray(),
            $roles->toArray()
        ));
    }

    /**
     * @param User         $user
     * @param Plan         $plan
     * @param Subscription $subscription
     * @return void
     */
    private function logToConsole(User $user, Plan $plan, Subscription $subscription): void
    {
        $this->consoleLog(
            sprintf(
                'User "%s" (#%d) subscribed to plan "%s" (#%d). User subscription (#%d) starts in "%s" and ends in "%s"',
                $user->getUsername(),
                $user->getId(),
                $plan->getName(),
                $plan->getId(),
                $subscription->getId(),
                $subscription->getFormattedStartDate(),
                $subscription->getFormattedEndDate()
            )
        );
    }

    /**
     * @param \Exception $e
     * @param Collection $transaction
     * @return void
     */
    private function logToLogger(\Exception $e, Collection $transaction): void
    {
        $this->logger->critical(
            sprintf(
                'Error while subscribing user to plan, error message: "%s", stacktrace: "%s", transaction details: "%s"',
                $e->getMessage(),
                $e->getTraceAsString(),
                collect($transaction)
            )
        );
    }

    /**
     * @param DoctrineCollection $roles
     * @return bool
     */
    private function shouldCreateDietGenerator(DoctrineCollection $roles): bool
    {
        foreach ($roles as $role) {
            if ($this->roleHierarchyService->roleContains($role->getRole(), RoleInterface::ROLE_FEATURE_DIET)) {
                return true;
            }
        }

        return false;
    }
}
