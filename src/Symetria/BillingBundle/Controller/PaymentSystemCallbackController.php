<?php

namespace Symetria\BillingBundle\Controller;

use Fitatu\BillingBundle\Service\PaymentService;
use Fitatu\Local\Repository\Auth\User\UserRepository;
use FOS\RestBundle\Controller\Annotations\Post;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Psr\Log\LoggerInterface;
use Symetria\UserBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class PaymentSystemCallbackController extends AbstractController
{
    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var PaymentService
     */
    private $paymentService;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param RequestStack    $requestStack
     * @param UserRepository  $userRepository
     * @param PaymentService  $paymentService
     * @param LoggerInterface $logger
     */
    public function __construct(
        RequestStack $requestStack,
        UserRepository $userRepository,
        PaymentService $paymentService,
        LoggerInterface $logger
    ) {
        $this->requestStack = $requestStack;
        $this->userRepository = $userRepository;
        $this->paymentService = $paymentService;
        $this->logger = $logger;
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  authentication=false,
     *  section="Billing",
     *  description="Catch all payment system callbacks",
     *  method="POST",
     *  uri="/api/billings/{userId}/callback",
     *  tags={
     *      "stable" = "green",
     *      "tested" = "red",
     *  },
     *  parameters={
     *  },
     *  statusCodes={
     *      200="Returned was successful"
     *  }
     * )
     *
     * @Post("/api/billing/{userId}/callback")
     *
     * @param int $userId
     * @return JsonResponse
     * @throws \Symfony\Component\Filesystem\Exception\IOException
     */
    public function postCallbackAction(int $userId): JsonResponse
    {
        /** @var Request $request */
        $request = $this->requestStack->getCurrentRequest()->request;
        $transaction = $request->all();

        $providedUserId = empty($transaction['additionalData']['developerPayload']) ?: intval($transaction['additionalData']['developerPayload']);

        if ($userId !== $providedUserId) {
            throw new AccessDeniedHttpException('Access denied.');
        }

        $this->temporaryPaymentLoggerForTest($request); // fixme !!!!!

        try{
            $isValid = $this->paymentService
                ->setUser($userId)
                ->setTransaction($transaction)
                ->validate();

            $this->temporaryPaymentLoggerForTest($isValid, 'Payment Response');

        } catch(\Exception $e) {
            $this->logger->error(sprintf(
                "Payment Validation ERROR (#%d): %s | %s | Transaction: %s",
                $userId,
                $e->getMessage(),
                $e->getTraceAsString(),
                collect($transaction)
            ));

            $isValid = false;
        }

        return new JsonResponse([
            'ok'   => $isValid,
            'data' => [
                'product' =>  $request->get('id')
            ]
        ], Response::HTTP_OK);
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  authentication=false,
     *  section="Billing",
     *  description="Catch all payment system logs",
     *  method="POST",
     *  uri="/api/billings/{userId}/log",
     *  tags={
     *      "stable" = "green",
     *      "tested" = "red",
     *  },
     *  parameters={
     *  },
     *  statusCodes={
     *      200="Returned was successful"
     *  }
     * )
     *
     * @Post("/api/billing/{userId}/log")
     *
     * @param int $userId
     * @return JsonResponse
     * @throws \Symfony\Component\Filesystem\Exception\IOException
     */
    public function postLogAction(int $userId): JsonResponse
    {
        $request = $this->requestStack->getCurrentRequest()->request;
        $transaction = $request->all();
        $request->add(['additionalLogData' => ['userId' => $userId]]);

        $this->paymentService
            ->setUser($userId)
            ->setTransaction($transaction)
            ->log();

        $this->temporaryPaymentLoggerForTest($request); // fixme !!!!!

        return new JsonResponse([], Response::HTTP_OK);
    }

    /**
     * //fixme usunąć po testach
     *
     * @param        $request
     * @param string $title
     * @throws \Symfony\Component\Filesystem\Exception\IOException
     */
    private function temporaryPaymentLoggerForTest($request, $title = 'Request'): void
    {
        $fs = new Filesystem();
        $logFilePath = $this->get('kernel')->getRootDir().'/../var/logs/payments.log';

        if (!$fs->exists($logFilePath)) {
            $fs->touch($logFilePath);
        }
        $content = file_get_contents($logFilePath);

        $row = sprintf(
            "%s | $title: %s    \n",
            (new \DateTime())->format('d-m-Y H:i'),
            collect($request)
        );
        $fs->dumpFile($logFilePath, $content.$row);
    }
}
