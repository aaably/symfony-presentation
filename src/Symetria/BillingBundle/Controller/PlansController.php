<?php

namespace Symetria\BillingBundle\Controller;

use Fitatu\ApiServerBundle\Util\Env;
use Fitatu\BillingBundle\Exception\BillingPlanNotFoundException;
use Fitatu\BillingBundle\Service\PlanGroupService;
use Fitatu\BillingBundle\Service\PlanService;
use Fitatu\Local\Repository\Auth\PlanGroupRepository;
use FOS\RestBundle\Controller\Annotations\Get;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symetria\UserBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class PlansController extends AbstractController
{
    /**
     * @var PlanGroupRepository
     */
    private $planGroupRepository;

    /**
     * @var RequestStack
     */
    private $requestStack;
    /**
     * @var PlanService
     */
    private $planService;

    /**
     * @param RequestStack $requestStack
     * @param PlanService $planService
     * @param PlanGroupRepository $planGroupRepository
     */
    public function __construct(RequestStack $requestStack,
        PlanService $planService,
        PlanGroupRepository $planGroupRepository)
    {
        $this->planGroupRepository = $planGroupRepository;
        $this->requestStack = $requestStack;
        $this->planService = $planService;
    }

    /**
     *  Example response<br>
     *
     *      [{
     *          "id": "1231232sda",
     *          "name": "Kolejna grupa",
     *          "duration": 12,
     *          "amount": {
     *                "saving": "120.00 zł",
     *                "monthly": "10.00 zł",
     *                "total": "120.00 zł
     *                "integer": 12000
     *          },
     *          "promoAmount": {
     *                "saving": "144.00 zł",
     *                "monthly": "8.00 zł",
     *                "total": "96.00 zł",
     *                "integer": 9600
     *          },
     *          "message": null,
     *          "isBestseller": true,
     *          "type": "paid subscription",
     *          "currency": "PLN"
     *
     *          },
     *          {
     *          "id": 1231,
     *          "name": "Nowa grupa",
     *          "duration": 1,
     *          "amount": {
     *               "saving": "",
     *               "monthly": "20.00 zł",
     *               "total": "20.00 zł",
     *               "integer": 12000
     *          },
     *          "promoAmount" : null,
     *          "isBestseller": true,
     *          "type": "paid subscription",
     *          "message": null,
     *          "currency": "PLN"
     *      }]
     *
     * @ApiDoc(
     *  resource=true,
     *  authentication=false,
     *  section="Billing",
     *  description="Get list of available plans",
     *  method="GET",
     *  uri="/api/billings/plans",
     *  tags={
     *      "stable" = "green",
     *      "tested" = "green",
     *      "cached" = "green"
     *  },
     *  parameters={
     *     {"name"="locale", "dataType"="string", "required"=false, "description"="Available plans"},
     *     {"name"="type", "dataType"="string", "required"=false, "description"="Plan type e.g. DIET|PREMIUM"},
     *  },
     *  statusCodes={
     *      200="Return plan groups"
     *  }
     * )
     *
     * @Get("/api/billing/plans")
     *
     * @return JsonResponse
     */
    public function plansAction(): JsonResponse
    {
        /** @var Request $request */
        $request = $this->requestStack->getCurrentRequest();
        $locale = $request->get('locale');
        $type = $request->get('type') ?: 'DIET';
        $provider = $request->headers->get('app-os');

        if (empty($locale) && $this->getUser()) {
            $locale = $this->getUser()->getLocale();
        }

        if (empty($locale)) {
            $locale = Env::get('APP_LOCALE');
        }

        $plans = $this->planService->getForLocaleByProvider($locale, $provider, $type);

        return new JsonResponse($plans, Response::HTTP_OK);
    }

    /**
     *  Example response<br>
     *
     *      {
     *          "id": "1231232sda",
     *          "name": "Kolejna grupa",
     *          "duration": 12,
     *          "amount": {
     *                "saving": "120.00 zł",
     *                "monthly": "10.00 zł",
     *                "total": "120.00 zł",
     *                "integer": 12000
     *          },
     *          "promoAmount" : null,
     *          "isBestseller": true,
     *          "type": "paid subscription",
     *          "message": null,
     *          "currency": "PLN"
     *      }
     *
     * @ApiDoc(
     *  resource=true,
     *  authentication=false,
     *  section="Billing",
     *  description="Get list of available plans",
     *  method="GET",
     *  uri="/api/billings/plans/{planId}",
     *  tags={
     *      "stable" = "green",
     *      "tested" = "green",
     *      "cached" = "green"
     *  },
     *  requirements={
     *      {"name"="planId", "dataType"="string", "required"=true, "requirement"="\d+", "description"="Plan group ID"}
     *  },
     *  parameters={
     *     {"name"="locale", "dataType"="string", "required"=false, "description"="If null is given, default locale will be used"},
     *     {"name"="provider", "dataType"="string", "required"=false, "description"="Plan platform.  Available: ANDROID|IOS|WINDOWS|WEB"},
     *  },
     *  statusCodes={
     *      200="Returned was successful",
     *      404="Plan group not found"
     *  }
     * )
     *
     * @Get("/api/billing/plans/{planId}")
     *
     * @param string $planId
     * @return JsonResponse
     * @throws NotFoundHttpException
     */
    public function showAction(string $planId): JsonResponse
    {
        $request = $this->requestStack->getCurrentRequest();
        $locale = $request->get('locale');

        if (empty($locale) && $this->getUser()) {
            $locale = $this->getUser()->getLocale();
        }

        if (empty($locale)) {
            $locale = getenv('APP_LOCALE');
        }
        try {
            $plan = $this->planService->get($planId, $locale);
        } catch (BillingPlanNotFoundException $e) {
            return new JsonResponse(
                $e->getMessage(),
                $e->getStatusCode()
            );
        }

        return new JsonResponse($plan, Response::HTTP_OK);
    }
}
