<?php

namespace Symetria\BillingBundle\Controller;

use FOS\RestBundle\Controller\Annotations\Get;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symetria\SharedBundle\Mapper\PremiumLocaleMapper;
use Symetria\UserBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class LocalesController extends AbstractController
{
    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @param RequestStack $requestStack
     */
    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }

    /**
     *  Example response<br>
     *
     *      [ "nl_NL", "fr_FR"]
     *
     * @ApiDoc(
     *  resource=true,
     *  authentication=true,
     *  section="Billing",
     *  description="Get list of available premium locales",
     *  method="GET",
     *  uri="/api/billings/locales",
     *  tags={
     *      "stable" = "green",
     *      "tested" = "green",
     *  },
     *  parameters={
     *     {"name"="version", "dataType"="string", "required"=false, "description"="App version in format 2.4.17"}
     *  },
     *  statusCodes={
     *      200="Return available locales"
     *  }
     * )
     *
     * @Get("/api/billing/locales")
     *
     * @return JsonResponse
     */
    public function listAction(): JsonResponse
    {
        /** @var Request $request */
        $request = $this->requestStack->getCurrentRequest();
        $version = $request->get('version') ?: $request->headers->get('app-version');

        $locales = PremiumLocaleMapper::get($version);

        return new JsonResponse($locales, Response::HTTP_OK);
    }
}
