<?php

namespace Symetria\BillingBundle\Controller;

use Fitatu\BillingBundle\Provider\BillingProvider;
use Fitatu\SharedUserBundle\Model\Role\RoleInterface;
use Symetria\BillingBundle\Exception\TokenNotProvidedException;
use Symetria\UserBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use FOS\RestBundle\Controller\Annotations\Post;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class ProviderSubscriptionCheckerController extends AbstractController
{
    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @param RequestStack $requestStack
     */
    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  authentication=false,
     *  section="Billing",
     *  description="Get Billing Provider Subscription Response",
     *  method="POST",
     *  uri="/api/billing/{provider}/subscription/{providerProductId}/{token}",
     *  tags={
     *      "stable" = "green",
     *      "tested" = "red",
     *  },
     *  parameters={
     *     {"name"="token", "dataType"="string", "required"=true, "description"="Android transaction token"},
     *  },
     *  statusCodes={
     *      200="Returned was successful"
     *  }
     * )
     *
     * @Post("/api/billing/{provider}/subscription/{providerProductId}")
     *
     * @param string $provider
     * @param string $providerProductId
     * @return JsonResponse
     */
    public function postSubscriptionAction(string $provider, string $providerProductId)
    {
        if (!$this->isGranted(RoleInterface::ROLE_ADMIN)) {
            throw new AccessDeniedHttpException('Access denied.');
        }

        $request = $this->requestStack->getCurrentRequest()->request;

        $token = $request->get('token');

        if (empty($token)) {
            throw new TokenNotProvidedException();
        }

        $billingProvider = BillingProvider::get(strtoupper($provider));

        return new JsonResponse(
            $billingProvider::getRawSubscriptionData($providerProductId, $token)
        );
    }
}