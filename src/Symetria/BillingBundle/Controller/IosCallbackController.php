<?php

namespace Symetria\BillingBundle\Controller;

use Fitatu\BillingBundle\Exception\SubscriptionNotFoundException;
use Fitatu\BillingBundle\Model\IosNotification;
use Fitatu\BillingBundle\Provider\IosBillingProvider;
use Fitatu\DatabaseBundle\Entity\Auth\Subscription;
use Fitatu\Local\Repository\Auth\PaymentRepository;
use FOS\RestBundle\Controller\Annotations\Post;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symetria\BillingBundle\Handler\Subscription\CancelUserSubscriptionCommand;
use Symetria\BillingBundle\Handler\Subscription\ExtendUserSubscriptionCommand;
use Symetria\BillingBundle\Service\IosPaymentEventBroadcastService;
use Symetria\UserBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use League\Tactician\CommandBus;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class IosCallbackController extends AbstractController
{
    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @var CommandBus
     */
    private $commandBus;

    /**
     * @var PaymentRepository
     */
    private $paymentRepository;

    /**
     * @var IosPaymentEventBroadcastService
     */
    private $iosPaymentEventBroadcastService;

    /**
     * @param RequestStack                    $requestStack
     * @param PaymentRepository               $paymentRepository
     * @param IosPaymentEventBroadcastService $iosPaymentEventBroadcastService
     * @param CommandBus                      $commandBus
     * @internal param PaymentService $paymentService
     */
    public function __construct(
        RequestStack $requestStack,
        PaymentRepository $paymentRepository,
        IosPaymentEventBroadcastService $iosPaymentEventBroadcastService,
        CommandBus $commandBus
    ) {
        $this->requestStack = $requestStack;
        $this->commandBus = $commandBus;
        $this->paymentRepository = $paymentRepository;
        $this->iosPaymentEventBroadcastService = $iosPaymentEventBroadcastService;
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  authentication=false,
     *  section="Billing",
     *  description="Catch iOS provider callbacks",
     *  method="POST",
     *  uri="/api/billings/ios/callbacks",
     *  tags={
     *      "stable" = "green",
     *      "tested" = "red",
     *  },
     *  statusCodes={
     *      200="Response ok",
     *      404="Subscription not found"
     *  }
     * )
     *
     * @Post("/api/billing/ios/callbacks")
     *
     * @throws SubscriptionNotFoundException
     * @return JsonResponse
     */
    public function postIosCallbackAction()
    {
        /** @var Request $request */
        $request = $this->requestStack->getCurrentRequest()->request;

        /** @var IosNotification $notification */
        $notification = IosBillingProvider::consumeNotification($request->all());

        $payment = $this->paymentRepository->findOneBy([
            'providerId' => $notification->getOriginalTransactionId()
        ]);

        if (empty($payment) && !$payment->getSubscription() instanceof Subscription) {
            throw new SubscriptionNotFoundException($notification->getOriginalTransactionId());
        }
        $subscription = $payment->getSubscription();

        $command = new ExtendUserSubscriptionCommand(
            $subscription,
            $notification->getExpiresDate()
        );

        $this->commandBus->handle($command);

        if (IosBillingProvider::isCancelled($notification)) {
            $command = new CancelUserSubscriptionCommand(
                $subscription,
                $notification->getCancellationDate()
            );

            $this->commandBus->handle($command);
        }

        $this->iosPaymentEventBroadcastService->broadcast($request->all());

        return new JsonResponse(['ok' => true]);
    }
}
