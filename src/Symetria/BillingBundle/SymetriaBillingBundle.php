<?php

namespace Symetria\BillingBundle;

use Symetria\BillingBundle\DependencyInjection\SymetriaBillingExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class SymetriaBillingBundle extends Bundle
{
    /**
     * @return SymetriaBillingExtension
     */
    public function getContainerExtension()
    {
        return new SymetriaBillingExtension();
    }
}
