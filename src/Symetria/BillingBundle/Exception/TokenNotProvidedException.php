<?php

namespace Symetria\BillingBundle\Exception;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class TokenNotProvidedException extends HttpException
{
    private const MESSAGE = 'Transaction token not provided';

    public function __construct()
    {
        parent::__construct(
            Response::HTTP_UNPROCESSABLE_ENTITY,
            static:: MESSAGE
        );
    }
}