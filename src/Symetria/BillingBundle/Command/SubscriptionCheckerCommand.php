<?php

namespace Symetria\BillingBundle\Command;

use Fitatu\BillingBundle\Provider\BillingProvider;
use Fitatu\DatabaseBundle\Entity\Auth\Payment;
use Fitatu\DatabaseBundle\Entity\Auth\Subscription;
use Fitatu\Local\Repository\Auth\PaymentRepository;
use Fitatu\Local\Repository\Auth\SubscriptionRepository;
use League\Tactician\CommandBus;
use Psr\Log\LoggerInterface;
use Symetria\BillingBundle\Handler\Subscription\CancelUserSubscriptionCommand;
use Symetria\BillingBundle\Handler\Subscription\ExtendUserSubscriptionCommand;
use Symetria\SecurityBundle\Service\RoleHierarchyService;
use Symetria\SharedBundle\Mapper\UserSystemMapper;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class SubscriptionCheckerCommand extends ContainerAwareCommand
{
    const COMMAND = 'fitatu:subscription:check';

    const PROVIDERS = [
        UserSystemMapper::ANDROID,
    ];

    /**
     * @var SubscriptionRepository
     */
    protected $subscriptionRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var PaymentRepository
     */
    private $paymentRepository;

    /**
     * @var RoleHierarchyService
     */
    private $roleHierarchyService;

    /**
     * @var CommandBus
     */
    private $commandBus;

    /**
     * @param SubscriptionRepository $subscriptionRepository
     * @param PaymentRepository      $paymentRepository
     * @param RoleHierarchyService   $roleHierarchyService
     * @param CommandBus             $commandBus
     * @param LoggerInterface        $logger
     */
    public function __construct(
        SubscriptionRepository $subscriptionRepository,
        PaymentRepository $paymentRepository,
        RoleHierarchyService $roleHierarchyService,
        CommandBus $commandBus,
        LoggerInterface $logger
    ) {
        parent::__construct();

        $this->subscriptionRepository = $subscriptionRepository;
        $this->logger = $logger;
        $this->paymentRepository = $paymentRepository;
        $this->roleHierarchyService = $roleHierarchyService;
        $this->commandBus = $commandBus;
    }

    public function configure()
    {
        $this->setName(static::COMMAND)
             ->setDescription('Check active subscription provider status')
             ->setHelp('This command allows to verify if subscription is still valid on provider side');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @return int
     */
    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $io->title('Verify active subscriptions');
        $successCount = 0;
        $errorCount = 0;

        foreach (static::PROVIDERS as $provider) {
            $payments = $this->paymentRepository->findWithActiveSubscriptionsByProvider(
                BillingProvider::ANDROID_PROVIDER_KEY
            );

            $count = count($payments);

            $io->text(sprintf(
                '%s: Active subscriptions %d',
                $provider,
                $count
            ));
            $io->newLine();

            $io->progressStart($count);

            foreach ($payments as $payment) {
                try {
                    $this->handleSubscription($payment);
                    ++$successCount;
                } catch (\Exception $e) {
                    $this->logError($payment, $e);
                    ++$errorCount;
                }

                $io->progressAdvance();
            }

            $io->progressFinish();

            if ($errorCount) {
                $io->warning('Total errors: '.$errorCount);
            }
            if ($successCount) {
                $io->success('Total subscriptions cancelled: '.$successCount);
            }
        }

        return 0;
    }

    /**
     * @param Payment $payment
     * @return Subscription
     */
    private function handleSubscription(Payment $payment): Subscription
    {
        $billingProvider = BillingProvider::getByTransactionType(
            $payment->getProvider()
        );

        /** @var \Google_Service_AndroidPublisher_SubscriptionPurchase $subscription */
        $subscription = $billingProvider::get($payment);

        $cancelledAt = $this->getCancelledAt($subscription);

        if ($billingProvider::isCancelled($subscription) && $cancelledAt < new \DateTime()) {
            $command = new CancelUserSubscriptionCommand(
                $payment->getSubscription(),
                $cancelledAt,
                $subscription->getCancelReason()
            );

            $subscription = $this->commandBus->handle($command);

            return $this->logInfo($payment, $subscription);
        }

        $command = new ExtendUserSubscriptionCommand(
            $payment->getSubscription(),
            new \DateTime('@'.round($subscription->getExpiryTimeMillis() / 1000))
        );

        return $this->commandBus->handle($command);
    }

    /**
     * @param \Google_Service_AndroidPublisher_SubscriptionPurchase $subscription
     * @return \DateTime
     */
    private function getCancelledAt($subscription)
    {
        if ($time = $subscription->getUserCancellationTimeMillis()) {
            return new \DateTime('@'.round($time / 1000));
        }

        return new \DateTime();
    }

    /**
     * @param Payment    $payment
     * @param \Exception $e
     */
    private function logError(Payment $payment, \Exception $e): void
    {
        $this->logger->critical(sprintf(
            'Failed Cancelling Subscription #%d (userId: %d, message: %s, stacktrace: %s)',
            $payment->getSubscription()->getId(),
            $payment->getUser()->getId(),
            $e->getMessage(),
            $e->getTraceAsString()
        ));
    }

    /**
     * @param Payment      $payment
     * @param Subscription $subscription
     */
    private function logInfo(Payment $payment, Subscription $subscription)
    {
        $this->logger->info(sprintf(
            'Cancelled Subscription #%d (userId: %d, cancelledAt: %s)',
            $payment->getSubscription()->getId(),
            $payment->getUser()->getId(),
            $subscription->getCancelledAt()
        ));
    }
}
