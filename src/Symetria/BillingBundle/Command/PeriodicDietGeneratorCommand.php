<?php

namespace Symetria\BillingBundle\Command;

use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Fitatu\DatabaseBundle\Entity\Auth\Settings\DietGenerator;
use Fitatu\Local\Repository\Auth\SubscriptionRepository;
use Psr\Log\LoggerInterface;
use Symetria\UserBundle\Service\DietGenerationQueueService;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * @author Lukasz Domanski
 * @copyright Fitatu Sp. z o.o.
 */
class PeriodicDietGeneratorCommand extends ContainerAwareCommand
{
    const COMMAND = 'symetria:diet-generator:periodic:run';
    const DESCRIPTION = 'Report periodic diets to queue generator';
    const BATCH_SIZE = 20;

    /**
     * @var array
     */
    private $stats = ['cnt' => 0, 'ids' => []];

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var SubscriptionRepository
     */
    private $subscriptionRepository;

    /**
     * @var DietGenerationQueueService
     */
    private $dietGeneratorQueueService;

    /**
     * @var string
     */
    private $locale;

    /**
     * @param EntityManagerInterface     $entityManager
     * @param LoggerInterface            $logger
     * @param SubscriptionRepository     $subscriptionRepository
     * @param DietGenerationQueueService $dietGeneratorQueueService
     * @param string                     $locale
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        LoggerInterface $logger,
        SubscriptionRepository $subscriptionRepository,
        DietGenerationQueueService $dietGeneratorQueueService,
        string $locale
    ) {
        parent::__construct();

        $this->entityManager = $entityManager;
        $this->logger = $logger;
        $this->subscriptionRepository = $subscriptionRepository;
        $this->dietGeneratorQueueService = $dietGeneratorQueueService;
        $this->locale = $locale;
    }

    protected function configure()
    {
        $this
            ->setName(static::COMMAND)
            ->setDescription(static::DESCRIPTION)
            ->addOption('--dry-run', null, InputOption::VALUE_NONE, 'Don\'t send any events');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->entityManager->getConfiguration()->setSQLLogger(null);

        $dryRun = $input->getOption('dry-run');

        $io = new SymfonyStyle($input, $output);
        $io->title('Report periodic user diets to queue');

        $this->reportDietToQueue($dryRun, $io);
        $this->flushChanges();
        $this->displaySummary($io);
        $this->log();

        $io->success('Finished');

        return 0;
    }

    /**
     * @param bool         $dryRun
     * @param SymfonyStyle $io
     */
    private function reportDietToQueue(bool $dryRun, SymfonyStyle $io)
    {
        $cnt = $this->subscriptionRepository->countAwaitingToReport($this->locale);
        $subscriptions = $this->subscriptionRepository->getIterableAwaitingToReport($this->locale);

        if ($cnt === 0) {
            return;
        }

        $io->text(sprintf('Report %d diet(s) to DietGenerator', $cnt));

        if (true === $dryRun) {
            $io->note('This is a dry run. No events will be sent.');
        }

        $i = 0;

        $io->progressStart($cnt);

        foreach ($subscriptions as $item) {

            $userId = (int)array_pop($item)['userId'];

            if (++$i % static::BATCH_SIZE == 0) {
                $this->flushChanges();
            }

            try {
                if (false === $dryRun) {
                    list(, , $requestAt) = DietGenerator::getNextPeriods();
                    $this->dietGeneratorQueueService->send($userId, ['requestAt' => $requestAt]);
                }
                $this->stats['ids'][] = $userId;

            } catch (Exception $e) {

                $io->error(sprintf('Could not send queue to DietGenerator for User %d: %s', $userId, $e->getMessage()));
            }

            $io->progressAdvance();

            $this->stats['cnt']++;
        }

        $io->progressFinish();
    }

    private function flushChanges()
    {
        $this->entityManager->flush();
        $this->entityManager->clear();
    }

    /**
     * @param SymfonyStyle $io
     */
    private function displaySummary(SymfonyStyle $io)
    {
        $io->newLine();
        $io->text(sprintf('Reported %d diet(s) to diet generator', $this->stats['cnt']));
        $io->text(sprintf('User ids: %s', join(',', $this->stats['ids'])));
        $io->newLine();
    }

    private function log()
    {
        $this->logger->info(sprintf('Reported periodic diet generator, ids: %s', join(',', $this->stats['ids'])));
    }
}
