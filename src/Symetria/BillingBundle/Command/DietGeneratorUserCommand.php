<?php

namespace Symetria\BillingBundle\Command;

use DateTime;
use DateTimeInterface;
use Exception;
use Fitatu\DatabaseBundle\Entity\Auth\Settings\DietGenerator;
use Fitatu\DatabaseBundle\Entity\Auth\User\User;
use Fitatu\Local\Repository\Auth\User\UserRepository;
use Symetria\SharedBundle\Event\DietGenerator\DietGeneratorQueueEvent;
use Symetria\UserBundle\Exception\PremiumServices\UserNotFoundException;
use Symetria\UserBundle\Service\DietGenerationQueueService;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * @author    Lukasz Domanski
 * @copyright Fitatu Sp. z o.o.
 */
class DietGeneratorUserCommand extends ContainerAwareCommand
{
    const COMMAND = 'fitatu:diet-generator:user:run';
    const DESCRIPTION = 'Report one user to diet generator queue';

    /**
     * @var DietGenerationQueueService
     */
    private $dietGeneratorQueueService;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var array
     */
    private $options = [];

    /**
     * @param DietGenerationQueueService $dietGeneratorQueueService
     * @param UserRepository             $userRepository
     */
    public function __construct(DietGenerationQueueService $dietGeneratorQueueService, UserRepository $userRepository)
    {
        parent::__construct();

        $this->dietGeneratorQueueService = $dietGeneratorQueueService;
        $this->userRepository = $userRepository;
    }

    protected function configure()
    {
        $this
            ->setName(static::COMMAND)
            ->setDescription(static::DESCRIPTION)
            ->addOption('--dry-run', null, InputOption::VALUE_NONE, 'Don\'t send any events')
            ->addOption('user', 'u', InputOption::VALUE_OPTIONAL)
            ->addOption('fromDate', 'f', InputOption::VALUE_OPTIONAL)
            ->addOption('toDate', 't', InputOption::VALUE_OPTIONAL)
            ->setHelp('Example: fitatu:diet-generator:user:run -u 123 -f 2018-01-01 -t 2018-01-07');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $dryRun = $input->getOption('dry-run');

        $io = new SymfonyStyle($input, $output);
        $io->title('Report user to diet generator queue');

        list($userId, $fromDate, $toDate) = $this->ask($io, $input);

        try {
            $this->options = [
                'fromDate' => new DateTime($fromDate),
                'toDate'   => new DateTime($toDate),
            ];
        } catch (Exception $e) {
            $io->error('User option is required');

            return 0;
        }

        if (empty($userId)) {
            $io->error('User option is required');

            return 0;
        }

        $event = $this->reportDietToQueue($userId, $dryRun, $io);
        $this->displaySummary($output, $event);

        $io->success('Finished');

        return 0;
    }

    /**
     * @param SymfonyStyle   $io
     * @param InputInterface $input
     * @return array
     */
    private function ask(SymfonyStyle $io, InputInterface $input): array
    {

        if (empty($user = $input->getOption('user'))) {
            $user = $io->ask('User name or id');
        }
        $userId = $this->getUserId($user);

        /** @var DateTimeInterface $defaultFromDate */
        /** @var DateTimeInterface $defaultToDate */
        list(, $defaultToDate) = DietGenerator::getNextPeriods();
        $defaultFromDate = new DateTime();

        $fromDate = $this->askDate($io, $input, $defaultFromDate, 'fromDate');
        $toDate = $this->askDate($io, $input, $defaultToDate, 'toDate');

        return [$userId, $fromDate, $toDate];
    }

    /**
     * @param SymfonyStyle      $io
     * @param InputInterface    $input
     * @param DateTimeInterface $defaultDate
     * @param string            $dateDirection
     * @return string
     */
    private function askDate(
        SymfonyStyle $io,
        InputInterface $input,
        DateTimeInterface $defaultDate,
        string $dateDirection
    ) {
        if (empty($date = $input->getOption($dateDirection))) {
            $date = $io->ask($dateDirection, $defaultDate->format('Y-m-d'));
        }

        return (string)$date;
    }

    /**
     * @param string $userNameId
     *
     * @return int
     *
     * @throws Exception
     */
    private function getUserId(string $userNameId): int
    {
        // TODO Change this when UUID is implemented
        if ((int)$userNameId === 0) {
            $user = $this->userRepository->findByUsernameOrEmail($userNameId);
        } else {
            $user = $this->userRepository->find($userNameId);
        }

        if (!$user instanceof User) {
            throw new UserNotFoundException($userNameId);
        }

        return $user->getId();
    }

    /**
     * @param int          $userId
     * @param bool         $dryRun
     * @param SymfonyStyle $io
     *
     * @return DietGeneratorQueueEvent
     */
    private function reportDietToQueue(int $userId, bool $dryRun, SymfonyStyle $io): DietGeneratorQueueEvent
    {
        if (true === $dryRun) {
            $io->note('This is a dry run. No events will be sent.');
        }

        return $this->dietGeneratorQueueService->send($userId, $this->options, $dryRun);
    }

    /**
     * @param OutputInterface         $output
     * @param DietGeneratorQueueEvent $event
     */
    private function displaySummary(OutputInterface $output, DietGeneratorQueueEvent $event)
    {
        $table = new Table($output);

        $table
            ->setHeaders(['UserId', 'Locale', 'From', 'To'])
            ->setRows(
                [
                    [
                        $event->userId,
                        $event->locale,
                        $event->dateRange->getFrom()->format('Y-m-d'),
                        $event->dateRange->getTo()->format('Y-m-d'),
                    ],
                ]
            );

        $table->render();
    }
}
