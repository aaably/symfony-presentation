<?php

namespace Symetria\BillingBundle\Command;

use Doctrine\ORM\EntityManagerInterface;
use Fitatu\BillingBundle\Service\SubscriptionService;
use Fitatu\DatabaseBundle\Entity\Auth\Subscription;
use Fitatu\Local\Repository\Auth\SubscriptionRepository;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Throwable;

/**
 * @author    Lukasz Domanski
 * @copyright Fitatu Sp. z o.o.
 */
class ActivateUserSubscriptionCommand extends ContainerAwareCommand
{
    const COMMAND = 'symetria:subscription:activate';
    const BATCH_SIZE = 20;

    /**
     * @var int
     */
    private $stats = 0;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var SubscriptionRepository
     */
    private $subscriptionRepository;

    /**
     * @var SubscriptionService
     */
    private $subscriptionService;

    /**
     * @param EntityManagerInterface $entityManager
     * @param LoggerInterface        $logger
     * @param SubscriptionRepository $subscriptionRepository
     * @param SubscriptionService    $subscriptionService
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        LoggerInterface $logger,
        SubscriptionRepository $subscriptionRepository,
        SubscriptionService $subscriptionService
    ) {
        parent::__construct();

        $this->entityManager = $entityManager;
        $this->logger = $logger;
        $this->subscriptionRepository = $subscriptionRepository;
        $this->subscriptionService = $subscriptionService;
    }

    protected function configure()
    {
        $this
            ->setName(self::COMMAND)
            ->setDescription('Activates user subscriptions')
            ->addOption('user', null, InputOption::VALUE_OPTIONAL, 'Type UserId')
            ->setHelp('Set user as option to activate user subscriptions');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->entityManager->getConfiguration()->setSQLLogger(null);

        $io = new SymfonyStyle($input, $output);

        $userId = $input->getOption('user');
        $this->activateSubscriptions($io, $userId);

        $this->flushChanges();
        $this->displaySummary($io);

        return 0;
    }

    /**
     * @param SymfonyStyle $io
     * @param int|null     $userId
     *
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    private function activateSubscriptions(SymfonyStyle $io, int $userId = null)
    {
        $io->title('Activate user subscriptions');

        $cnt = $this->subscriptionRepository->countAwaitingToActivate($userId);
        $subscriptions = $this->subscriptionRepository->getIterableAwaitingToActivate($userId);
        $i = 0;

        $io->progressStart($cnt);

        foreach ($subscriptions as $subscription) {
            $subscription = array_pop($subscription);

            if (++$i % static::BATCH_SIZE == 0) {
                $this->flushChanges();
            }

            try {
                /** @var Subscription $subscription */
                $this->subscriptionService->activate($subscription->getId());
                $this->logger->info(
                    sprintf(
                        'Subscription activate: user #%d, id #%d',
                        $subscription->getUser()->getId(),
                        $subscription->getId()
                    )
                );
            } catch (Throwable $e) {
                $this->logger->warning(
                    sprintf('Subscription activate ERROR: user #%d, id #%d',
                        $subscription->getUser()->getId(),
                        $subscription->getId()));
            }

            $io->progressAdvance();

            $this->stats++;
        }

        $io->progressFinish();
    }

    private function flushChanges()
    {
        $this->entityManager->flush();
        $this->entityManager->clear();
    }

    /**
     * @param SymfonyStyle $io
     */
    private function displaySummary(SymfonyStyle $io)
    {
        $io->newLine();
        $io->text(sprintf('Activated subscriptions: %d', $this->stats));
        $io->newLine();
    }
}
