<?php

namespace Symetria\BillingBundle\Command;

use Fitatu\BillingBundle\Service\SubscriptionService;
use Fitatu\Local\Repository\Auth\RoleRepository;
use Fitatu\Local\Repository\Auth\Settings\DietGeneratorRepository;
use Fitatu\Local\Repository\Auth\User\UserRepository;
use Fitatu\SharedUserBundle\Model\Role\RoleInterface;
use Symetria\UserBundle\Exception\PremiumServices\UserNotFoundException;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class UnsubscribeUserCommand extends ContainerAwareCommand
{
    private const COMMAND = 'fitatu:user:unsubscribe';

    /**
     * @var SubscriptionService
     */
    private $subscriptionService;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var DietGeneratorRepository
     */
    private $dietGeneratorRepository;
    /**
     * @var RoleRepository
     */
    private $roleRepository;

    /**
     * @param SubscriptionService     $subscriptionService
     * @param UserRepository          $userRepository
     * @param RoleRepository          $roleRepository
     * @param DietGeneratorRepository $dietGeneratorRepository
     */
    public function __construct(
        SubscriptionService $subscriptionService,
        UserRepository $userRepository,
        RoleRepository $roleRepository,
        DietGeneratorRepository $dietGeneratorRepository
    ) {
        parent::__construct();

        $this->subscriptionService = $subscriptionService;
        $this->userRepository = $userRepository;
        $this->dietGeneratorRepository = $dietGeneratorRepository;
        $this->roleRepository = $roleRepository;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName(static::COMMAND)
            ->setDescription('Unsubscribe user/s')
            ->addArgument('user', InputArgument::REQUIRED, 'User ids separated by comma or email address')
            ->setHelp('This command removes subscriptions and premium privileges.');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $io->title('Unsubscribe User');

        $users = $this->prepareUsers($input);

        $successCount = 0;
        $failCount = 0;
        foreach ($users as $user) {
            try {
                $roles = $this->roleRepository->findBy([
                    'role' => [
                        RoleInterface::ROLE_PREMIUM,
                        RoleInterface::ROLE_PREMIUM_AVAILABLE
                    ]
                ]);
                foreach ($roles as $role) {
                    $user->removeRole($role);
                }
                $this->userRepository->persist($user);
                $this->dietGeneratorRepository->deleteByUser($user);

                ++$successCount;
            } catch (\Exception $e) {
                $io->error($e->getMessage());
                ++$failCount;
            }
        }

        if ($failCount) {
            $io->warning('Total errors: '.$failCount);
        }
        if ($successCount) {
            $io->success('Total users subscribed: '.$successCount);
        }

        return 0;
    }

    /**
     * @param InputInterface $input
     * @return array
     */
    private function prepareUsers(InputInterface $input): array
    {
        $users = $input->getArgument('user');

        if (strpos($users, '@') !== false) {
            $user = $this->userRepository->findByUsernameOrEmail($users);

            if (empty($user)) {
                throw new UserNotFoundException($users);
            }

            return [$user];
        }

        if (strpos($users, ',') !== false) {
            $users = explode(',', $users);
        }

        return $this->userRepository->findBy([
            'id' => $users
        ]);
    }
}
