<?php

namespace Symetria\BillingBundle\Command;

use Fitatu\BillingBundle\Service\SubscriptionService;
use Fitatu\Local\Repository\Auth\PlanGroupRepository;
use Fitatu\Local\Repository\Auth\User\UserRepository;
use Symetria\SharedBundle\Event\Subscription\SubscribeUserToPlanEvent;
use Symetria\SharedBundle\EventBus\Dispatcher\RabbitMQEventDispatcherBridge;
use Symetria\UserBundle\Exception\PremiumServices\UserNotFoundException;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class SubscribeUserCommand extends ContainerAwareCommand
{
    const COMMAND = 'fitatu:user:subscribe';

    /**
     * @var SubscriptionService
     */
    private $subscriptionService;

    /**
     * @var PlanGroupRepository
     */
    private $planGroupRepository;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var RabbitMQEventDispatcherBridge
     */
    private $dispatcher;

    /**
     * @param SubscriptionService           $subscriptionService
     * @param PlanGroupRepository           $planGroupRepository
     * @param UserRepository                $userRepository
     * @param RabbitMQEventDispatcherBridge $dispatcher
     */
    public function __construct(
        SubscriptionService $subscriptionService,
        PlanGroupRepository $planGroupRepository,
        UserRepository $userRepository,
        RabbitMQEventDispatcherBridge $dispatcher
    ) {
        parent::__construct();

        $this->subscriptionService = $subscriptionService;
        $this->planGroupRepository = $planGroupRepository;
        $this->userRepository = $userRepository;
        $this->dispatcher = $dispatcher;
    }

    public function configure()
    {
        $this->setName(static::COMMAND)
            ->setDescription('Subscribe user/s to given subscription')
            ->addArgument('user', InputArgument::REQUIRED, 'User ids separated by comma or email address')
            ->addOption('plan', 'p', InputOption::VALUE_OPTIONAL)
            ->addOption('planGroup', 'g', InputOption::VALUE_OPTIONAL)
            ->setHelp('This command allows to create user subscriptions');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @return int
     */
    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $io->title('Subscribe User');

        $users = $this->prepareUsers($input);
        $planGroupId = $this->getPlanGroupId($input, $output);

        $successCount = 0;
        $failCount = 0;
        foreach ($users as $userId) {
            try {
                $user = $this->userRepository->find($userId);
                $plan = $this->subscriptionService->getPlanFromPlanGroup($planGroupId, $user->getLocale());
                $event = new SubscribeUserToPlanEvent($userId, $plan->getId());

                $this->dispatcher->dispatch($event);

                ++$successCount;
            } catch (\Exception $e) {
                $io->error($e->getMessage());
                ++$failCount;
            }
        }
        if ($failCount) {
            $io->warning('Total errors: '.$failCount);
        }
        if ($successCount) {
            $io->success('Total users subscribed: '.$successCount);
        }

        return 0;
    }

    /**
     * @param InputInterface $input
     * @return array
     */
    private function prepareUsers(InputInterface $input): array
    {
        $users = $input->getArgument('user');

        if (strpos($users, ',') !== false) {
            return explode(',', $users);
        }

        if (strpos($users, '@') !== false) {
            $user = $this->userRepository->findByUsernameOrEmail($users);

            if (empty($user)) {
                throw new UserNotFoundException($users);
            }

            return [$user->getId()];
        }

        return [$users];
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @return int
     */
    private function getPlanGroupId(InputInterface $input, OutputInterface $output): int
    {
        if ($planId = $input->getOption('plan')) {
            return intval($planId);
        }

        return (int)$this->askForPlanGroup($input, $output);
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @return int
     */
    private function askForPlanGroup(InputInterface $input, OutputInterface $output): int
    {
        if ($planGroupId = $input->getOption('planGroup')) {
            return intval($planGroupId);
        }

        $planGroups = collect($this->planGroupRepository->findAll())->map->toArray();

        $helper = $this->getHelper('question');
        $question = new ChoiceQuestion(
            'Please select a Plan Group',
            collect($planGroups)->pluck('name')->toArray()
        );
        $planGroupName = $helper->ask($input, $output, $question);

        $selectGroup = function ($group) use ($planGroupName) {
            return $group['name'] == $planGroupName;
        };

        return collect($planGroups)->filter($selectGroup)->pluck('id')->first();
    }
}
