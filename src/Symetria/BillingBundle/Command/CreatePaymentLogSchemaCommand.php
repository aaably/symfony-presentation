<?php

namespace Symetria\BillingBundle\Command;

use Fitatu\Cassandra\Command\AbstractCassandraSchemaCommand;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class CreatePaymentLogSchemaCommand extends AbstractCassandraSchemaCommand
{
    const COMMAND = 'create:payment-log';
    const COMMAND_TITLE = 'Create Payment Log Table';

    const TABLE_NAME = 'payment_log';
    const TABLE_FIELDS = [
        'user_id'          => 'int',
        'currency'         => 'varchar',
        'state'            => 'varchar',
        'price'            => 'int',
        'type'             => 'varchar',
        'product_id'       => 'varchar',
        'transaction_id'   => 'bigint',
        'transaction_path' => 'varchar',
        'product_type'     => 'varchar',
        'valid'            => 'boolean',
        'updated_at'       => 'timestamp',
        'created_at'       => 'timestamp',
    ];

    /**
     * @example
     *  $this->cassandra
     *       ->addTable('tableName)
     *       ->setPrimaryKey('id')
     *       ->withFields([ 'name' => 'type' ])
     *       ->persist();
     *
     * @return void
     */
    public function createTable()
    {
        $this->cassandra
            ->addTable(static::TABLE_NAME)
            ->setPrimaryKey('id', 'uuid')
            ->withFields(static::TABLE_FIELDS)
            ->persist();
    }
}
