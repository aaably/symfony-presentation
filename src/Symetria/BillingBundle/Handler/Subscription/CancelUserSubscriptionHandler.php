<?php

namespace Symetria\BillingBundle\Handler\Subscription;

use Doctrine\ORM\EntityManagerInterface;
use Fitatu\ApiClientBundle\Service\UserCacheCleanerService;
use Fitatu\DatabaseBundle\Entity\Auth\Subscription;
use Fitatu\Local\Repository\Auth\SubscriptionRepository;
use Psr\Log\LoggerInterface;
use Symetria\SecurityBundle\Service\RolesService;
use Symetria\SharedBundle\EventBus\EventCollectingHandlerInterface;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class CancelUserSubscriptionHandler implements EventCollectingHandlerInterface
{
    /**
     * @var array
     */
    private $events = [];

    /**
     * @var SubscriptionRepository
     */
    private $subscriptionRepository;

    /**
     * @var RolesService
     */
    private $rolesService;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var UserCacheCleanerService
     */
    private $userCacheCleanerService;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param EntityManagerInterface  $entityManager
     * @param SubscriptionRepository  $subscriptionRepository
     * @param RolesService            $rolesService
     * @param LoggerInterface         $logger
     * @param UserCacheCleanerService $userCacheCleanerService
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        SubscriptionRepository $subscriptionRepository,
        RolesService $rolesService,
        LoggerInterface $logger,
        UserCacheCleanerService $userCacheCleanerService
    ) {
        $this->subscriptionRepository = $subscriptionRepository;
        $this->rolesService = $rolesService;
        $this->entityManager = $entityManager;
        $this->userCacheCleanerService = $userCacheCleanerService;
        $this->logger = $logger;
    }

    /**
     * @param CancelUserSubscriptionCommand $command
     * @return Subscription
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function handle(CancelUserSubscriptionCommand $command)
    {
        $subscription = $command->getSubscription();
        $user = $subscription->getUser();

        try {
            $this->entityManager->getConnection()->beginTransaction();
            // check if has other subscriptions
            $subscriptions = $this->subscriptionRepository->findActiveForUser($user->getId());

            // set cancelled_at -> date from microtime
            // set cancelReason from provider
            $subscription
                ->setCancelledAt($command->getCancelledAt())
                ->setCancelReason($command->getCancelReason());

            // take back premium roles
            $this->rolesService->removePremiumRoles($user);

            // if yes, give him premium roles for propper subscription
            if (count($subscriptions) > 1) {
                $subscription = $this->getRemainingSubscription($command, $subscriptions);

                if (!empty($subscription)) {
                    $this->rolesService->assignRoles($user, $subscription['roles']);
                }
            }

            $this->subscriptionRepository->persist($subscription);
            $this->entityManager->commit();

        } catch (\Exception $e) {
            $this->entityManager->getConnection()->rollBack();
            $this->log($command->getSubscription(), $e);
        }

        return $subscription;
    }

    /**
     * @return array
     */
    public function getEvents(): array
    {
        return $this->events;
    }

    /**
     * @param Subscription $subscripthion
     * @param \Exception     $e
     */
    private function log(Subscription $subscripthion, $e)
    {
        $this->logger->critical(
            sprintf(
                'Error while cancelling subscription (%d) for user %s; error message: "%s"; stacktrace: %s',
                $subscripthion->getId(),
                $subscripthion->getUser()->getId(),
                $e->getMessage(),
                $e->getTraceAsString()
            )
        );
    }

    /**
     * @param CancelUserSubscriptionCommand $command
     * @param                               $subscriptions
     * @return array
     */
    private function getRemainingSubscription(CancelUserSubscriptionCommand $command, $subscriptions)
    {
        $subscription = collect($subscriptions)
            ->filter(function ($subscription) use ($command) {
                return $subscription->getId() != $command->getSubscription()->getId();
            })
            ->map(function ($subscription) {
                return [
                    'id'    => $subscription->getId(),
                    'roles' => $subscription->getPlan()->getPlanGroup()->getRoles(),
                ];
            })
            ->first();

        return $subscription;
    }
}
