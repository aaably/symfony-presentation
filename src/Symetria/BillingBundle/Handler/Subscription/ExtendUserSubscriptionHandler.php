<?php

namespace Symetria\BillingBundle\Handler\Subscription;

use Doctrine\ORM\EntityManagerInterface;
use Fitatu\ApiClientBundle\Service\UserCacheCleanerService;
use Fitatu\DatabaseBundle\Entity\Auth\Subscription;
use Fitatu\Local\Repository\Auth\SubscriptionRepository;
use Psr\Log\LoggerInterface;
use Symetria\SecurityBundle\Service\RolesService;
use Symetria\SharedBundle\EventBus\EventCollectingHandlerInterface;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class ExtendUserSubscriptionHandler implements EventCollectingHandlerInterface
{
    /**
     * @var array
     */
    private $events = [];

    /**
     * @var SubscriptionRepository
     */
    private $subscriptionRepository;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var UserCacheCleanerService
     */
    private $userCacheCleanerService;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var RolesService
     */
    private $rolesService;

    /**
     * @param SubscriptionRepository  $subscriptionRepository
     * @param EntityManagerInterface  $entityManager
     * @param UserCacheCleanerService $userCacheCleanerService
     * @param LoggerInterface         $logger
     * @param RolesService            $rolesService
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        SubscriptionRepository $subscriptionRepository,
        RolesService $rolesService,
        LoggerInterface $logger,
        UserCacheCleanerService $userCacheCleanerService
    ) {
        $this->subscriptionRepository = $subscriptionRepository;
        $this->entityManager = $entityManager;
        $this->userCacheCleanerService = $userCacheCleanerService;
        $this->logger = $logger;
        $this->rolesService = $rolesService;
    }

    /**
     * @param ExtendUserSubscriptionCommand $command
     * @return Subscription
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function handle(ExtendUserSubscriptionCommand $command)
    {
        $subscription = $command->getSubscription();
        $user = $subscription->getUser();

        try {
            $this->entityManager->getConnection()->beginTransaction();

            // take back premium roles if has (for future updates)
            $this->rolesService->removePremiumRoles($user);
            $roles = $subscription->getPlan()->getPlanGroup()->getRoles();
            $this->rolesService->assignRoles($user, $roles);

            $subscription->setEndDate($command->getDateTo());
            $subscription = $this->subscriptionRepository->persist($subscription);

            $this->entityManager->commit();

        } catch (\Exception $e) {
            $this->entityManager->getConnection()->rollBack();
            $this->log($command->getSubscription(), $e);
        }

        return $subscription;
    }

    /**
     * @param Subscription $subscripthion
     * @param \Exception   $e
     */
    private function log(Subscription $subscripthion, \Exception $e)
    {
        $this->logger->critical(
            sprintf(
                'Error while extending subscription (%d) for user %s; error message: "%s"; stacktrace: %s',
                $subscripthion->getId(),
                $subscripthion->getUser()->getId(),
                $e->getMessage(),
                $e->getTraceAsString()
            )
        );
    }

    /**
     * @return array
     */
    public function getEvents(): array
    {
        return $this->events;
    }
}
