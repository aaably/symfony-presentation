<?php

namespace Symetria\BillingBundle\Handler\Subscription;

use Fitatu\DatabaseBundle\Entity\Auth\Subscription;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class CancelUserSubscriptionCommand
{
    /**
     * @var Subscription
     */
    private $subscription;

    /**
     * @var \DateTime
     */
    private $cancelledAt;

    /**
     * @var int
     */
    private $cancelReason;

    /**
     * @param Subscription $subscription
     * @param \DateTime    $cancelledAt
     * @param int          $cancelReason
     */
    public function __construct(Subscription $subscription, \DateTime $cancelledAt, int $cancelReason = 0)
    {
        $this->subscription = $subscription;
        $this->cancelledAt = $cancelledAt;
        $this->cancelReason = $cancelReason;
    }

    /**
     * @return int
     */
    public function getCancelReason(): int
    {
        return $this->cancelReason;
    }

    /**
     * @return \DateTime
     */
    public function getCancelledAt(): \DateTime
    {
        return $this->cancelledAt;
    }

    /**
     * @return Subscription
     */
    public function getSubscription(): Subscription
    {
        return $this->subscription;
    }
}
