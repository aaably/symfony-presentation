<?php

namespace Symetria\BillingBundle\Handler\Subscription;

use Fitatu\DatabaseBundle\Entity\Auth\Subscription;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class ExtendUserSubscriptionCommand
{
    /**
     * @var Subscription
     */
    private $subscription;

    /**
     * @var \DateTimeInterface
     */
    private $dateTo;

    /**
     * @param Subscription       $subscription
     * @param \DateTimeInterface $dateTo
     */
    public function __construct(Subscription $subscription, \DateTimeInterface $dateTo)
    {
        $this->subscription = $subscription;
        $this->dateTo = $dateTo;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getDateTo(): \DateTimeInterface
    {
        return $this->dateTo;
    }

    /**
     * @return Subscription
     */
    public function getSubscription(): Subscription
    {
        return $this->subscription;
    }
}
