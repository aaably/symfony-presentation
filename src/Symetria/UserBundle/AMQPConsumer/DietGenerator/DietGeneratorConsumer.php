<?php

namespace Symetria\UserBundle\AMQPConsumer\DietGenerator;

use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use Psr\Log\LoggerInterface;
use Symetria\SharedBundle\AMQPConsumer\AbstractConsumer;
use Symetria\SharedBundle\Event\DietGenerator\DietGenerationEvent;
use Symetria\UserBundle\Exception\PremiumServices\DietGeneratorSettingsNotFoundException;
use Symetria\UserBundle\Service\DietGeneratorService;

/**
 * @author Lukasz Domanski
 * @copyright Fitatu Sp. z o.o.
 */
class DietGeneratorConsumer extends AbstractConsumer
{
    /**
     * @var DietGeneratorService
     */
    private $dietGeneratorService;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param DietGeneratorService $dietGeneratorService
     * @param LoggerInterface      $logger
     */
    public function __construct(DietGeneratorService $dietGeneratorService, LoggerInterface $logger)
    {
        $this->dietGeneratorService = $dietGeneratorService;
        $this->logger = $logger;
    }

    /**
     * @param AMQPMessage $msg
     * @return int
     */
    public function run(AMQPMessage $msg): int
    {
        if (true === $this->isBrokenMessage($msg->getBody())) {
            return ConsumerInterface::MSG_REJECT;
        }

        /** @var DietGenerationEvent $event */
        $event = unserialize($msg->getBody());

        try {
            $this->setStatus($event);
        } catch (DietGeneratorSettingsNotFoundException $exception) {
            $this->logger->warning($exception->getMessage());

            return ConsumerInterface::MSG_REJECT;
        }

        $this->consoleLog(sprintf('User diet was generated: %d', $event->userId));

        return ConsumerInterface::MSG_ACK;
    }

    /**
     * @param DietGenerationEvent $event
     */
    private function setStatus(DietGenerationEvent $event)
    {
        $this->dietGeneratorService->setGenerationStatus($event->getUserId(), $event->getGeneratedAt());
    }
}
