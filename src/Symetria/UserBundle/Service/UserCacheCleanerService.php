<?php

namespace Symetria\UserBundle\Service;

use Symetria\ApiClientBundle\ApiClient;
use Symetria\ApiClientBundle\Util\CacheNameGenerator\User\UserInfoCacheNameGenerator;
use Symetria\ApiClientBundle\Util\UriGenerator\Users\DeleteUserCacheUriGenerator;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class UserCacheCleanerService
{
    /**
     * @var ApiClient
     */
    private $apiClient;

    /**
     * @var string
     */
    private $intranetToken;

    /**
     * @var string
     */
    private $locale;

    /**
     * @param ApiClient $apiClient
     * @param string    $intranetToken
     * @param string    $locale
     */
    public function __construct(ApiClient $apiClient, string $intranetToken, string $locale)
    {
        $this->apiClient = $apiClient;
        $this->intranetToken = $intranetToken;
        $this->locale = $locale;
    }

    /**
     * @param int  $userId
     * @param string|null $key
     */
    public function clear(int $userId, $key = null)
    {
        if (!$key) {
            $key = $this->getDefaultCacheKeyName($userId);
        }

        $this->apiClient->sendAnonymousRequest(
            Request::METHOD_DELETE,
            (new DeleteUserCacheUriGenerator($userId)),
            [
                'query'   => [
                    'intranetToken' => $this->intranetToken,
                    'key'           => $key,
                ],
                'headers' => [
                    'API-Cluster' => $this->getApiCluster($userId),
                ],
            ]
        );
    }

    /**
     * @param int $userId
     * @return string
     */
    private function getApiCluster(int $userId): string
    {
        $locale = str_replace('_', '-', strtolower($this->locale));

        return sprintf('%s%d', $locale, $userId);
    }

    /**
     * @param int $userId
     * @return string
     */
    protected function getDefaultCacheKeyName(int $userId): string
    {
        $cacheName = new UserInfoCacheNameGenerator($userId);

        return $cacheName->get();
    }
}
