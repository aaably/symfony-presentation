<?php

namespace Symetria\UserBundle\Service;

use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Fitatu\BillingBundle\Service\SubscriptionService;
use Fitatu\DatabaseBundle\Entity\Auth\Settings\DietGenerator;
use Fitatu\DatabaseBundle\Entity\Auth\Settings\SettingsDiet;
use Fitatu\Local\Repository\Auth\Settings\DietGeneratorRepository;
use Fitatu\SharedUserBundle\Model\Role\RoleInterface;
use Fitatu\SharedUserBundle\Model\Settings\AbstractSettingsDietNutritionSchema;
use Fitatu\SharedUserBundle\Model\Settings\SettingsDietInterface;
use League\Tactician\CommandBus;
use LogicException;
use Symetria\ApiClientBundle\ApiClient\CacheManager;
use Symetria\ApiClientBundle\Util\CacheNameGenerator\Settings\Diet\DietPlanSettingsByDateCacheNameGenerator;
use Symetria\ApiServerBundle\Util\Serializer\ObjectSerializer;
use Symetria\SharedBundle\Model\UnitType;
use Symetria\SharedBundle\Util\Converter\MeasureConverterUtil;
use Symetria\SharedBundle\Util\Date\DateHelperUtil;
use Symetria\SharedBundle\Util\Diet\Calculators\TempoCalculator;
use Symetria\UserBundle\Diet\Norms\NutritionalNorms;
use Symetria\UserBundle\Exception\DietPreferences\UserHasCustomGoalsForWholeWeekException;
use Symetria\UserBundle\Handler\Settings\Diet\Get\SettingsDietGetCommand;
use Symetria\UserBundle\Service\Preferences\ExcludedProductsService;
use Symetria\UserBundle\Service\Preferences\LimitsService;
use Symetria\UserBundle\Service\Preferences\MealSchemaService;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * @author    Marcin Budzinski, Lukasz Domanski
 * @copyright Fitatu Sp. z o.o.
 */
class DietSettingsService
{
    /**
     * @var ObjectSerializer
     */
    private $serializer;

    /**
     * @var CommandBus
     */
    private $commandBus;

    /**
     * @var UserService
     */
    private $userService;

    /**
     * @var CacheManager
     */
    private $cacheManager;

    /**
     * @var ExcludedProductsService
     */
    private $excludedProductsService;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @var LimitsService
     */
    private $limitsService;

    /**
     * @var MealSchemaService
     */
    private $mealSchemaService;

    /**
     * @var SubscriptionService
     */
    private $subscriptionService;

    /**
     * @var DietGeneratorRepository
     */
    private $dietGeneratorRepository;

    /**
     * @param EntityManagerInterface  $entityManager
     * @param ObjectSerializer        $serializer
     * @param CommandBus              $commandBus
     * @param UserService             $userService
     * @param CacheManager            $cacheManager
     * @param ExcludedProductsService $excludedProductsService
     * @param TranslatorInterface     $translator
     * @param LimitsService           $limitsService
     * @param MealSchemaService       $mealSchemaService
     * @param SubscriptionService     $subscriptionService
     * @param DietGeneratorRepository $dietGeneratorRepository
     */
    function __construct(
        EntityManagerInterface $entityManager,
        ObjectSerializer $serializer,
        CommandBus $commandBus,
        UserService $userService,
        CacheManager $cacheManager,
        ExcludedProductsService $excludedProductsService,
        TranslatorInterface $translator,
        LimitsService $limitsService,
        MealSchemaService $mealSchemaService,
        SubscriptionService $subscriptionService,
        DietGeneratorRepository $dietGeneratorRepository
    ) {
        $this->serializer = $serializer;
        $this->commandBus = $commandBus;
        $this->userService = $userService;
        $this->cacheManager = $cacheManager;
        $this->excludedProductsService = $excludedProductsService;
        $this->translator = $translator;
        $this->limitsService = $limitsService;
        $this->mealSchemaService = $mealSchemaService;
        $this->subscriptionService = $subscriptionService;
        $this->dietGeneratorRepository = $dietGeneratorRepository;
    }

    /**
     * @param int         $userId
     * @param string|null $date
     *
     * @return array
     */
    public function getDietSettings(int $userId, $date = null): array
    {
        $settings = $this->getDietSettingsEntity($userId, $date);

        return $this->prepareSettingsResponse($settings, $date);
    }

    /**
     * @param int $userId
     *
     * @return array
     */
    public function getFormattedDietSettingsCached(int $userId): array
    {
        $cacheNameGenerator = new DietPlanSettingsByDateCacheNameGenerator($userId, new DateTime());
        $cacheName = $cacheNameGenerator->get();
        $settings = $this->cacheManager->get($cacheName);
        if (empty($settings)) {
            $settings = $this->getDietSettings($userId);
            $this->cacheManager->save(
                $cacheName,
                $settings,
                [CacheManager::OPTION_TTL => $cacheNameGenerator->getTTL()]
            );
        }

        return $settings;
    }

    /**
     * Prepare and convert diet settings
     *
     * @param SettingsDietInterface $settings
     * @param string|null           $date
     *
     * @return array
     */
    private function prepareSettingsResponse(SettingsDietInterface $settings, $date = null): array
    {
        $weekDay = 1;

        $mealSchema = $settings->getMealSchema();
        $response = $this->serializer->toArrayByGroup($settings, 'userSettingsDietController');

        if (empty($settings->getUser())) {
            $response['excludedProducts'] = [];
            $response['mealSchemaPreferences'] = [];
        } else {
            $userId = (int)$settings->getUser()->getId();
            $response['excludedProducts'] = $this->excludedProductsService->getExcludedForUserWithNames($userId);
            $response['mealSchemaPreferences'] = $this->getMealSchemaPreferences($userId, $weekDay, $date);
        }

        try {
            $TMRCalculator = $this->userService->getPreparedTMRCalculator($settings);
            $response['TMR'] = $TMRCalculator->calculate();
        } catch (\LogicException $e) {
            $response['TMR'] = null;
        }

        $response['mealSchema'] = $mealSchema;
        $response['activityEnergyInclusionMode'] = SettingsDiet::activityEnergyInclusionModeToLabel(
            $response['activityEnergyInclusionMode']
        );

        if ($this->isPremiumUser($settings)) {
            $response = $this->pushDietGenerationData($settings, $response);
        }

        $response['calculatedEnergy'] = $response['energy'];

        if ($settings->getManualEnergyTarget()) {
            $response['calculatedEnergy'] = $this->userService->calculateEnergyInDiet($settings);
        }

        $response['nutritionSchema'] = new \ArrayObject();
        if ($settings->getSettingsDietNutritionGroup()) {
            $response['nutritionSchema'] = $settings->getSettingsDietNutritionGroup()->getSchemaArray();
        }

        $response['calculatedTempo'] = $this->getCalculatedTempo($response);

        return UnitType::formatUnitIdsToLabelsInArray($response);
    }

    /**
     * @param int $userId
     *
     * @return int
     */
    public function getEnergyInDiet(int $userId): int
    {
        $command = new SettingsDietGetCommand($userId);

        try {
            /** @var SettingsDietInterface $settings */
            $settings = $this->commandBus->handle($command);
        } catch (LogicException $e) {
            $settings = new SettingsDiet();
            $settings->setUserId($userId);
        }

        if ($settings->getManualEnergyTarget()) {
            $energy = $settings->getEnergy();
        } else {
            $energy = $this->userService->calculateEnergyInDiet($settings);
        }

        return (int)$energy;
    }

    /**
     * @param SettingsDietInterface $settings
     *
     * @return array
     * @throws UserHasCustomGoalsForWholeWeekException
     */
    private function getDefaultBasicNutrition(SettingsDietInterface $settings): array
    {
        if ($settings->getManualEnergyTarget()) {
            return [
                'energy'                 => $settings->getEnergy(),
                'proteinPercentage'      => $settings->getProteinPercentage(),
                'proteinWeight'          => $settings->getProteinWeight(),
                'fatPercentage'          => $settings->getFatPercentage(),
                'fatWeight'              => $settings->getFatWeight(),
                'carbohydratePercentage' => $settings->getCarbohydratePercentage(),
                'carbohydrateWeight'     => $settings->getCarbohydrateWeight(),
            ];
        }

        $calculatedEnergy = $this->userService->calculateEnergyInDiet($settings);
        if (empty($calculatedEnergy)) {
            throw new UserHasCustomGoalsForWholeWeekException("User has custom goals for whole week");
        }

        return NutritionalNorms::getBasicNutritionalNormsByEnergy($calculatedEnergy);
    }

    /**
     * @param int $userId
     *
     * @return array
     */
    public function getWeeklyBasicNutrition(int $userId): array
    {
        $settings = $this->getDietSettingsEntity($userId);

        if ($settings->getSettingsDietNutritionGroup()) {
            $weeklySchema = $settings->getSettingsDietNutritionGroup()->getSchemaArray();
        } else {
            $weeklySchema = [];
        }

        try {
            $defaultBasicNutrition = $this->getDefaultBasicNutrition($settings);
        } catch (UserHasCustomGoalsForWholeWeekException $e) {
            return $weeklySchema;
        }

        foreach (AbstractSettingsDietNutritionSchema::INT_TO_DAY as $day) {
            if (isset($weeklySchema[$day])) {
                continue;
            }

            $weeklySchema[$day] = $defaultBasicNutrition;
        }

        return $weeklySchema;
    }

    /**
     * @param int         $userId
     * @param int         $weekDay
     * @param string|null $date
     *
     * @return array
     */
    public function getMealSchemaPreferences(int $userId, int $weekDay, $date = null): array
    {
        $limits = $this->limitsService->getUserLimitsForMeals($userId)->toArray();

        if (DateHelperUtil::isDate($date)) {
            $date = new DateTime($date);
            $hasRolePremium = $this->subscriptionService->isActive($userId, $date);
            $meals = $this->mealSchemaService->get($userId, $date, $weekDay, $hasRolePremium);
        } else {
            $hasRolePremium = $this->subscriptionService->isActive($userId);
            $meals = $this->mealSchemaService->getLatest($userId, $weekDay, $hasRolePremium);
        }

        return collect($meals)->map(
            function ($item, $key) use ($limits) {
                $item = collect($item)->only(['selected', 'mealName', 'mealTime']);

                if (!empty($limits[$key])) {
                    $item = array_merge($item->toArray(), $limits[$key]);
                } else {
                    $item['items'] = [];
                }

                return $item;
            }
        )->toArray();
    }

    /**
     * @param SettingsDietInterface $settings
     *
     * @return bool
     */
    private function isPremiumUser(SettingsDietInterface $settings): bool
    {
        return !empty($settings->getUser()) && $settings->getUser()->hasRole(RoleInterface::ROLE_PREMIUM);
    }

    /**
     * @param double $weightCurrent
     * @param double $weightTarget
     *
     * @return int
     */
    public static function getWeightChangeDirection($weightCurrent, $weightTarget)
    {
        if ($weightTarget == $weightCurrent) {
            return 0;
        } elseif ($weightTarget > $weightCurrent) {
            return 1;
        } else {
            return -1;
        }
    }

    /**
     * @param SettingsDietInterface $settings
     * @param array                 $response
     *
     * @return array
     */
    private function pushDietGenerationData(SettingsDietInterface $settings, array $response): array
    {
        /** @var DietGenerator $dietGeneration */
        $dietGeneration = $this->dietGeneratorRepository->findOneBy(['user' => $settings->getUser()]);

        if ($dietGeneration instanceof DietGenerator) {
            $response['dietGeneration'] = [
                'timeTable' => $dietGeneration->getTimeTable()['next'],
            ];
        }

        return $response;
    }

    /**
     * @param int  $userId
     * @param null $date
     *
     * @return SettingsDietInterface|SettingsDiet
     */
    private function getDietSettingsEntity(int $userId, $date = null): SettingsDietInterface
    {
        $command = new SettingsDietGetCommand($userId, $date);

        try {
            /** @var SettingsDietInterface $settings */
            $settings = $this->commandBus->handle($command);
        } catch (LogicException $e) {
            $settings = new SettingsDiet();
            $settings->setUserId($userId);
        }

        return $settings;
    }

    /**
     * @param array $response
     *
     * @return float|null
     */
    private function getCalculatedTempo(array $response)
    {
        $nutritionSchema = $response['nutritionSchema'];
        if ($nutritionSchema instanceof \ArrayObject) {
            $nutritionSchema = $nutritionSchema->getArrayCopy();
        }

        try {
            $energy = $response['calculatedEnergy'];
            if ($response['manualEnergyTarget']) {
                $energy = $response['energy'];
            }

            $tempoCalculator = new TempoCalculator(
                intval($energy),
                $nutritionSchema,
                intval($response['TMR'])
            );

            $calculatedTempo =  $tempoCalculator->calculate();
            $unit = UnitType::getLabelById($response["weightChangeSpeedUnit"]);

            return round(MeasureConverterUtil::convert("kg", $unit, $calculatedTempo), 2);
        } catch (\LogicException $e) {
            return null;
        }
    }
}
