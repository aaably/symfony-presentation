<?php

namespace Symetria\UserBundle\Service;

use DateTime;
use DateTimeInterface;
use Fitatu\BillingBundle\Service\SubscriptionService;
use Fitatu\DatabaseBundle\Entity\Auth\Settings\DietGenerator;
use Fitatu\DatabaseBundle\Entity\Auth\User\User;
use Fitatu\Local\Repository\Auth\Settings\DietGeneratorRepository;
use Fitatu\Local\Repository\Auth\User\UserRepository;
use Fitatu\SharedBundle\Model\Date\DateRange;
use League\Tactician\CommandBus;
use Symetria\SharedBundle\Event\DietGenerator\DietGeneratorQueueEvent;
use Symetria\SharedBundle\EventBus\Dispatcher\RabbitMQEventDispatcherBridge;
use Symetria\UserBundle\Exception\PremiumServices\DietGeneratorSettingsNotFoundException;
use Symetria\UserBundle\Exception\PremiumServices\UserNotFoundException;
use Symetria\UserBundle\Exception\PremiumServices\WrongDateDietGeneratorQueueException;
use Symetria\UserBundle\Service\DietGenerationQueueService\WeeklyGoals;
use Symetria\UserBundle\Service\Preferences\ExcludedProductsService;

/**
 * @author    Lukasz Domanski
 * @copyright Fitatu Sp. z o.o.
 */
class DietGenerationQueueService
{
    /**
     * @var RabbitMQEventDispatcherBridge
     */
    private $dispatcher;

    /**
     * @var CommandBus
     */
    private $commandBus;

    /**
     * @var DietSettingsService
     */
    private $dietSettingsService;

    /**
     * @var DietGeneratorRepository
     */
    private $dietGeneratorRepository;

    /**
     * @var ExcludedProductsService
     */
    private $excludedProductsService;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var WeeklyGoals
     */
    private $weeklyGoals;

    /**
     * @var SubscriptionService
     */
    private $subscriptionService;

    /**
     * @var string
     */
    private $defaultLocale;

    /**
     * @param RabbitMQEventDispatcherBridge $dispatcher
     * @param CommandBus                    $commandBus
     * @param DietSettingsService           $dietSettingsService
     * @param DietGeneratorRepository       $dietGeneratorRepository
     * @param ExcludedProductsService       $excludedProductsService
     * @param UserRepository                $userRepository
     * @param WeeklyGoals                   $weeklyGoals
     * @param SubscriptionService           $subscriptionService
     * @param string                        $defaultLocale
     */
    public function __construct(
        RabbitMQEventDispatcherBridge $dispatcher,
        CommandBus $commandBus,
        DietSettingsService $dietSettingsService,
        DietGeneratorRepository $dietGeneratorRepository,
        ExcludedProductsService $excludedProductsService,
        UserRepository $userRepository,
        WeeklyGoals $weeklyGoals,
        SubscriptionService $subscriptionService,
        string $defaultLocale
    ) {
        $this->dispatcher = $dispatcher;
        $this->commandBus = $commandBus;
        $this->dietSettingsService = $dietSettingsService;
        $this->dietGeneratorRepository = $dietGeneratorRepository;
        $this->excludedProductsService = $excludedProductsService;
        $this->userRepository = $userRepository;
        $this->weeklyGoals = $weeklyGoals;
        $this->subscriptionService = $subscriptionService;
        $this->defaultLocale = $defaultLocale;
    }

    /**
     * @param int   $userId
     * @param array $options ['fromDate' => DateTimeInterface, 'toDate' => DateTimeInterface, 'requestAt' => DateTimeInterface]
     * @param bool  $dryRun
     *
     * @return DietGeneratorQueueEvent
     */
    public function send(int $userId, $options = [], bool $dryRun = false): DietGeneratorQueueEvent
    {
        $dietGenerator = $this->dietGeneratorRepository->findOneBy(['userId' => $userId]);
        if (!$dietGenerator instanceof DietGenerator) {
            throw new DietGeneratorSettingsNotFoundException($userId);
        }

        // Now dietGeneratorSettings.locale is the same as user.storageLocale base
        // but it is recognized by system
        $dietLocale = $this->getDietLocale($dietGenerator);
        $userStorageLocale = $this->getUserStorageLocale($userId);
        $subscriptionEndDate = $this->subscriptionService->getDietSubscriptionEndDate($userId);

        [$generatedFromDate, $generatedToDate, $requestAt] = $this->getOptions(
            $options,
            $userStorageLocale,
            $subscriptionEndDate
        );

        $dateRange = new DateRange($generatedFromDate, $generatedToDate);

        $event = new DietGeneratorQueueEvent(
            $userId,
            $requestAt,
            $dietLocale,
            $dateRange,
            $this->getGoals($dietGenerator, $dietLocale),
            $dietGenerator->getMealId(),
            $this->getExclusions($userId)
        );

        // We send request event to Planner where user is assigned (user.storageLocale)
        $event->setLocale($userStorageLocale);

        $this->validate($event);

        if (true === $dryRun) {
            return $event;
        }

        $this->dispatcher->dispatch($event);

        $dietGenerator->setLastRequestedAt(new DateTime());

        $dietGeneratorRequestAt = clone $generatedFromDate;
        $dietGenerator->setRequestAt($dietGeneratorRequestAt);
        $this->dietGeneratorRepository->persist($dietGenerator);

        return $event;
    }

    /**
     * @param DietGenerator $dietGenerator
     * @param string        $dietLocale
     * @return array
     */
    private function getGoals(DietGenerator $dietGenerator, string $dietLocale): array
    {
        return $this->weeklyGoals->get($dietGenerator, $dietLocale);
    }

    /**
     * @param array                  $options
     * @param string                 $userLocale
     * @param DateTimeInterface|null $subscriptionEndDate
     * @return array [fromDate, toDate, requestAt]
     */
    private function getOptions(array $options, string $userLocale, ?DateTimeInterface $subscriptionEndDate): array
    {
        if (empty($options['fromDate']) && empty($options['toDate'])) {
            [$fromDate, $toDate] = DietGenerator::getNextPeriods($userLocale);
        } else {
            $fromDate = $options['fromDate'] ?? new DateTime('now');
            $toDate = $options['toDate'] ?? DietGenerator::getActualPeriods($userLocale)[1];

            if ($fromDate instanceof DateTimeInterface) {
                $fromDate = $fromDate < new DateTime('now') ? new DateTime('now') : $fromDate;
            }
            if ($toDate instanceof DateTimeInterface) {
                $toDate = $toDate < new DateTime('now') ? new DateTime('now') : $toDate;
            }
        }

        $requestAt = $options['requestAt'] ?? new DateTime('now');
        $toDate = $this->limitGenerationDateBySubscriptionLength($toDate, $subscriptionEndDate);

        return [$fromDate, $toDate, $requestAt];
    }

    /**
     * @param DateTimeInterface      $toDate
     * @param DateTimeInterface|null $subscriptionEndDate
     * @return DateTimeInterface
     */
    private function limitGenerationDateBySubscriptionLength(
        DateTimeInterface $toDate,
        ?DateTimeInterface $subscriptionEndDate
    ): DateTimeInterface {
        if (!$subscriptionEndDate instanceof DateTimeInterface) {
            return $toDate;
        }

        return min($toDate, $subscriptionEndDate);
    }

    /**
     * @param DietGeneratorQueueEvent $event
     * @throws WrongDateDietGeneratorQueueException
     */
    private function validate(DietGeneratorQueueEvent $event)
    {
        if (!$event->dateRange) {
            return;
        }

        if ($event->dateRange->getTo()->format('Ymd') < $event->dateRange->getFrom()->format('Ymd')) {
            throw new WrongDateDietGeneratorQueueException(sprintf('Wrong fromDate or toDate', $event->userId));
        }
    }

    /**
     * @param DietGenerator $dietGenerator
     * @return string
     */
    private function getDietLocale(DietGenerator $dietGenerator): string
    {
        return $dietGenerator->getLocale() ?? $this->defaultLocale;
    }

    /**
     * @param int $userId
     * @throws UserNotFoundException
     * @return string
     */
    private function getUserStorageLocale(int $userId): string
    {
        /** @var User $user */
        $user = $this->userRepository->find($userId);
        if (false === $user instanceof User) {
            throw new UserNotFoundException($userId);
        }

        return $user->getStorageLocale() ?? $user->getLocale();
    }

    /**
     * @param int $userId
     * @return array
     */
    private function getExclusions(int $userId): array
    {
        $exclusions = [
            'products' => [],
            'recipes'  => [],
        ];
        $products = $this->excludedProductsService->getExcludedForUser($userId);

        if (!$products->isEmpty()) {
            $exclusions['products'] = $products->toArray();
        }

        return $exclusions;
    }
}
