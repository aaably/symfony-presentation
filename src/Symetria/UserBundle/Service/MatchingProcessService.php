<?php

namespace Symetria\UserBundle\Service;

use Fitatu\DatabaseBundle\Entity\Auth\Settings\SettingsPremium;
use Fitatu\Local\Repository\Auth\Settings\DietGeneratorRepository;
use Fitatu\Local\Repository\Auth\Settings\SettingsPremiumRepository;
use Fitatu\SharedUserBundle\Model\Role\RoleInterface;
use Symetria\SharedBundle\Model\UserInterface;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class MatchingProcessService
{
    public const DIET_MATCHING_PROCESS = 'DIET';
    public const PREMIUM_MATCHING_PROCESS = 'PREMIUM';
    /**
     * @var DietGeneratorService
     */
    private $dietGeneratorService;

    /**
     * @var DietGeneratorRepository
     */
    private $dietGeneratorRepository;

    /**
     * @var SettingsPremiumRepository
     */
    private $settingsPremiumRepository;

    /**
     * @param DietGeneratorService      $dietGeneratorService
     * @param DietGeneratorRepository   $dietGeneratorRepository
     * @param SettingsPremiumRepository $settingsPremiumRepository
     */
    public function __construct(
        DietGeneratorService $dietGeneratorService,
        DietGeneratorRepository $dietGeneratorRepository,
        SettingsPremiumRepository $settingsPremiumRepository
    ) {
        $this->dietGeneratorService = $dietGeneratorService;
        $this->dietGeneratorRepository = $dietGeneratorRepository;
        $this->settingsPremiumRepository = $settingsPremiumRepository;
    }

    /**
     * @param UserInterface $user
     * @return null|string
     */
    public function getStatusForUser(UserInterface $user)
    {
        if ($this->showDietUserMatchingProcess($user)) {
            return static::DIET_MATCHING_PROCESS;
        }

        if ($this->showPremiumUserMatchingProcess($user)) {
            return static::PREMIUM_MATCHING_PROCESS;
        }

        return null;
    }

    /**
     * @param UserInterface $user
     * @return bool
     */
    private function showDietUserMatchingProcess(UserInterface $user): bool
    {
        if (!$user->hasRole(RoleInterface::ROLE_PREMIUM)) {
            return false;
        }

        return $this->showMatchingProcessForUser($user);
    }

    /**
     * @param UserInterface $user
     * @return bool
     */
    private function showPremiumUserMatchingProcess(UserInterface $user): bool
    {
        if (!$user->hasRole(RoleInterface::ROLE_PREMIUM_WITHOUT_DIET)) {
            return false;
        }

        return $this->showMatchingProcessForUser($user);
    }

    /**
     * @param UserInterface $user
     * @return bool
     */
    public function showMatchingProcessForUser(UserInterface $user): bool
    {
        $settings = $this->settingsPremiumRepository->findOneBy(['user' => $user]);
        $dietGenerator = $this->dietGeneratorRepository->findOrMake($user);

        if (!$settings instanceof SettingsPremium) {
            return false;
        }

        if (!$dietGenerator->isReviewed() && $settings->showMatchingProcess()) {
            return true;
        }

        return $settings->showMatchingProcess();
    }
}
