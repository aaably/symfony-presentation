<?php

namespace Symetria\UserBundle\Service;

use DateTime;
use DateTimeInterface;
use Fitatu\BillingBundle\Service\SubscriptionService;
use Fitatu\DatabaseBundle\Entity\Auth\Settings\DietGenerator;
use Fitatu\DatabaseBundle\Entity\Auth\User\User;
use Fitatu\Local\Repository\Auth\Settings\DietGeneratorRepository;
use Fitatu\Local\Repository\Auth\User\UserRepository;
use Illuminate\Support\Collection;
use Symetria\SharedBundle\Diet\Meals;
use Symetria\SharedBundle\EventBus\Dispatcher\RabbitMQEventDispatcherBridge;
use Symetria\SharedBundle\Util\Date\DateHelperUtil;
use Symetria\UserBundle\Exception\PremiumServices\DietGeneratorSettingsNotFoundException;
use Symetria\UserBundle\Exception\PremiumServices\NoActiveSubscriptionException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class DietGeneratorService
{
    /**
     * @var DietGeneratorRepository
     */
    private $dietGeneratorRepository;

    /**
     * @var RabbitMQEventDispatcherBridge
     */
    private $dispatcher;

    /**
     * @var DietSettingsService
     */
    private $dietSettingsService;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var DietGenerationQueueService
     */
    private $dietGenerationQueueService;

    /**
     * @var MatchingProcessService
     */
    private $matchingProcessService;

    /**
     * @var SubscriptionService
     */
    private $subscriptionService;

    /**
     * @var SettingsPremiumService
     */
    private $settingsPremiumService;

    /**
     * @param DietGeneratorRepository                             $dietGeneratorRepository
     * @param SubscriptionService                                 $subscriptionService
     * @param RabbitMQEventDispatcherBridge                       $dispatcher
     * @param DietSettingsService                                 $dietSettingsService
     * @param UserRepository                                      $userRepository
     * @param DietGenerationQueueService                          $dietGenerationQueueService
     * @param SettingsPremiumService                              $settingsPremiumService
     * @param \Symetria\UserBundle\Service\MatchingProcessService $matchingProcessService
     */
    public function __construct(
        DietGeneratorRepository $dietGeneratorRepository,
        SubscriptionService $subscriptionService,
        RabbitMQEventDispatcherBridge $dispatcher,
        DietSettingsService $dietSettingsService,
        UserRepository $userRepository,
        DietGenerationQueueService $dietGenerationQueueService,
        SettingsPremiumService $settingsPremiumService,
        MatchingProcessService $matchingProcessService
    ) {
        $this->dietGeneratorRepository = $dietGeneratorRepository;
        $this->subscriptionService = $subscriptionService;
        $this->dispatcher = $dispatcher;
        $this->dietSettingsService = $dietSettingsService;
        $this->userRepository = $userRepository;
        $this->dietGenerationQueueService = $dietGenerationQueueService;
        $this->matchingProcessService = $matchingProcessService;
        $this->settingsPremiumService = $settingsPremiumService;
    }

    /**
     * @param int $id
     * @return null|DietGenerator
     */
    public function getForUser(int $id)
    {
        if (!$this->hasActiveSubcriptionForUser($id)) {
            return null;
        }

        /** @var User $user */
        $user = $this->userRepository->find($id);

        return $this->dietGeneratorRepository->findOrMake($user);
    }

    /**
     * @param int $userId
     * @return array[]
     */
    public function getFormattedForUser(int $userId): array
    {
        $dietGenerator = $this->getForUser($userId);

        if (!$dietGenerator instanceof DietGenerator) {
            throw new NoActiveSubscriptionException();
        }

        $fromDate = new DateTime('today');

        if ($dietGenerator->getToDate() && $dietGenerator->getToDate() >= new DateTime()) {
            $toDate = $dietGenerator->getToDate();
        } else {
            $toDate = DietGenerator::getActualPeriods($dietGenerator->getLocale())[1];
        }

        $subscriptionEndDate = $this->subscriptionService->getDietSubscriptionEndDate($userId);
        $toDate = $this->limitGenerationDateBySubscriptionLength($toDate, $subscriptionEndDate);

        $settings = $this->getFilteredMealSchema($userId);
        $diff = $fromDate->diff($toDate)->days;

        // todo kiedy będą godziny weryfikacja ile posiłków zostało danego dnia
        $options = collect();
        foreach (range(0, $diff) as $dayNb) {
            $date = (clone $fromDate)
                ->add(new \DateInterval('P'.$dayNb.'D'))
                ->format('Y-m-d');

            $options[$date] = $settings;
        }

        $array = $dietGenerator->toArray();

        $array['remainingDates'] = $options->toArray();

        return $array;
    }

    /**
     * @param DateTimeInterface      $toDate
     * @param DateTimeInterface|null $subscriptionEndDate
     * @return DateTimeInterface
     */
    private function limitGenerationDateBySubscriptionLength(
        DateTimeInterface $toDate,
        ?DateTimeInterface $subscriptionEndDate
    ): DateTimeInterface {
        if (!$subscriptionEndDate instanceof DateTimeInterface) {
            return $toDate;
        }

        return min($toDate, $subscriptionEndDate);
    }

    /**
     * @param int $userId
     * @param int $weekDay
     * @return Collection
     */
    protected function getFilteredMealSchema(int $userId, int $weekDay = 1): Collection
    {
        return collect($this->dietSettingsService->getMealSchemaPreferences($userId, $weekDay))
            ->filter(function ($item) {
                return $item['selected'];
            })->map(function ($item, $key) {
                return [
                    'key'  => $key,
                    'name' => $item['mealName'],
                    'time' => $item['mealTime'],
                ];
            });
    }

    /**
     * @param int   $userId
     * @param array $data
     * @return DietGenerator
     */
    public function update(int $userId, array $data): DietGenerator
    {
        if (!$this->hasActiveSubcriptionForUser($userId)) {
            throw new AccessDeniedHttpException('Access denied.');
        }

        /** @var User $user */
        $user = $this->userRepository->find($userId);

        /** @var DietGenerator $dietGenerator */
        $dietGenerator = $this->dietGeneratorRepository->findOrMake($user);

        foreach ($this->preparePayload($data) as $method => $argument) {
            $dietGenerator->$method($argument);
        }

        if ($dietGenerator->isReviewed()) {
            $this->settingsPremiumService->update($userId, [
                'matchingProcess' => false,
            ], false);
        }

        $this->sendToGenerationQueue($userId, $dietGenerator, $data);

        return $this->dietGeneratorRepository->addOrUpdate($dietGenerator);
    }

    /**
     * @param array $data
     * @return Collection
     */
    protected function preparePayload(array $data): Collection
    {
        $data['mealId'] = $this->prepareMeal($data);
        unset($data['mealName']);

        return collect($data)->filter(function ($item, $key) {
            return $key != 'id';
        })->mapWithKeys(function ($item, $key) {

            if (in_array($key, ['requestAt'])) {

                if (strlen($item) <= 10) {
                    $item = new DateTime(sprintf('%s +%d hours +%d minutes', $item, date('H'), date('i')));
                    $item->modify(sprintf('+%d hours', DietGenerator::HOUR_SPACE_FROM_REQUEST));
                } else {
                    $item = new DateTime($item);
                }

                $item = DateHelperUtil::convertToDateTime($item);
            }

            return ['set'.ucfirst($key) => $item];
        });
    }

    /**
     * @param array $data
     * @return int
     */
    protected function prepareMeal(array $data): int
    {
        $meal = !empty($data['mealName']) ? Meals::getMealNameToId($data['mealName']) : null;
        if (empty($meal)) {
            $meal = Meals::MEAL_ID_BREAKFAST;
        }

        return $meal;
    }

    /**
     * @param int $userId
     * @return bool
     */
    protected function hasActiveSubcriptionForUser(int $userId): bool
    {
        return $this->subscriptionService->isActive($userId);
    }

    /**
     * @param int               $userId
     * @param DateTimeInterface $generatedAt
     * @return bool
     *
     * @throws DietGeneratorSettingsNotFoundException
     */
    public function setGenerationStatus(int $userId, DateTimeInterface $generatedAt): bool
    {
        /** @var DietGenerator $dietGenerator */
        $dietGenerator = $this->dietGeneratorRepository->findOneBy(['user' => $userId]);

        if (false === $dietGenerator instanceof DietGenerator) {
            throw new DietGeneratorSettingsNotFoundException($userId);
        }

        $dietGenerator->setLastGeneratedAt($generatedAt);
        $this->dietGeneratorRepository->persist($dietGenerator);

        // todo Send notification to dietetician etc.

        return true;
    }

    /**
     * @param int               $userId
     * @param DateTimeInterface $activatedAt
     * @param DateTimeInterface $toDate
     * @return bool
     */
    public function setActivationStatus(int $userId, DateTimeInterface $activatedAt, DateTimeInterface $toDate): bool
    {
        /** @var DietGenerator $dietGenerator */
        $dietGenerator = $this->dietGeneratorRepository->findOneBy(['userId' => $userId]);

        if (false === $dietGenerator instanceof DietGenerator) {
            throw new DietGeneratorSettingsNotFoundException($userId);
        }

        $dietGenerator
            ->setLastActivatedAt($activatedAt)
            ->setToDate($toDate);
        $this->dietGeneratorRepository->persist($dietGenerator);

        // todo Send notification to user etc.

        return true;
    }

    /**
     * @param int $userId
     * @return array
     */
    public function getStatusForUser(int $userId): array
    {
        $dietGenerator = $this->dietGeneratorRepository->findOneBy(['user' => $userId]);

        if (!$dietGenerator instanceof DietGenerator) {
            throw new DietGeneratorSettingsNotFoundException($userId);
        }

        return [
            'isReviewed' => $dietGenerator->isReviewed(),
        ];
    }

    /**
     * @param int           $userId
     * @param DietGenerator $dietGenerator
     * @param array         $data
     */
    private function sendToGenerationQueue(int $userId, DietGenerator $dietGenerator, array $data)
    {
        $attributes = $dietGenerator->toGeneratorFormatArray();

        if (
            MatchingProcessService::DIET_MATCHING_PROCESS === $this->matchingProcessService->getStatusForUser(
                $dietGenerator->getUser()
            )
        ) {
            return;
        }

        $this->dietGenerationQueueService->send($userId, $attributes);
    }

    /**
     * @param int $userId
     */
    public function queue(int $userId)
    {
        $dietGenerator = $this->dietGeneratorRepository->findOneBy(['user' => $userId]);

        if (!$dietGenerator instanceof DietGenerator) {
            throw new DietGeneratorSettingsNotFoundException($userId);
        }

        $attributes = $dietGenerator->toGeneratorFormatArray();
        $this->dietGenerationQueueService->send($userId, $attributes);
    }
}
