<?php

namespace Symetria\UserBundle\Service;

use DateTime;
use DateTimeInterface;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\EntityManager;
use Fitatu\DatabaseBundle\Entity\Auth\Settings\SettingsDiet;
use Fitatu\DatabaseBundle\Entity\Auth\Settings\SettingsDietHistory;
use Fitatu\DatabaseBundle\Entity\Auth\Settings\SettingsUser;
use Fitatu\DatabaseBundle\Entity\Auth\Settings\SettingsUserHistory;
use Fitatu\DatabaseBundle\Entity\Auth\User\User;
use Fitatu\SharedUserBundle\Model\Settings\AbstractSettingsDiet;
use Fitatu\SharedUserBundle\Model\Settings\SettingsDietInterface;
use Fitatu\SharedUserBundle\Model\Settings\SettingsUserInterface;
use LogicException;
use Psr\Log\LoggerInterface;
use Symetria\ApiClientBundle\ApiClient;
use Symetria\ApiClientBundle\Exception\TokenNotFoundException;
use Symetria\ApiClientBundle\Util\UriGenerator\ActivityPlan\ActivityPlanByDateRangeUriGenerator;
use Symetria\SharedBundle\Util\Activity\Calculators\ActivityTrainingRateCalculator;
use Symetria\SharedBundle\Util\Date\DateHelperUtil;
use Symetria\SharedBundle\Util\Diet\Calculators\BMRCalculator;
use Symetria\SharedBundle\Util\User\AgeCalculator;
use Symetria\UserBundle\Diet\Norms\NutritionalNorms;
use Symetria\UserBundle\Exception\UserNorms\DatesPeriodException;
use Symetria\UserBundle\Exception\UserNorms\UserSettingsNotFoundException;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author    Lukasz Domanski
 * @copyright Fitatu Sp. z o.o.
 */
class UserNormsService
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var UserService
     */
    private $userService;

    /**
     * @var ApiClient
     */
    private $apiClient;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var DateTime
     */
    private $fromDate;

    /**
     * @var DateTime
     */
    private $toDate;

    /**
     * @var array[]
     */
    private $activities;

    /**
     * @param EntityManager   $entityManager
     * @param UserService     $userService
     * @param ApiClient       $apiClient
     * @param LoggerInterface $logger
     */
    public function __construct(
        EntityManager $entityManager,
        UserService $userService,
        ApiClient $apiClient,
        LoggerInterface $logger
    ) {
        $this->entityManager = $entityManager;
        $this->userService = $userService;
        $this->apiClient = $apiClient;
        $this->logger = $logger;
    }

    /**
     * @param int         $userId
     * @param string      $period
     * @param string|null $fromDate
     * @param string|null $toDate
     * @param bool        $grouped
     * @param string|null $nutrition
     * @param string|null $locale
     *
     * @return array
     */
    public function getUserNormsByPeriod(
        int $userId,
        string $period,
        string $fromDate = null,
        string $toDate = null,
        bool $grouped = true,
        string $nutrition = null,
        string $locale = null
    ): array {

        $this->validate($userId, $period, $fromDate, $toDate);

        $user = $this->userService->getUser($userId);
        $fromDate = new DateTime($fromDate);
        $toDate = !empty($toDate) ? new DateTime($toDate) : null;

        // Retrieve settings diet history
        $settingsDietHistory = $this->findFullSettingsByPeriod(
            'diet',
            $user,
            $period,
            $fromDate,
            $toDate,
            null,
            $locale
        );
        if (empty($settingsDietHistory)) {
            throw new UserSettingsNotFoundException('Settings diet not found');
        }

        // Retrieve settings user history
        $settingsUserHistory = $this->findFullSettingsByPeriod(
            'user',
            $user,
            $period,
            $fromDate,
            $toDate,
            null,
            $locale
        );
        if (empty($settingsUserHistory)) {
            throw new UserSettingsNotFoundException('Settings user not found');
        }

        switch ($period) {
            default:
            case DateHelperUtil::PERIOD_DAY:
                $this->fromDate = $fromDate;
                $this->toDate = $fromDate;
                break;

            case DateHelperUtil::PERIOD_WEEK:
                list($this->fromDate, $this->toDate) = (null === $locale)
                    ? DateHelperUtil::getWeeklyDateRange($fromDate, $user->getLocale()) /* Fallback */
                    : DateHelperUtil::getFirstAndLastDateByPeriod(
                        $period,
                        $locale,
                        $fromDate,
                        $toDate,
                        false
                    );
                break;

            case DateHelperUtil::PERIOD_MONTH:
                list($this->fromDate, $this->toDate) = DateHelperUtil::getMonthlyFirstAndLastDay($fromDate);
                break;

            case DateHelperUtil::PERIOD_CUSTOM:
                $this->fromDate = $fromDate;
                $this->toDate = $toDate;
                break;
        }

        $normsData = [];
        $currentDate = clone $this->fromDate;

        while (true) {
            $key = $currentDate->format('Y-m-d');

            if (isset($settingsDietHistory[$key])) {
                $settingsDiet = array_shift($settingsDietHistory);
            }
            if (isset($settingsUserHistory[$key])) {
                $settingsUser = array_shift($settingsUserHistory);
            }

            if (isset($settingsDiet) && isset($settingsUser)) {
                $normsData[$key] = $this->calculateNutritionalNorms($currentDate, $settingsDiet, $settingsUser);
            }

            $currentDate->modify('+1 day');

            if (!isset($this->toDate) || $currentDate > $this->toDate) {
                break;
            }
        }

        return (null !== $nutrition && !$grouped)
            ? $this->sortNormsDataByNutrition($normsData, $nutrition)
            : $this->sumDataForResponse($normsData);
    }

    /**
     * @param int         $userId
     * @param string      $period
     * @param string|null $fromDate
     * @param string|null $toDate
     * @throws DatesPeriodException
     */
    private function validate(int $userId, string $period, string $fromDate = null, string $toDate = null): void
    {
        $this->userService->getUser($userId);

        if (!DateHelperUtil::isDate($fromDate)) {
            throw new DatesPeriodException('Wrong fromDate parameter');
        }

        if ($period == 'custom') {
            if (!DateHelperUtil::isDate($toDate)) {
                throw new DatesPeriodException('You sent wrong date in [toDate] variable');
            }

            if (new DateTime($fromDate) > new DateTime($toDate)) {
                throw new DatesPeriodException(sprintf('Choosen dates period are out of range [%s-%s]',
                    $fromDate,
                    $toDate));
            }
        }
    }

    /**
     * @param DateTimeInterface     $date
     * @param SettingsDietInterface $settingsDiet
     * @param SettingsUserInterface $settingsUser
     * @return array
     */
    private function calculateNutritionalNorms(
        DateTimeInterface $date,
        SettingsDietInterface $settingsDiet,
        SettingsUserInterface $settingsUser
    ) {
        $activityNorms = $this->calculateActivityNorms($date, $settingsDiet, $settingsUser);

        $activityEnergy = $activityNorms['activityEnergy'];
        $addedActivityEnergy = $activityNorms['addedActivityEnergy'];

        $normsUtil = (new NutritionalNorms($settingsDiet))->setAdditionalEnergy($addedActivityEnergy);

        try {
            $norms = $normsUtil->get($date->format('N'));
        } catch (LogicException $exception) {
            throw new LogicException(
                sprintf('LogicException "%s" for user %s', $exception->getMessage(), $settingsUser->getUserId()),
                0,
                $exception
            );
        }

        $norms['activityEnergy'] = $activityEnergy;
        $norms['addedActivityEnergy'] = $addedActivityEnergy;

        if ($settingsDiet->getManualEnergyTarget() === false) {
            $norms['dietEnergy'] -= $norms['activityEnergy'];
        }

        $norms['dietEnergy'] -= $addedActivityEnergy;
        $norms['energy'] = $norms['dietEnergy'] + $norms['activityEnergy'] + $norms['addedActivityEnergy'];
        $norms['water'] = $this->calculateWaterNorms($date, $settingsDiet, $settingsUser);

        return $norms;
    }

    /**
     * @param DateTimeInterface     $date
     * @param SettingsDietInterface $settingsDiet
     * @param SettingsUserInterface $settingsUser
     * @return array
     */
    private function calculateActivityNorms(
        DateTimeInterface $date,
        SettingsDietInterface $settingsDiet,
        SettingsUserInterface $settingsUser
    ) {
        $activityNorms = [
            'activityEnergy'      => null,
            'addedActivityEnergy' => null,
        ];

        $activityEnergyInclusionMode = $settingsDiet->getActivityEnergyInclusionMode();

        if (is_null($activityEnergyInclusionMode)) {
            $activityEnergyInclusionMode = SettingsDiet::DEFAULT_ACTIVITY_ENERGY_INCLUSION_MODE;
        }

        switch ($activityEnergyInclusionMode) {
            case SettingsDiet::IGNORE_ENTIRELY_ACTIVITY_ENERGY:
                break;

            case SettingsDiet::INCREASE_ENERGY_BY_ACTIVITY_TRAINING:
                $activityNorms['activityEnergy'] = $this->getActivityEnergy($settingsUser, $settingsDiet);
                break;

            case SettingsDiet::INCLUDE_ADDED_ACTIVITIES_IN_ENERGY:
                $activityNorms['addedActivityEnergy'] = $this->getAddedActivityEnergy($settingsUser->getUserId(),
                    $date);
                break;

            case SettingsDiet::INCLUDE_ADDED_ACTIVITIES_AND_INCREASE_ENERGY_BY_ACTIVITY_TRAINING:
                $activityNorms['activityEnergy'] = $this->getActivityEnergy($settingsUser, $settingsDiet);
                $activityNorms['addedActivityEnergy'] = $this->getAddedActivityEnergy($settingsUser->getUserId(),
                    $date);
                break;
        }

        return $activityNorms;
    }

    /**
     * @param DateTimeInterface     $date
     * @param SettingsDietInterface $settingsDiet
     * @param SettingsUserInterface $settingsUser
     * @return int
     */
    private function calculateWaterNorms(
        DateTimeInterface $date,
        SettingsDietInterface $settingsDiet,
        SettingsUserInterface $settingsUser
    ) {
        $activityEnergyInclusionMode = $settingsDiet->getActivityEnergyInclusionMode();

        $basicRate = 30;
        $waterNorms = $settingsUser->getWeightCurrent() * $basicRate;

        switch ($activityEnergyInclusionMode) {
            case AbstractSettingsDiet::INCREASE_ENERGY_BY_ACTIVITY_TRAINING:
                $waterNorms += $this->getActivityEnergy($settingsUser, $settingsDiet);
                break;
            case AbstractSettingsDiet::INCLUDE_ADDED_ACTIVITIES_IN_ENERGY:
                $waterNorms += $this->getAddedActivityEnergy($settingsUser->getUserId(), $date);
                break;
            case AbstractSettingsDiet::INCLUDE_ADDED_ACTIVITIES_AND_INCREASE_ENERGY_BY_ACTIVITY_TRAINING:
                $waterNorms += $this->getActivityEnergy($settingsUser, $settingsDiet);
                $waterNorms += $this->getAddedActivityEnergy($settingsUser->getUserId(), $date);
                break;
        }

        return (int)round($waterNorms, 0);
    }

    /**
     * @param array $dataNorms
     * @return array
     */
    private function sumDataForResponse(array $dataNorms)
    {
        $norms = [];

        foreach ($dataNorms as $itemNorm) {
            foreach ($itemNorm as $field => $value) {
                if (is_array($value)) {

                    if (!isset($norms[$field])) {
                        $norms[$field] = ['min' => 0, 'max' => 0];
                    }

                    if (!empty($value['min'])) {
                        $norms[$field]['min'] += $value['min'];
                    }
                    if (!empty($value['max'])) {
                        $norms[$field]['max'] += $value['max'];
                    }
                } else {
                    if (!isset($norms[$field])) {
                        $norms[$field] = is_null($value) ? null : 0;
                    }

                    if (is_numeric($value)) {
                        $norms[$field] += $value;
                    }
                }
            }
        }

        return $norms;
    }

    /**
     * @param array  $normsData
     * @param string $nutrition
     *
     * @return array
     */
    private function sortNormsDataByNutrition(array $normsData, string $nutrition): array
    {
        $sortedNormsData = [];
        $additionalIncludedNorms = ['dietEnergy', 'energy'];
        foreach ($normsData as $k => $v) {
            if (array_key_exists($nutrition.'Weight', $v)) {
                $sortedNormsData[$k] = $v[$nutrition.'Weight'];
            }
            if (in_array($nutrition, $additionalIncludedNorms)) {
                $sortedNormsData[$k] = $v[$nutrition];
            }
        }

        return $sortedNormsData;
    }

    /**
     * @param SettingsUserInterface $settingsUser
     * @param SettingsDietInterface $settingsDiet
     *
     * @return int|null
     */
    private function getActivityEnergy(SettingsUserInterface $settingsUser, SettingsDietInterface $settingsDiet)
    {
        // TODO: wiek w ustawieniach użytkownika - brak parametru
        $birthDate = $settingsUser->getBirthDate();

        if (!$birthDate instanceof \DateTimeInterface) {
            return null; // FIXME
        }

        $age = (new AgeCalculator($birthDate))->calculate();

        // BMR Rate
        $bmrRate = (new BMRCalculator($settingsUser->getWeightCurrentKg(), $age, $settingsUser->getSex()))->calculate();

        $activityTrainingRate = new ActivityTrainingRateCalculator($settingsDiet->getActivityTraining(), $bmrRate);
        $activityEnergy = $activityTrainingRate->calculate();

        return (int)$activityEnergy;
    }

    /**
     * @param int               $userId
     * @param DateTimeInterface $date
     * @return int
     * @throws TokenNotFoundException
     */
    private function getAddedActivityEnergy($userId, DateTimeInterface $date)
    {
        $energySum = 0;

        if (!isset($this->activities[$date->format("Y-m-d")])) {
            $this->activities = $this->apiClient->sendCachedRequest(
                Request::METHOD_GET,
                new ActivityPlanByDateRangeUriGenerator($userId, $this->fromDate, $this->toDate)
            );
        }

        if (!isset($this->activities[$date->format("Y-m-d")])) {
            $this->logger->error(
                sprintf(
                    "Can't get activity plan by %s",
                    (new ActivityPlanByDateRangeUriGenerator($userId, $this->fromDate, $this->toDate))->get()
                )
            );

            return $energySum;
        }

        foreach ($this->activities[$date->format("Y-m-d")] as $activity) {
            $energySum += (int)$activity['energy'];
        }

        return $energySum;
    }

    /**
     * @param string        $type (diet|user)
     * @param User          $user
     * @param DateTime      $fromDate
     * @param DateTime|null $toDate
     * @param string        $period
     * @param string|null   $mode (ARRAY)
     * @param string|null   $locale
     *
     * @return array
     * @throws DBALException
     */
    private function findFullSettingsByPeriod(
        $type,
        User $user,
        $period,
        DateTime $fromDate,
        DateTime $toDate = null,
        $mode = null,
        string $locale = null
    ) {
        switch ($period) {
            default:
            case DateHelperUtil::PERIOD_DAY:
                $history = $this->getSettingsByDay($type, $user, $fromDate, $mode);
                break;
            case DateHelperUtil::PERIOD_WEEK:
                $history = $this->getSettingsByWeek($type, $user, $fromDate, $locale);
                break;
            case DateHelperUtil::PERIOD_MONTH:
                $history = $this->getSettingsByMonth($type, $user, $fromDate);
                break;
            case DateHelperUtil::PERIOD_CUSTOM:
                $history = $this->getSettingsByCustom($type, $user, $fromDate, $toDate);
                break;
        }

        return $history;
    }

    /**
     * @param string            $type
     * @param User              $user
     * @param DateTimeInterface $dateTime
     * @param null|string       $mode
     * @return array
     * @throws DBALException
     */
    private function getSettingsByDay(string $type, User $user, DateTimeInterface $dateTime, $mode = null): array
    {
        $date = $dateTime->format('Y-m-d 23:59:59');
        $userId = $user->getId();

        if (!$this->checkType($type)) {
            return false;
        }

        $sql = <<<___SQL
                SELECT h.*
                FROM   settings_{$type}_history h
                WHERE  h.user_id  = {$userId}
                AND    h.updated_at <= '{$date}'
                ORDER BY h.updated_at DESC
                LIMIT 1
___SQL;

        $stmt = $this->entityManager
            ->getConnection()
            ->prepare($sql);

        $stmt->execute();

        $data = $stmt->fetchAll();

        if (!$data) {
            $data = $this->getFirstSettingsAfterDate($type, $user, $dateTime);
        }

        if (!$data) {
            return false;
        }

        if ($mode == 'ARRAY') {
            return $data;
        }

        $history[$dateTime->format('Y-m-d')] = $this->createModel($type, $data[0]);

        return $history;
    }

    /**
     * @param string            $type
     * @param User              $user
     * @param DateTimeInterface $dateTime
     * @param string|null       $locale
     *
     * @return array
     * @throws DBALException
     */
    private function getSettingsByWeek(
        string $type,
        User $user,
        DateTimeInterface $dateTime,
        string $locale = null
    ): array {
        $userId = $user->getId();

        if (!$this->checkType($type)) {
            return false;
        }

        /**
         * @var DateTimeInterface $firstDayOfWeek
         * @var DateTimeInterface $lastDayOfWeek
         */
        list($firstDayOfWeek, $lastDayOfWeek) = (null === $locale)
            ? DateHelperUtil::getWeeklyDateRange($dateTime, $user->getLocale()) /* Fallback */
            : DateHelperUtil::getFirstAndLastDateByPeriod(
                DateHelperUtil::PERIOD_WEEK,
                $locale,
                $dateTime,
                null,
                false
            );

        $firstDayOfWeekFormatted = $firstDayOfWeek->format('Y-m-d 00:00:00');
        $lastDayOfWeekFormatted = $lastDayOfWeek->format('Y-m-d 23:59:59');

        $sql = <<<___SQL
                SELECT settings.* FROM(
                    SELECT h.*
                    FROM   settings_{$type}_history h
                    WHERE  h.updated_at BETWEEN '{$firstDayOfWeekFormatted}' AND '{$lastDayOfWeekFormatted}'
                    AND    h.user_id  = {$userId}
                    ORDER BY h.updated_at DESC
                ) as settings
                GROUP BY DAY(settings.updated_at)
                ORDER BY settings.updated_at ASC
___SQL;

        $stmt = $this->entityManager
            ->getConnection()
            ->prepare($sql);

        $stmt->execute();

        $history = $this->fillPeriodDaysHistory($type, $user, $stmt->fetchAll(), $firstDayOfWeek, $locale);

        return $history;
    }

    /**
     * @param string   $type
     * @param User     $user
     * @param DateTime $dateTime
     * @return array
     * @throws DBALException
     */
    private function getSettingsByMonth($type, User $user, DateTime $dateTime): array
    {
        $userId = $user->getId();

        if (!$this->checkType($type)) {
            return false;
        }

        /**
         * @var DateTime $firstDayDateTime
         * @var DateTime $lastDayDateTime
         */
        list($firstDayDateTime, $lastDayDateTime) = DateHelperUtil::getMonthlyFirstAndLastDay($dateTime);

        $firstDay = $firstDayDateTime->format('Y-m-d 00:00:00');
        $lastDay = $lastDayDateTime->format('Y-m-d 23:59:59');

        $sql = <<<___SQL
                SELECT settings.* FROM(
                    SELECT h.*
                    FROM   settings_{$type}_history h
                    WHERE  h.updated_at BETWEEN '{$firstDay}' AND '{$lastDay}'
                    AND    h.user_id  = {$userId}
                    ORDER BY h.updated_at DESC
                ) as settings
                GROUP BY DAY(settings.updated_at)
                ORDER BY settings.updated_at ASC
___SQL;

        $stmt = $this->entityManager
            ->getConnection()
            ->prepare($sql);

        $stmt->execute();

        $history = $this->fillPeriodDaysHistory($type, $user, $stmt->fetchAll(), $firstDayDateTime);

        return $history;
    }

    /**
     * @param string   $type
     * @param User     $user
     * @param DateTime $fromDate
     * @param DateTime $toDate
     * @return array
     * @throws DBALException
     */
    private function getSettingsByCustom(
        $type,
        User $user,
        DateTime $fromDate,
        DateTime $toDate
    ): array {
        $userId = $user->getId();

        if (!$this->checkType($type)) {
            return false;
        }

        $firstDay = $fromDate->format('Y-m-d 00:00:00');
        $lastDay = $toDate->format('Y-m-d 23:59:59');

        $sql = <<<___SQL
                SELECT settings.* FROM(
                    SELECT h.*
                    FROM   settings_{$type}_history h
                    WHERE  h.updated_at BETWEEN '{$firstDay}' AND '{$lastDay}'
                    AND    h.user_id  = {$userId}
                    ORDER BY h.updated_at DESC
                ) as settings
                GROUP BY DAY(settings.updated_at)
                ORDER BY settings.updated_at ASC
___SQL;

        $stmt = $this->entityManager
            ->getConnection()
            ->prepare($sql);

        $stmt->execute();

        $history = $this->fillPeriodDaysHistory($type, $user, $stmt->fetchAll(), $fromDate);

        return $history;
    }

    /**
     * @param string            $type
     * @param User              $user
     * @param DateTimeInterface $beforeDay
     * @return array
     * @throws DBALException
     */
    private function getFirstSettingsAfterDate($type, User $user, DateTimeInterface $beforeDay)
    {
        $userId = $user->getId();
        $date = $beforeDay->format('Y-m-d 23:59:59');

        if (!$this->checkType($type)) {
            return false;
        }

        if ($type == 'diet') {
            $sql = <<<___SQL
            SELECT settings.* FROM (
                SELECT h.*
                FROM settings_diet_history h
                WHERE  h.user_id  = {$userId}
                AND    h.energy IS NOT NULL
                AND    h.updated_at >= '{$date}'
                ORDER BY h.updated_at DESC
            ) as settings
            GROUP BY DAY(settings.updated_at)
            ORDER BY settings.updated_at ASC
            LIMIT 1
___SQL;
        } else {
            $sql = <<<___SQL
            SELECT settings.* FROM (
                SELECT h.*
                FROM settings_user_history h
                WHERE  h.user_id  = {$userId}
                AND    h.updated_at >= '{$date}'
                ORDER BY h.updated_at DESC
            ) as settings
            GROUP BY DAY(settings.updated_at)
            ORDER BY settings.updated_at ASC
            LIMIT 1
___SQL;
        }

        $stmt = $this->entityManager
            ->getConnection()
            ->prepare($sql);

        $stmt->execute();

        $data = $stmt->fetchAll();

        return $data;
    }

    /**
     * @param string            $type (user|diet)
     * @param User              $user
     * @param array             $history
     * @param DateTimeInterface $fromDay
     * @param string|null       $locale
     *
     * @return array|bool
     */
    private function fillPeriodDaysHistory(string $type, User $user, array $history, DateTimeInterface $fromDay, string $locale = null)
    {
        if (!$this->checkType($type)) {
            return false;
        }

        $firstDaySettings = $this->getSettingsForFirstDay($type, $user, $fromDay, $history, $locale);

        if (!$firstDaySettings) {
            return false;
        }

        if (!$history) {
            $history = [$firstDaySettings];
        } else {
            $date1 = (new DateTime($firstDaySettings['updated_at']))->format('Ymd');
            $date2 = (new DateTime($history[0]['updated_at']))->format('Ymd');

            if ($date1 != $date2) {
                array_unshift($history, $firstDaySettings);
            }
        }

        $data = [];

        foreach ($history as $k => $item) {
            $key = (new DateTime($item['updated_at']))->format('Y-m-d');

            // Create SettingsDiet or SettingsUser object for smart using
            $model = $this->createModel($type, $item);
            $data[$key] = $model;
        }

        return $data;
    }

    /**
     * Create model data for smart using
     *
     * @param string $type
     * @param array  $item
     * @return SettingsDiet|SettingsUser
     */
    private function createModel($type, $item)
    {
        switch ($type) {
            default:
                $model = $item;
                break;
            case 'diet':
                $repository = $this->entityManager->getRepository(SettingsDietHistory::class);
                $model = $repository->find($item['id']);
                break;
            case 'user';
                $repository = $this->entityManager->getRepository(SettingsUserHistory::class);
                $model = $repository->find($item['id']);
                break;
        }

        return $model;
    }

    /**
     * @param string      $type (diet|user)
     * @param User        $user
     * @param DateTime    $fromDay
     * @param array       $history
     * @param string|null $locale
     *
     * @return bool|mixed
     */
    private function getSettingsForFirstDay(string $type, User $user, DateTime $fromDay, array $history, string $locale = null)
    {
        $firstDayHistory = $this->findFullSettingsByPeriod($type, $user, 'day', $fromDay, null, 'ARRAY', $locale);
        if (!$firstDayHistory) {
            if (!$history) {
                return false;
            }
            $foundDay = current($history);
        } else {
            $foundDay = current($firstDayHistory);
        }

        // Paste into found day 'updated_at' the first day of period
        $foundDay['updated_at'] = $fromDay->format('Y-m-d H:i:s');

        return $foundDay;
    }

    /**
     * @param string $type
     * @return bool
     */
    private function checkType($type)
    {
        if (in_array($type, ['diet', 'user'])) {
            return true;
        }

        return false;
    }
}
