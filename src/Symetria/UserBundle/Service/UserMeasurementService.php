<?php

namespace Symetria\UserBundle\Service;

use DateTimeInterface;
use Doctrine\ORM\EntityManagerInterface;
use Fitatu\DatabaseBundle\Entity\Auth\Measurement;
use Fitatu\DatabaseBundle\Entity\Auth\User\User;
use Fitatu\Local\Repository\Auth\MeasurementRepository;
use League\Tactician\CommandBus;
use LogicException;
use Psr\Log\LoggerInterface;
use Symetria\ApiClientBundle\ApiClient\CacheManager;
use Symetria\ApiClientBundle\Util\CacheNameGenerator\Measurements\MeasurementsChartWeightCacheNameGenerator;
use Symetria\SharedBundle\Model\UnitType;
use Symetria\SharedBundle\Util\Converter\MeasureConverterUtil;
use Symetria\UserBundle\Handler\Measurement\Create\MeasurementCreateCommand;
use Symetria\UserBundle\Handler\Measurement\MeasurementModel;
use Symetria\UserBundle\Handler\Measurement\Update\MeasurementUpdateCommand;
use Symetria\UserBundle\Service\UserMeasurements\BodyPartSummary;
use Symetria\UserBundle\Service\UserMeasurements\FatPercentageSummary;
use Symetria\UserBundle\Service\UserMeasurements\SizeSummary;
use Symetria\UserBundle\Service\UserMeasurements\WeightSummary;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @author    Lukasz Domanski
 * @copyright Fitatu Sp. z o.o.
 */
class UserMeasurementService
{
    /**
     * @var array
     */
    const SIZE_UNITS = [
        'neck',
        'chest',
        'waist',
        'stomach',
        'hips',
        'thigh',
        'calf',
        'biceps',
    ];

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var MeasurementRepository
     */
    private $measurementRepository;

    /**
     * @var UserSettingsService
     */
    private $userSettingsService;

    /**
     * @var CommandBus
     */
    private $commandBus;

    /**
     * @var CacheManager
     */
    private $cacheManager;

    /**
     * @param EntityManagerInterface $entityManager
     * @param LoggerInterface        $logger
     * @param UserService            $userService
     * @param CommandBus             $commandBus
     * @param UserSettingsService    $userSettingsService
     * @param CacheManager           $cacheManager
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        LoggerInterface $logger,
        UserService $userService,
        CommandBus $commandBus,
        UserSettingsService $userSettingsService,
        CacheManager $cacheManager
    ) {
        $this->entityManager = $entityManager;
        $this->logger = $logger;
        $this->userService = $userService;
        $this->commandBus = $commandBus;
        $this->userSettingsService = $userSettingsService;
        $this->cacheManager = $cacheManager;

        $this->measurementRepository = $entityManager->getRepository(Measurement::class);
    }

    /**
     * @param User              $user
     * @param DateTimeInterface $date
     * @return array
     */
    public function getCalculatedMeasurement(User $user, DateTimeInterface $date):array
    {
        /** @var Measurement $measurement */
        $measurement = $this->measurementRepository->findOneBy(['date' => $date, 'user' => $user]);

        if (false === $measurement instanceof Measurement) {
            throw new HttpException(Response::HTTP_NOT_FOUND, sprintf(
                "There is no measurement for day %s", $date->format("Y-m-d")
            ));
        }

        $allMeasurements = array_merge(['weight'], self::SIZE_UNITS);
        $targetUnits = $this->userService->getDefaultUserUnits($measurement->getUser()->getId());

        if (is_null($targetUnits[UserService::WEIGHT_UNIT]) || is_null($targetUnits[UserService::SIZE_UNIT])) {
            throw new LogicException("User does not have settings");
        }

        $result = [];

        foreach ($allMeasurements as $measurementName) {
            $getValueMethodName = 'get' . $measurementName;
            $getUnitMethodName = 'get' . $measurementName . 'Unit';
            $originalValue = $measurement->$getValueMethodName();

            if (is_null($originalValue)) {
                $result[$measurementName] = null;
                continue;
            }

            $originalUnit = UnitType::getLabelById($measurement->$getUnitMethodName());

            if ($measurementName == 'weight') {
                $targetUnit = $targetUnits[UserService::WEIGHT_UNIT];
            } else {
                $targetUnit = $targetUnits[UserService::SIZE_UNIT];
            }

            $result[$measurementName] = MeasureConverterUtil::convert($originalUnit, $targetUnit, $originalValue);
        }

        $result['fatPercentage'] = $measurement->getFatPercentage();
        $result['weightUnit'] = $targetUnits[UserService::WEIGHT_UNIT];
        $result['sizeUnit'] = $targetUnits[UserService::SIZE_UNIT];

        return $result;
    }

    /**
     * @param int               $userId
     * @param DateTimeInterface $fromDate
     * @param DateTimeInterface $toDate
     * @param int               $page
     * @param int               $limit
     * @return array
     * @throws NotFoundHttpException
     */
    public function getWeightSummary(
        int $userId,
        DateTimeInterface $fromDate,
        DateTimeInterface $toDate,
        int $page,
        int $limit
    ):array
    {
        return (new WeightSummary($this->entityManager))->getSummary($userId, $fromDate, $toDate, $page, $limit);
    }

    /**
     * @param int               $userId
     * @param DateTimeInterface $fromDate
     * @param DateTimeInterface $toDate
     * @return array
     * @throws NotFoundHttpException
     */
    public function getSizeSummary(
        int $userId,
        DateTimeInterface $fromDate,
        DateTimeInterface $toDate
    ):array
    {
        return (new SizeSummary($this->entityManager))->getSummary($userId, $fromDate, $toDate);
    }

    /**
     * @param int               $userId
     * @param DateTimeInterface $fromDate
     * @param DateTimeInterface $toDate
     * @param int               $page
     * @param int               $limit
     * @param string            $bodyPart
     * @return array
     * @throws NotFoundHttpException
     */
    public function getBodyPartSummary(
        int $userId,
        DateTimeInterface $fromDate,
        DateTimeInterface $toDate,
        int $page,
        int $limit,
        string $bodyPart
    ):array
    {
        return (new BodyPartSummary($this->entityManager))->getSummary($userId, $fromDate, $toDate, $page, $limit, $bodyPart);
    }

    /**
     * @param int               $userId
     * @param DateTimeInterface $fromDate
     * @param DateTimeInterface $toDate
     * @param int               $page
     * @param int               $limit
     * @return array
     * @throws NotFoundHttpException
     */
    public function getFatPercentageSummary(
        int $userId,
        DateTimeInterface $fromDate,
        DateTimeInterface $toDate,
        int $page,
        int $limit
    ):array
    {
        return (new FatPercentageSummary($this->entityManager))->getSummary($userId, $fromDate, $toDate, $page, $limit);
    }


    /**
     * @param User              $user
     * @param DateTimeInterface $date
     * @throws NotFoundHttpException
     */
    public function deleteMeasurement(User $user, DateTimeInterface $date)
    {
        /** @var Measurement $measurement */
        $measurement = $this->measurementRepository->findOneBy(['date' => $date, 'user' => $user]);

        if (false === $measurement instanceof Measurement) {
            throw new NotFoundHttpException("Measurements not found");
        }

        $this->measurementRepository->remove($measurement);
    }

    /**
     * @param int               $userId
     * @param DateTimeInterface $fromDate
     * @param DateTimeInterface $toDate
     * @return array[]
     */
    public function getChartData(int $userId, DateTimeInterface $fromDate, DateTimeInterface $toDate):array
    {
        $result = [
            'weights'    => [],
            'targets'    => [],
            'weightUnit' => $this->userService->getDefaultUserUnits($userId)['weightUnit'],
        ];

        $weights = $this->getWeightSummary($userId, $fromDate, $toDate, 1, 100000);
        $weightsCount = count($weights);

        for ($i = 0; $i < $weightsCount; $i++) {
            $result['weights'][$weights[$i]['date']] = $weights[$i]['value'];
        }

        $targets = $this->userSettingsService->getTargetsByRange($userId, $fromDate, $toDate);

        foreach (array_keys($targets) as $key) {
            $targets[$key] = $this->convertWeightTargetToSelectedUnit($targets[$key], $result['weightUnit']);
        }

        $result['targets'] = $targets;

        return $result;
    }

    /**
     * @param int               $userId
     * @param DateTimeInterface $fromDate
     * @param DateTimeInterface $toDate
     * @return array[]
     */
    public function getChartDataCached(int $userId, DateTimeInterface $fromDate, DateTimeInterface $toDate):array
    {
        $cacheName = (new MeasurementsChartWeightCacheNameGenerator($userId, $fromDate, $toDate))->get();

        $chartData = $this->cacheManager->get($cacheName);

        if (empty($chartData)) {
            $chartData = $this->getChartData($userId, $fromDate, $toDate);

            $this->cacheManager->save(
                $cacheName,
                $chartData,
                [CacheManager::OPTION_TTL => MeasurementsChartWeightCacheNameGenerator::TTL_TIME]
            );
        }

        return $chartData;
    }

    /**
     * @param array  $target
     * @param string $targetUnit
     * @return float
     */
    private function convertWeightTargetToSelectedUnit(array $target, string $targetUnit):float
    {
        return (float)MeasureConverterUtil::convert(
            UnitType::getLabelById($target['weightTargetUnit']),
            $targetUnit,
            $target['weightTarget']
        );
    }

    /**
     * @param int               $userId
     * @param DateTimeInterface $date
     * @return bool
     */
    private function isMeasurementExists(int $userId, DateTimeInterface $date):bool
    {
        /** @var Measurement $measurement */
        $measurement = $this->measurementRepository->findOneBy(['date' => $date, 'user' => $userId]);

        return $measurement instanceof Measurement;
    }

    /**
     * @param User              $user
     * @param DateTimeInterface $date
     * @param MeasurementModel  $measurement
     */
    public function createOrUpdateMeasurement(User $user, DateTimeInterface $date, MeasurementModel $measurement)
    {
        if ($this->isMeasurementExists($user->getId(), $date)) {
            $measurementCommand = new MeasurementUpdateCommand();
        } else {
            $measurementCommand = new MeasurementCreateCommand();
            $measurementCommand->updatedAt = new \DateTime();
        }

        $measurementCommand->user = $user;
        $measurementCommand->weightUnit = $measurement->weightUnit;
        $measurementCommand->sizeUnit = $measurement->sizeUnit;
        $measurementCommand->date = $measurement->date;
        $measurementCommand->weight = $measurement->weight;
        $measurementCommand->neck = $measurement->neck;
        $measurementCommand->chest = $measurement->chest;
        $measurementCommand->waist = $measurement->waist;
        $measurementCommand->stomach = $measurement->stomach;
        $measurementCommand->hips = $measurement->hips;
        $measurementCommand->thigh = $measurement->thigh;
        $measurementCommand->calf = $measurement->calf;
        $measurementCommand->biceps = $measurement->biceps;
        $measurementCommand->fatPercentage = $measurement->fatPercentage;

        $this->commandBus->handle($measurementCommand);
    }
}
