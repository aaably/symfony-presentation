<?php

namespace Symetria\UserBundle\Service;

use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Fitatu\DatabaseBundle\Entity\Auth\Settings\SettingsUser;
use Fitatu\DatabaseBundle\Entity\Auth\Settings\SettingsUserHistory;
use Fitatu\Local\Repository\Auth\Settings\SettingsUserHistoryRepository;
use Fitatu\Local\Repository\Auth\Settings\SettingsUserRepository;
use Symetria\ApiClientBundle\ApiClient\CacheManager;
use Symetria\ApiClientBundle\Util\CacheNameGenerator\User\UserSettingsCacheNameGenerator;
use Symetria\ApiServerBundle\Util\Serializer\ObjectSerializer;
use Symetria\SharedBundle\Model\UnitType;
use Symetria\SharedBundle\Util\Diet\Calculators\BMRCalculator;
use Symetria\SharedBundle\Util\User\AgeCalculator;

/**
 * @author    Marcin Budzinski
 * @copyright Fitatu Sp. z o.o.
 */
class UserSettingsService
{
    /**
     * @var SettingsUserRepository
     */
    private $settingsUserRepository;

    /**
     * @var SettingsUserHistoryRepository
     */
    private $settingsUserHistoryRepository;

    /**
     * @var ObjectSerializer
     */
    private $serializer;

    /**
     * @var CacheManager
     */
    private $cacheManager;

    /**
     * @param EntityManagerInterface $entityManager
     * @param ObjectSerializer       $serializer
     * @param CacheManager           $cacheManager
     */
    function __construct(
        EntityManagerInterface $entityManager,
        ObjectSerializer $serializer,
        CacheManager $cacheManager
    ) {
        $this->settingsUserRepository = $entityManager->getRepository(SettingsUser::class);
        $this->settingsUserHistoryRepository = $entityManager->getRepository(SettingsUserHistory::class);
        $this->serializer = $serializer;
        $this->cacheManager = $cacheManager;
    }

    /**
     * @param int $userId
     * @return array
     */
    public function getFormattedUserSettings(int $userId):array
    {
        $settings = $this->settingsUserRepository->find($userId);

        if (is_null($settings)) {
            $settings = new SettingsUser();
            $settings->setUserId($userId);
        }

        $settings = $this->serializer->toArray($settings);
        unset($settings['user']);

        // todo: Pole wiek mamy w planach refaktoringu aby trzymać to w bazie i przeliczać
        if (empty($settings['birthDate'])) {
            $settings['age'] = null;
        } else {
            $birthDate = new DateTime($settings['birthDate']);
            $settings['age'] = (new AgeCalculator($birthDate))->calculate();
            $settings['birthDate'] = $birthDate->format('Y-m-d');
        }

        try {
            $bmrCalculator = new BMRCalculator($settings['weightCurrent'], $settings['age'], $settings['sex']);
            $settings['bmr'] = $bmrCalculator->calculate();
        } catch (\Exception $exception) {
            $settings['bmr'] = null;
        }

        return UnitType::formatUnitIdsToLabelsInArray($settings);
    }

    /**
     * @param int $userId
     * @return array
     */
    public function getFormattedUserSettingsCached(int $userId)
    {
        $cacheName = (new UserSettingsCacheNameGenerator($userId))->get();

        $userSettings = $this->cacheManager->get($cacheName);

        if (empty($userSettings)) {
            $userSettings = $this->getFormattedUserSettings($userId);

            $this->cacheManager->save(
                $cacheName,
                $userSettings,
                [CacheManager::OPTION_TTL => UserSettingsCacheNameGenerator::TTL_TIME]
            );
        }

        return $userSettings;
    }

    /**
     * @param int                $userId
     * @param \DateTimeInterface $fromDate
     * @param \DateTimeInterface $toDate
     * @return array
     */
    public function getTargetsByRange(int $userId, \DateTimeInterface $fromDate, \DateTimeInterface $toDate):array
    {
        $targets = [];

        $firstTarget = $this->settingsUserHistoryRepository->findSettings($userId, $fromDate);
        if ($firstTarget instanceof SettingsUserHistory) {
            $targets[$fromDate->format('Y-m-d')] = [
                'weightTarget'     => (float)$firstTarget->getWeightTarget(),
                'weightTargetUnit' => $firstTarget->getWeightTargetUnit(),
                'weightTargetKg'   => $firstTarget->getWeightTargetKg(),
            ];
        }

        $targets = array_merge(
            $this->settingsUserHistoryRepository->getWeightTargetPerDayByUserIdAndRange($userId, $fromDate, $toDate),
            $targets
        );

        return $targets;
    }
}
