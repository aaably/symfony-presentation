<?php

namespace Symetria\UserBundle\Service;

use Fitatu\BillingBundle\Service\SubscriptionService;
use Fitatu\DatabaseBundle\Entity\Auth\Settings\SettingsPremium;
use Fitatu\Local\Repository\Auth\Settings\SettingsPremiumRepository;
use Fitatu\Local\Repository\Auth\User\UserRepository;
use Fitatu\SharedBundle\Model\ModelPropertiesSetter;
use Symetria\SharedBundle\EventBus\Dispatcher\RabbitMQEventDispatcherBridge;
use Symetria\SharedBundle\Model\UserInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class SettingsPremiumService
{
    /**
     * @var SubscriptionService
     */
    private $subscriptionService;

    /**
     * @var RabbitMQEventDispatcherBridge
     */
    private $dispatcher;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var SettingsPremiumRepository
     */
    private $settingsPremiumRepository;

    /**
     * @var DietGeneratorService
     */
    private $dietGeneratorService;

    /**
     * @param SubscriptionService           $subscriptionService
     * @param UserRepository                $userRepository
     * @param SettingsPremiumRepository     $settingsPremiumRepository
     * @param DietGeneratorService          $dietGeneratorService
     * @param RabbitMQEventDispatcherBridge $dispatcher
     */
    public function __construct(
        SubscriptionService $subscriptionService,
        UserRepository $userRepository,
        SettingsPremiumRepository $settingsPremiumRepository,
        DietGeneratorService $dietGeneratorService,
        RabbitMQEventDispatcherBridge $dispatcher
    ) {
        $this->subscriptionService = $subscriptionService;
        $this->dispatcher = $dispatcher;
        $this->userRepository = $userRepository;
        $this->settingsPremiumRepository = $settingsPremiumRepository;
        $this->dietGeneratorService = $dietGeneratorService;
    }

    /**
     * @param int   $userId
     * @param array $data
     * @param bool  $shouldQueue
     * @return SettingsPremium
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function update(int $userId, array $data = [], bool $shouldQueue = true): SettingsPremium
    {
        if (!$this->hasActiveSubscriptionForUser($userId)) {
            throw new AccessDeniedHttpException('Access denied.');
        }

        $user = $this->userRepository->find($userId);
        $originalSettingsPremium = $this->settingsPremiumRepository->findOrMake($user);

        $existingMatching = $originalSettingsPremium->showMatchingProcess();

        /** @var SettingsPremium $settingsPremium */
        $settingsPremium = ModelPropertiesSetter::set($originalSettingsPremium, $data);

        if ($shouldQueue && $this->shouldSendToGenerationQueue($user, $settingsPremium->showMatchingProcess(), $existingMatching)) {
            $this->dietGeneratorService->queue($userId);
        }

        return $this->settingsPremiumRepository->addOrUpdate($settingsPremium);
    }

    /**
     * @param int $userId
     * @return bool
     */
    private function hasActiveSubscriptionForUser(int $userId): bool
    {
        return $this->subscriptionService->isActive($userId);
    }

    /**
     * @param UserInterface $user
     * @param bool          $matchingProcess
     * @param bool          $existingMatchingProcess
     * @return bool
     */
    private function shouldSendToGenerationQueue(
        UserInterface $user,
        bool $matchingProcess,
        bool $existingMatchingProcess
    ): bool {

        return $user->isPremium() && $existingMatchingProcess && $matchingProcess !== $existingMatchingProcess;
    }
}
