<?php

namespace Symetria\UserBundle\Service;

use DateTime;
use Fitatu\BillingBundle\Service\SubscriptionService;
use Fitatu\DatabaseBundle\Entity\Auth\Settings\DietGenerator;
use Fitatu\DatabaseBundle\Entity\Auth\Settings\SettingsUser;
use Fitatu\DatabaseBundle\Entity\Auth\User\User;
use Fitatu\DatabaseBundle\Entity\Auth\User\UserMeta;
use Fitatu\Local\Repository\Auth\MeasurementRepository;
use Fitatu\Local\Repository\Auth\Settings\SettingsDietRepository;
use Fitatu\Local\Repository\Auth\Settings\SettingsUserHistoryRepository;
use Fitatu\Local\Repository\Auth\Settings\SettingsUserRepository;
use Fitatu\Local\Repository\Auth\User\UserRepository;
use Fitatu\SharedUserBundle\Model\Role\RoleInterface;
use Fitatu\SharedUserBundle\Model\Settings\SettingsDietInterface;
use FOS\UserBundle\Util\TokenGeneratorInterface;
use Symetria\ApiClientBundle\ApiClient\CacheManager;
use Symetria\ApiClientBundle\Util\CacheNameGenerator\User\UserInfoCacheNameGenerator;
use Symetria\SecurityBundle\Mailer\CustomFOSMailer;
use Symetria\SecurityBundle\Service\GatekeeperService;
use Symetria\SharedBundle\Diet\Config\ConfigDietFactory;
use Symetria\SharedBundle\Diet\DietGenerator\DietGeneratorResources;
use Symetria\SharedBundle\Mapper\PremiumLocaleMapper;
use Symetria\SharedBundle\Model\SettingsDiet;
use Symetria\SharedBundle\Model\UnitType;
use Symetria\SharedBundle\Util\Diet\Calculators\BMRCalculator;
use Symetria\SharedBundle\Util\Diet\Calculators\PALCalculator;
use Symetria\SharedBundle\Util\Diet\Calculators\TMRCalculator;
use Symetria\SharedBundle\Util\User\AgeCalculator;
use Symetria\UserBundle\Exception\PremiumServices\UserNotFoundException;
use Symetria\UserBundle\Service\Preferences\ReviewsService;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * @author    Sebastian Superczynski, Lukasz Domanski, Tomasz Dojcz
 * @copyright Fitatu Sp. z o.o.
 */
class UserService
{
    const WEIGHT_UNIT = 'weightUnit';
    const SIZE_UNIT = 'sizeUnit';
    const USER_WEIGHT_REQUIRED_EVERY_7_DAYS = 7;
    private const ENERGY_LIMIT_DEVIATION_PERCENTAGE = 2.0;
    private const CARBOHYDRATE_LIMIT_DEVIATION_PERCENTAGE = 2.0;
    private const PROTEIN_LIMIT_DEVIATION_PERCENTAGE = 2.0;
    private const FAT_LIMIT_DEVIATION_PERCENTAGE = 2.0;

    /**
     * @var SettingsDietRepository
     */
    private $settingsDietRepository;

    /**
     * @var SettingsUserRepository
     */
    private $settingsUserRepository;

    /**
     * @var SettingsUserHistoryRepository
     */
    private $settingsUserHistoryRepository;

    /**
     * @var MeasurementRepository
     */
    private $measurementRepository;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var CacheManager
     */
    private $cacheManager;

    /**
     * @var GatekeeperService
     */
    private $gatekeeperService;

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @var DietGeneratorService
     */
    private $dietGeneratorService;

    /**
     * @var float
     */
    private $searchDeviationRatePercentage;

    /**
     * @var CustomFOSMailer
     */
    private $mailer;

    /**
     * @var TokenGeneratorInterface
     */
    private $tokenGenerator;

    /**
     * @var ReviewsService
     */
    private $reviewsService;
    /**
     * @var MatchingProcessService
     */
    private $matchingProcessService;
    /**
     * @var SubscriptionService
     */
    private $subscriptionService;

    /**
     * @param SettingsDietRepository        $settingsDietRepository
     * @param SettingsUserRepository        $settingsUserRepository
     * @param SettingsUserHistoryRepository $settingsUserHistoryRepository
     * @param SubscriptionService           $subscriptionService
     * @param MeasurementRepository         $measurementRepository
     * @param UserRepository                $userRepository
     * @param CacheManager                  $cacheManager
     * @param GatekeeperService             $gatekeeperService
     * @param RequestStack                  $requestStack
     * @param DietGeneratorService          $dietGeneratorService
     * @param float                         $searchDeviationRatePercentage
     * @param ReviewsService                $reviewsService
     * @param CustomFOSMailer               $mailer
     * @param TokenGeneratorInterface       $tokenGenerator
     * @param MatchingProcessService        $matchingProcessService
     */
    function __construct(
        SettingsDietRepository $settingsDietRepository,
        SettingsUserRepository $settingsUserRepository,
        SettingsUserHistoryRepository $settingsUserHistoryRepository,
        MeasurementRepository $measurementRepository,
        UserRepository $userRepository,
        CacheManager $cacheManager,
        GatekeeperService $gatekeeperService,
        RequestStack $requestStack,
        DietGeneratorService $dietGeneratorService,
        float $searchDeviationRatePercentage,
        ReviewsService $reviewsService,
        CustomFOSMailer $mailer,
        TokenGeneratorInterface $tokenGenerator,
        MatchingProcessService $matchingProcessService,
        SubscriptionService $subscriptionService
    ) {
        $this->settingsDietRepository = $settingsDietRepository;
        $this->settingsUserRepository = $settingsUserRepository;
        $this->settingsUserHistoryRepository = $settingsUserHistoryRepository;
        $this->measurementRepository = $measurementRepository;
        $this->userRepository = $userRepository;
        $this->cacheManager = $cacheManager;
        $this->gatekeeperService = $gatekeeperService;
        $this->requestStack = $requestStack;
        $this->dietGeneratorService = $dietGeneratorService;
        $this->searchDeviationRatePercentage = $searchDeviationRatePercentage;
        $this->reviewsService = $reviewsService;
        $this->mailer = $mailer;
        $this->tokenGenerator = $tokenGenerator;
        $this->matchingProcessService = $matchingProcessService;
        $this->subscriptionService = $subscriptionService;
    }

    /**
     * @param User $user
     * @param int  $requestedUserId
     * @param int  $loggedUser
     * @param bool $isGrantedSuperAdmin
     *
     * @return array
     */
    public function getUserInfo(User $user, int $requestedUserId, int $loggedUser, bool $isGrantedSuperAdmin): array
    {
        /** @var SettingsDiet $userDietSettings */
        $userDietSettings = $this->settingsDietRepository->find($user->getId());

        /** @var SettingsUser $userSettings */
        $userSettings = $this->settingsUserRepository->find($user->getId());
        $hasActivityEnergyInclusionMode = $userDietSettings
            ? !is_null($userDietSettings->getActivityEnergyInclusionMode()) : false;

        $data = [
            'id'                             => $user->getId(),
            'username'                       => $user->getUsername(),
            'nickname'                       => $user->getNickname(),
            'email'                          => $user->getEmail(),
            'roles'                          => $this->getUserRoles($user),
            'accessControl'                  => $this->getAccessControl($user),
            'sex'                            => $userSettings ? $userSettings->getSex() : null,
            'enabled'                        => $user->isEnabled(),
            'createdAt'                      => $user->getCreatedAt(),
            'hasDietSettings'                => (bool) $userDietSettings,
            'hasUserSettings'                => (bool) $userSettings,
            'hasActivityEnergyInclusionMode' => $hasActivityEnergyInclusionMode,
            'demo'                           => $user->isDemo(),
            'locale'                         => $user->getLocale(),
            'storageLocale'                  => $user->getStorageLocale(),
            'searchLocale'                   => $user->getSearchLocale(),
            'timezone'                       => $user->getTimezone(),
            'meta'                           => $this->getMeta($user),
            // App need this information when old user from Vitalia starts using activity
            'hasActivityTraining'            => $userDietSettings
                ? (bool) $userDietSettings->getActivityTraining() : false,
            'appConfig'                      => [
                'searchDeviationRatePercentage'     => $this->searchDeviationRatePercentage,
                'nutritionLimitDeviationPercentage' => [
                    'energy'       => self::ENERGY_LIMIT_DEVIATION_PERCENTAGE,
                    'carbohydrate' => self::CARBOHYDRATE_LIMIT_DEVIATION_PERCENTAGE,
                    'protein'      => self::PROTEIN_LIMIT_DEVIATION_PERCENTAGE,
                    'fat'          => self::FAT_LIMIT_DEVIATION_PERCENTAGE,
                ],
                'isAllowedToReviewDiet'             => $this->reviewsService->isAllowedToReview($user),
            ],
        ];

        if ($data['createdAt'] instanceof \DateTimeInterface) {
            $data['createdAt'] = $data['createdAt']->format('c');
        }

        // Attach more user data if user has proper permissions
        if ($requestedUserId == $loggedUser || $isGrantedSuperAdmin) {
            $data['facebookId'] = $user->getFacebookId();
            $data['hasPassword'] = $user->getPassword() != User::NO_PASSWORD_VALUE;
            $data['requestedEmailChange'] = $user->getRequestedEmailChange();
        }

        $data['isWeightMeasurementRequired'] = $this->isUserWeightMeasurementRequired($user);
        $data['dietGeneration'] = $this->getDietGeneration($user);
        $data['matchingProcess'] = $this->matchingProcessService->getStatusForUser($user);
        $data['subscription'] = $this->subscriptionService->get($user->getId())->toArray();

        return array_merge(
            $data,
            $this->getDefaultUserUnits($user->getId()),
            $this->getSystemInfo($user)
        );
    }

    /**
     * @param int $userId
     *
     * @throws UserNotFoundException
     *
     * @return void
     */
    public function deleteUser(int $userId)
    {
        $user = $this->getUser($userId);
        $randomHash = 'DELETED@'.md5($userId.rand().'+rp+wFMgtd_a+QLaZ<G)Awc5YndYm;6.:Gh*D<-5=yFnCCR_Jut:qB{huB$');

        $user->setUsername($randomHash);
        $user->setUsernameCanonical($randomHash);
        $user->setPassword($randomHash);
        $user->setEmail(
            $user->getEmail().':'.$randomHash
        );

        $user->setEmailCanonical($randomHash);
        $user->setEnabled(false);
        $user->setDeletedAt(
            new DateTime()
        );

        $this->userRepository->persist($user);
    }

    /**
     * @param int    $userId
     * @param string $confirmationToken
     *
     * @throws \InvalidArgumentException
     */
    public function deleteUserByConfirmationToken(int $userId, string $confirmationToken)
    {
        $user = $this->getUser($userId);
        if ($user->getConfirmationToken() === $confirmationToken) {
            $this->deleteUser($userId);
        } else {
            throw new \InvalidArgumentException('Invalid confirmationToken');
        }
    }

    /**
     * @param int $userId
     */
    public function sendUserDeletionEmail(int $userId)
    {
        $this->generateConfirmationToken($userId);
        $this->mailer->sendDeleteConfirmationEmailMessage($this->getUser($userId));
    }

    /**
     * @param int $userId
     */
    private function generateConfirmationToken(int $userId)
    {
        $user = $this->getUser($userId);
        $user->setConfirmationToken($this->tokenGenerator->generateToken());
        $this->userRepository->persist($user);
    }

    /**
     * @param User $user
     *
     * @return array
     */
    private function getDietGeneration(User $user): array
    {
        $config = (ConfigDietFactory::factory($user->getLocale()));

        $data = [
            'indicatedLimits' => [
                'energy'                 => [
                    'min' => $config::getMinEnergyLimit(),
                    'max' => $config::getMaxEnergyLimit(),
                ],
                'carbohydratePercentage' => [
                    'min' => DietGeneratorResources::DEFAULT_NUTRIENTS_PERCENTAGE['carbohydrate']['min'],
                    'max' => DietGeneratorResources::DEFAULT_NUTRIENTS_PERCENTAGE['carbohydrate']['max'],
                ],
                'fatPercentage'          => [
                    'min' => DietGeneratorResources::DEFAULT_NUTRIENTS_PERCENTAGE['fat']['min'],
                    'max' => DietGeneratorResources::DEFAULT_NUTRIENTS_PERCENTAGE['fat']['max'],
                ],
                'proteinPercentage'      => [
                    'min' => DietGeneratorResources::DEFAULT_NUTRIENTS_PERCENTAGE['protein']['min'],
                    'max' => DietGeneratorResources::DEFAULT_NUTRIENTS_PERCENTAGE['protein']['max'],
                ],
            ],
        ];
        if ($user->isPremium()) {
            $data = $this->pushPremiumDataForUser($user, $data);
        }

        return $data;
    }

    /**
     * @param User $user
     *
     * @return bool
     */
    private function isUserWeightMeasurementRequired(User $user): bool
    {
        $isWeightMeasurementRequired = false;
        $todayDate = new DateTime();
        $measurement = $this->measurementRepository->getLastMeasurement($user->getId());

        if (!empty($measurement)) {
            if ($todayDate->diff($measurement->getDate())->days >= self::USER_WEIGHT_REQUIRED_EVERY_7_DAYS) {
                $isWeightMeasurementRequired = true;
            }
        }

        return $isWeightMeasurementRequired;
    }

    /**
     * @param float $weightTarget
     * @param float $oneBeforeLastMeasurement
     * @param float $lastMeasurement
     *
     * @return bool
     */
    private function isTargetAchieved(
        float $weightTarget = null,
        float $oneBeforeLastMeasurement = null,
        float $lastMeasurement = null
    ): bool {
        if ($oneBeforeLastMeasurement <= $weightTarget && $lastMeasurement >= $weightTarget) {
            return true;
        } elseif ($oneBeforeLastMeasurement >= $weightTarget && $lastMeasurement <= $weightTarget) {
            return true;
        }

        return false;
    }

    /**
     * @param User $user
     *
     * @return array
     */
    private function getSystemInfo(User $user): array
    {
        $systemData = array_flip(User::SYSTEM_INFO_ARRAY);

        $systemInfo = !empty($user->getSystemInfo()) ? $systemData[$user->getSystemInfo()] : null;

        $data = [
            'systemInfo'    => $systemInfo,
            'systemVersion' => $user->getSystemVersion(),
            'appVersion'    => $user->getAppVersion(),
        ];

        return $data;
    }

    /**
     * @param User $user
     *
     * @return \ArrayObject
     */
    private function getMeta(User $user): \ArrayObject
    {
        $metaData = [];

        /** @var UserMeta $meta */
        foreach ($user->getMeta() as $meta) {
            $metaData[$meta->getField()] = $meta->getValue();
        }

        return new \ArrayObject($metaData);
    }


    /**
     * @param User $user
     *
     * @return array
     */
    public function getAccessControl(User $user): array
    {
        return $this->gatekeeperService->getAccessControl(
            $this->getUserRoles($user)
        );
    }

    /**
     * @param User $user
     * @param int  $requestedUserId
     * @param int  $loggedUser
     * @param bool $isGrantedSuperAdmin
     *
     * @return array
     */
    public function getUserInfoCached(
        User $user,
        int $requestedUserId,
        int $loggedUser,
        bool $isGrantedSuperAdmin
    ): array {
        if ((int) $user->getId() !== $requestedUserId) {
            return $this->getUserInfo($user, $requestedUserId, $loggedUser, $isGrantedSuperAdmin);
        }

        $cacheNameGenerator = new UserInfoCacheNameGenerator($requestedUserId);
        $cacheName = $cacheNameGenerator->get();

        $userInfo = $this->cacheManager->get($cacheName);

        if (empty($userInfo)) {
            $userInfo = $this->getUserInfo($user, $requestedUserId, $loggedUser, $isGrantedSuperAdmin);

            $this->cacheManager->save(
                $cacheName,
                $userInfo,
                [CacheManager::OPTION_TTL => $cacheNameGenerator->getTTL()]
            );
        }

        return $userInfo;
    }

    /**
     * @param int $userId
     *
     * @return array
     */
    public function getDefaultUserUnits($userId)
    {
        $userSettings = $this->settingsUserRepository->find($userId);

        if (!empty($userSettings)) {
            $units[self::WEIGHT_UNIT] = UnitType::getLabelById($userSettings->getWeightCurrentUnit());
            $units[self::SIZE_UNIT] = UnitType::getLabelById($userSettings->getHeightUnit());
        } else {
            $units[self::WEIGHT_UNIT] = null;
            $units[self::SIZE_UNIT] = null;
        }

        return $units;
    }

    /**
     * @param SettingsDietInterface $settingsDiet
     * @param int|null              $manualEnergy
     *
     * @return int|null
     */
    public function calculateEnergyInDiet(SettingsDietInterface $settingsDiet, $manualEnergy = null)
    {
        if (!is_null($manualEnergy)) {
            return (int) $manualEnergy;
        }

        // TMR - deficit
        try {
            $tmrCalculator = $this->getPreparedTMRCalculator($settingsDiet);

            if ($settingsDiet->getSettingsDietNutritionGroup() != null) {
                $tmrCalculator->setCustomDailyTargets($settingsDiet->getSettingsDietNutritionGroup()->getSchemaArray());
            }

            $energy = (int) $tmrCalculator->calculateWithDeficit(
                intval($settingsDiet->getWeightChangeDirection()),
                floatval($settingsDiet->getWeightChangeSpeedKg())
            );
        } catch (\Exception $e) {
            $energy = null;
        }

        return $energy;
    }

    /**
     * @param SettingsDietInterface $settingsDiet
     *
     * @return TMRCalculator
     * @throws \LogicException
     */
    public function getPreparedTMRCalculator(SettingsDietInterface $settingsDiet)
    {
        if (is_null($settingsDiet->getUser())) {
            throw new \LogicException("Settings are incomplete");
        }

        /** @var SettingsUser $settingsUser */
        $settingsUser = $this->settingsUserRepository->find($settingsDiet->getUser()->getId());

        if (false === $settingsUser instanceof SettingsUser) {
            throw new \LogicException("Settings are incomplete");
        }

        $birthDate = $settingsUser->getBirthDate();

        if (!$birthDate instanceof \DateTimeInterface) {
            $age = UserDemoService::DEFAULT_AGE_OF_DEMO_USER;
        } else {
            $age = (new AgeCalculator($birthDate))->calculate();
        }

        $tmrCalculator = new TMRCalculator();
        $tmrCalculator->setWeight($settingsUser->getWeightCurrentKg())
            ->setAge($age)
            ->setWeight($settingsUser->getWeightCurrentKg())
            ->setActivityBase($settingsDiet->getActivityBase())
            ->setActivityTraining(
                $settingsDiet->getActivityTraining() ? $settingsDiet->getActivityTraining(
                ) : SettingsDiet::DEFAULT_ACTIVITY_TRAINING
            )
            ->setActivityEnergyInclusionMode($settingsDiet->getActivityEnergyInclusionMode())
            ->setSex($settingsUser->getSex());


        if ($settingsDiet->getSettingsDietNutritionGroup() != null) {
            $tmrCalculator->setCustomDailyTargets($settingsDiet->getSettingsDietNutritionGroup()->getSchemaArray());
        }


        return $tmrCalculator;
    }

    /**
     * @param SettingsDietInterface $settingsDiet
     * @return BMRCalculator
     * @throws \LogicException
     */
    public function getPreparedBMRCalculator(SettingsDietInterface $settingsDiet): BMRCalculator
    {
        if (is_null($settingsDiet->getUser())) {
            throw new \LogicException("Settings are incomplete");
        }

        /** @var SettingsUser $settingsUser */
        $settingsUser = $this->settingsUserRepository->find($settingsDiet->getUser()->getId());

        if (!$settingsUser instanceof SettingsUser) {
            throw new \LogicException("Settings are incomplete");
        }

        $birthDate = $settingsUser->getBirthDate();

        if (!$birthDate instanceof \DateTimeInterface) {
            $age = UserDemoService::DEFAULT_AGE_OF_DEMO_USER;
        } else {
            $age = (new AgeCalculator($birthDate))->calculate();
        }

        $bmrCalculator = new BMRCalculator(
            $settingsUser->getWeightCurrentKg(),
            $age,
            $settingsUser->getSex()
        );

        return $bmrCalculator;
    }

    /**
     * @param SettingsDietInterface $settingsDiet
     * @return PALCalculator
     * @throws \LogicException
     */
    public function getPreparedPALCalculator(SettingsDietInterface $settingsDiet): PALCalculator
    {
        if (is_null($settingsDiet->getUser())) {
            throw new \LogicException("Settings are incomplete");
        }

        /** @var SettingsUser $settingsUser */
        $settingsUser = $this->settingsUserRepository->find($settingsDiet->getUser()->getId());

        if (!$settingsUser instanceof SettingsUser) {
            throw new \LogicException("Settings are incomplete");
        }

        $palCalculator = new PALCalculator(
            $settingsDiet->getActivityBase(),
            $settingsDiet->getActivityTraining() ? $settingsDiet->getActivityTraining() : SettingsDiet::DEFAULT_ACTIVITY_TRAINING,
            $settingsDiet->getActivityEnergyInclusionMode()
        );

        return $palCalculator;
    }

    /**
     * @param int $userId
     * @return array
     */
    public function getCalculationDetails($userId): array
    {
        /** @var SettingsDietInterface $settingsDiet */
        $settingsDiet = $this->settingsDietRepository->find($userId);

        /** @var SettingsUser $settingUser */
        $settingUser = $this->settingsUserRepository->find($userId);

        $tmr = $this->getPreparedTMRCalculator($settingsDiet);
        $bmr = $this->getPreparedBMRCalculator($settingsDiet);
        $pal = $this->getPreparedPALCalculator($settingsDiet);
        $currentWeight = $settingUser->getWeightCurrent();
        $weightChangeSpeed = $settingsDiet->getWeightChangeSpeed();
        $weightChangeDirection = $settingsDiet->getWeightChangeDirection();

        $result = [
            'TMR' => $tmr->calculate(),
            'BMR' => $bmr->calculate(),
            'BMRConstant' => $bmr->getFormulaConstants(),
            'PAL' => $pal->getPAL(),
            'weightChangeSpeed' => (float)$weightChangeSpeed,
            'weightChangeSpeedUnit' => UnitType::getLabelById($settingsDiet->getWeightChangeSpeedUnit()),
            'energy' => $tmr->calculateWithDeficit($weightChangeDirection, $weightChangeSpeed),
            'deficit' => $tmr->calculateDeficit($weightChangeDirection, $weightChangeSpeed, $tmr->calculate()),
            'weightCurrent' => (float)$currentWeight,
            'weightCurrentUnit' => UnitType::getLabelById($settingUser->getWeightCurrentUnit()),
            'weightCurrentUpdatedAt' => $this->settingsUserHistoryRepository->geDateTimeOfLastChangeCurrentWeight($userId, $currentWeight),
            'activityBase' => (float)$settingsDiet->getActivityBase(),
            'activityTraining' => (float)$settingsDiet->getActivityTraining(),
        ];

        return $result;
    }

    /**
     * @param User $user
     *
     * @return string[]
     */
    private function getUserRoles(User $user): array
    {
        $request = $this->requestStack->getMasterRequest();
        $roles = $user->getRoles();
        $locale = (string) $user->getLocale();

        // todo Request headers should be injected from controllers
        $version = $request ? (string) $request->headers->get('app-version') : null;

        if (PremiumLocaleMapper::isLocalePremium($version, $locale)) {
            $roles[] = RoleInterface::ROLE_PREMIUM_AVAILABLE;
        }

        return $roles;
    }

    /**
     * @param int    $userId
     * @param string $featureFlag
     *
     * @return bool
     */
    public function userHasPremiumFeature(int $userId, string $featureFlag): bool
    {
        if ($this->subscriptionService->isActive($userId)) {
            $user = $this->getUser($userId);
            $allRoles = $this->getAccessControl($user);

            return in_array($featureFlag, $allRoles);
        }

        return false;
    }

    /**
     * @param int $userId
     *
     * @return User
     */
    public function getUser(int $userId): User
    {
        /** @var User $user */
        $user = $this->userRepository->find($userId);

        if (false === $user instanceof User) {
            throw new UserNotFoundException($userId);
        }

        return $user;
    }

    /**
     * @param User  $user
     * @param array $data
     *
     * @return array
     */
    private function pushPremiumDataForUser(User $user, array $data): array
    {
        $dietGenerator = $this->dietGeneratorService->getForUser($user->getId());

        if (!$dietGenerator instanceof DietGenerator) {
            return $data;
        }

        $premiumData = [
            'timetable' => $dietGenerator->getTimeTable(),
            'period'    => [
                'day'  => DietGenerator::ACTIVATED_TIME['day'],
                'time' => DietGenerator::ACTIVATED_TIME['time'],
            ],
        ];

        $data = array_merge($data, $premiumData);

        return $data;
    }
}
