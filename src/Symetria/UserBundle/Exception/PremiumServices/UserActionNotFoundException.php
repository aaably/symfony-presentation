<?php

namespace Symetria\UserBundle\Exception\PremiumServices;

use Exception;
use LogicException;

/**
 * @author    Tomasz Dojcz
 * @copyright Fitatu Sp. z o.o.
 */
class UserActionNotFoundException extends LogicException
{
    /**
     * @param int            $userId
     * @param Exception|null $previous
     */
    public function __construct(int $userId, Exception $previous = null)
    {
        $message = sprintf('User action not found', $userId);

        parent::__construct($message, 422, $previous);
    }
}
