<?php

namespace Symetria\UserBundle\Exception\PremiumServices;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class NoActiveSubscriptionException extends AccessDeniedException
{
    const MESSAGE = "Given user has no active subscriptions";

    /**
     * @param string|null     $message
     * @param \Exception|null $previous
     */
    public function __construct(string $message = null, \Exception $previous = null)
    {
        $message = $message ?? static::MESSAGE;
        parent::__construct($message);
    }
}
