<?php

namespace Symetria\UserBundle\Exception\PremiumServices;

use Exception;
use LogicException;

/**
 * @author    Tomasz Dojcz
 * @copyright Fitatu Sp. z o.o.
 */
class UserNotSetException extends LogicException
{
    /**
     * @param Exception|null $previous
     */
    public function __construct(Exception $previous = null)
    {
        parent::__construct('User was not set', 422, $previous);
    }
}
