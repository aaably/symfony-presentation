<?php

namespace Symetria\UserBundle\Exception\PremiumServices;

use Exception;
use LogicException;

/**
 * @author Lukasz Domanski
 * @copyright Fitatu Sp. z o.o.
 */
class DietGeneratorSettingsNotFoundException extends LogicException
{
    /**
     * @param int            $userId
     * @param Exception|null $previous
     */
    public function __construct(int $userId, Exception $previous = null)
    {
        $msg = sprintf('Diet generator settings for user %d not found', $userId);
        parent::__construct($msg, 0, $previous);
    }
}
