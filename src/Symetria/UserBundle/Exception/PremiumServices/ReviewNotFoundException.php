<?php

namespace Symetria\UserBundle\Exception\PremiumServices;

use Doctrine\ORM\EntityNotFoundException;
/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class ReviewNotFoundException extends EntityNotFoundException
{
    const MESSAGE = 'Review with given UUID was not found (%s)';
    public function __construct(string $uuid)
    {
        parent::__construct(
            sprintf(
                static::MESSAGE,
                $uuid
            )
        );
    }

}
