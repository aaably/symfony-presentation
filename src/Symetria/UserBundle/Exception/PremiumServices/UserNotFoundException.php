<?php

namespace Symetria\UserBundle\Exception\PremiumServices;

use Exception;
use LogicException;

/**
 * @author Lukasz Domanski
 * @copyright Fitatu Sp. z o.o.
 */
class UserNotFoundException extends LogicException
{
    /**
     * @param int|string     $userNameId
     * @param Exception|null $previous
     */
    public function __construct($userNameId, Exception $previous = null)
    {
        $msg = sprintf('User %s not found.', $userNameId);
        parent::__construct($msg, 0, $previous);
    }
}
