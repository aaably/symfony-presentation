<?php

namespace Symetria\UserBundle\Exception\PremiumServices;

use Exception;
use Symetria\ApiClientBundle\Exception\InvalidArgumentException;

/**
 * @author Lukasz Domanski
 * @copyright Fitatu Sp. z o.o.
 */
class WrongDateDietGeneratorQueueException extends InvalidArgumentException
{
    /**
     * @param string         $message
     * @param Exception|null $previous
     */
    public function __construct(string $message, Exception $previous = null)
    {
        $msg = sprintf('Error occured while preparing payload for DietGenerationQueue: %s.', $message);
        parent::__construct($msg, 0, $previous);
    }
}
