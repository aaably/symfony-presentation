<?php

namespace Symetria\UserBundle\Exception\PremiumServices;

use Exception;
use LogicException;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class SubscriptionPlanNotFoundException extends LogicException
{

    public function __construct(int $planId, Exception $previous = null)
    {
        $message = sprintf('Subscription plan not found (#%d)', $planId);

        parent::__construct($message, 422, $previous);
    }
}
