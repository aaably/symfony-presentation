<?php

namespace Symetria\UserBundle\Exception\PremiumServices;

use Exception;
use LogicException;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class SubscriptionPlangroupNotFoundException extends LogicException
{

    public function __construct(int $planGroupId, Exception $previous = null)
    {
        $message = sprintf('Subscription plan group not found (#%d)', $planGroupId);

        parent::__construct($message, 422, $previous);
    }
}
