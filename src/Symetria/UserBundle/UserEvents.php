<?php

namespace Symetria\UserBundle;

/**
 * @author    Nikodem Osmialowski
 * @copyright Fitatu Sp. z o.o.
 */
final class UserEvents
{
    /**
     * Called after successful user registration or after a user was created manually.
     */
    const USER_CREATED = 'user.created';

    /**
     * Called after updating user data.
     */
    const USER_UPDATED = 'user.updated';

    /**
     * Called after user has requested our app to re-send the activation e-mail
     */
    const USER_REQUESTED_ACTIVATION_MAIL = 'user.requested_activation_mail';
}
