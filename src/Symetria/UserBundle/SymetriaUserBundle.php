<?php

namespace Symetria\UserBundle;

use Symetria\UserBundle\DependencyInjection\SymetriaUserExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class SymetriaUserBundle extends Bundle
{
    /**
     * @return SymetriaUserExtension
     */
    public function getContainerExtension()
    {
        return new SymetriaUserExtension();
    }
}
