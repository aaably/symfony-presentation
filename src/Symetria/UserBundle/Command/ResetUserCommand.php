<?php

namespace Symetria\UserBundle\Command;

use Fitatu\BillingBundle\Service\SubscriptionService;
use Fitatu\Local\Repository\Auth\MeasurementRepository;
use Fitatu\Local\Repository\Auth\PlanGroupRepository;
use Fitatu\Local\Repository\Auth\Preferences\LikeableRepository;
use Fitatu\Local\Repository\Auth\Settings\SettingsDietRepository;
use Fitatu\Local\Repository\Auth\User\UserRepository;
use Fitatu\SharedUserBundle\Model\Settings\AbstractSettingsDiet;
use Symetria\SharedBundle\Event\Subscription\SubscribeUserToPlanEvent;
use Symetria\SharedBundle\Event\User\Delete\UserDeletedEvent;
use Symetria\SharedBundle\EventBus\Dispatcher\RabbitMQEventDispatcherBridge;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class ResetUserCommand extends ContainerAwareCommand
{
    const COMMAND = 'fitatu:user:reset';

    // our test users
    const TEST_USERS = [
        5085383,
        5085387,
        5085391,
        5085395,
        5085399,
        5085403,
        5085407,
        5085411,
        5085415,
        5085419,
        5085423,
        5085431,
        5087951,
        5087955,
        5087959,
        5087963,
        5087967,
        5087971,
        5087975,
        5087979,
        5087991,
        5088011,
        5088023,
        5088039,
        5088355,
        5088359,
        5088363,
        5088367,
        5088371,
        5088379,
        5088383,
        5088407,
        5088419,
        5088427,
        5088447,
        5088451,
        5088455,
        5088459,
        5088463,
        5088467,
        5088475,
        5088559,
        5088563,
        5088567,
        5088571,
        5088575,
        5088579,
        5088583,
    ];

    /**
     * @var SubscriptionService
     */
    private $subscriptionService;

    /**
     * @var PlanGroupRepository
     */
    private $planGroupRepository;

    /**
     * @var SettingsDietRepository
     */
    private $settingsDietRepository;

    /**
     * @var bool
     */
    private $shouldSubscribe;

    /**
     * @var RabbitMQEventDispatcherBridge
     */
    private $dispatcher;

    /**
     * @var int|null
     */
    private $planGroupId;

    /**
     * @var MeasurementRepository
     */
    private $measurementRepository;

    /**
     * @var LikeableRepository
     */
    private $likeableRepository;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @param SubscriptionService           $subscriptionService
     * @param PlanGroupRepository           $planGroupRepository
     * @param SettingsDietRepository        $settingsDietRepository
     * @param RabbitMQEventDispatcherBridge $dispatcher
     * @param MeasurementRepository         $measurementRepository
     * @param LikeableRepository            $likeableRepository
     * @param UserRepository                $userRepository
     */
    public function __construct(
        SubscriptionService $subscriptionService,
        PlanGroupRepository $planGroupRepository,
        SettingsDietRepository $settingsDietRepository,
        RabbitMQEventDispatcherBridge $dispatcher,
        MeasurementRepository $measurementRepository,
        LikeableRepository $likeableRepository,
        UserRepository $userRepository
    ) {
        parent::__construct();

        $this->subscriptionService = $subscriptionService;
        $this->planGroupRepository = $planGroupRepository;
        $this->settingsDietRepository = $settingsDietRepository;
        $this->dispatcher = $dispatcher;
        $this->measurementRepository = $measurementRepository;
        $this->likeableRepository = $likeableRepository;
        $this->userRepository = $userRepository;
    }

    public function configure()
    {
        $this->setName(static::COMMAND)
             ->setDescription('Reset user settings')
             ->addArgument('userId', InputArgument::OPTIONAL, 'User ids separated by comma.', false)
             ->addOption('subscribe', 'c', InputOption::VALUE_NONE)
             ->addOption('plan', 'p', InputOption::VALUE_OPTIONAL)
             ->setHelp('This command resets user settings and creates new subscription');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @return int
     */
    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->shouldSubscribe = $input->getOption('subscribe');
        $io = new SymfonyStyle($input, $output);
        $io->title('Reset User');

        $users = $this->prepareUsers($input);
        $progress = new ProgressBar($output, count($users));

        $successCount = 0;
        $failCount = 0;

        $settings = $this->settingsDietRepository->findBy(['user' => $users]);

        /** @var AbstractSettingsDiet $setting */
        foreach ($settings as $setting) {
            try {
                $this->handleReset($input, $output, $setting);
                ++$successCount;
            } catch (\Exception $e) {
                $io->error($e->getMessage());
                ++$failCount;
            }
            $progress->advance();
        }

        $progress->finish();
        $io->newLine(2);

        if ($failCount) {
            $io->warning('Total errors: '.$failCount);
        }
        if ($successCount) {
            $io->success('Total users reset: '.$successCount);
        }

        return 0;
    }

    /**
     * @param InputInterface $input
     * @return array
     */
    private function prepareUsers(InputInterface $input): array
    {
        $users = $input->getArgument('userId');

        if (strpos($users, ',') !== false) {
            return explode(',', $users);
        }

        if (empty($users)) {
            return static::TEST_USERS;
        }

        return [$users];
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @return int
     */
    private function getPlanGroupId(InputInterface $input, OutputInterface $output): int
    {
        if ($this->planGroupId) {
            return $this->planGroupId;
        }
        if ($planId = $input->getOption('plan')) {
            return intval($planId);
        }

        return $this->planGroupId = (int)$this->askForPlanGroup($input, $output);
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @return int
     */
    private function askForPlanGroup($input, $output): int
    {
        $planGroups = collect($this->planGroupRepository->findAll())->map->toArray();

        $helper = $this->getHelper('question');
        $question = new ChoiceQuestion(
            'Please select a Plan Group',
            collect($planGroups)->pluck('name')->toArray()
        );
        $planGroupName = $helper->ask($input, $output, $question);

        $selectGroup = function ($group) use ($planGroupName) {
            return $group['name'] == $planGroupName;
        };

        return collect($planGroups)->filter($selectGroup)->pluck('id')->first();
    }

    /**
     * @param InputInterface       $input
     * @param OutputInterface      $output
     * @param AbstractSettingsDiet $setting
     */
    private function handleReset(InputInterface $input, OutputInterface $output, AbstractSettingsDiet $setting)
    {
        $this->settingsDietRepository->addOrUpdate($setting->reset());

        $this->measurementRepository->removeByUser($setting->getUser());
        $this->likeableRepository->removeByUser($setting->getUser());

        if ($this->shouldSubscribe) {
            $event = new SubscribeUserToPlanEvent(
                $setting->getUser()->getId(),
                $this->subscriptionService->getPlanFromPlanGroup($this->getPlanGroupId($input, $output))
            );

            $this->dispatcher->dispatch($event);
        }

        $this->sendEvent($setting);
    }

    /**
     * @param AbstractSettingsDiet $setting
     */
    private function sendEvent(AbstractSettingsDiet $setting)
    {
        $user = $this->userRepository->find($setting->getUserId());
        $event = (new UserDeletedEvent($user->getId()))->setLocale($user->getLocale());
        $this->dispatcher->dispatch($event);
    }
}
