<?php

namespace Symetria\UserBundle\Command;

use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Fitatu\DatabaseBundle\Entity\Auth\User\User;
use Fitatu\Local\Repository\Auth\RoleRepository;
use Fitatu\Local\Repository\Auth\User\UserRepository;
use FOS\UserBundle\Util\UserManipulator;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputOption;

/**
 * @author Lukasz Domanski
 * @copyright Fitatu Sp. z o.o.
 */
class CreateUserCommand extends Command
{
    private const COMMAND = 'fitatu:user:create';

    private const PARTNER_ID = 100;
    private const PASSWORD = 'f';
    private const DOMAIN = '@fitatu.com';
    private const SUPPORTED_LOCALE = [
        'pl_PL',
        'en_GB',
        'nl_NL',
        'fr_FR',
        'de_DE',
        'it_IT',
        'es_ES',
    ];
    /**
     * array[role => planGroup.id]
     */
    private const ROLES_PREMIUM = [
        'ROLE_USER'                 => null,
        'ROLE_PREMIUM'              => 3,
        'ROLE_PREMIUM_WITHOUT_DIET' => 5,
    ];

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var UserManipulator
     */
    private $userManipulator;

    /**
     * @var RoleRepository
     */
    private $roleRepository;

    /**
     * @var DefaultSettingsSetter
     */
    private $defaultSettingsSetter;

    /**
     * @var KernelInterface
     */
    private $kernel;

    /**
     * @param LoggerInterface        $logger
     * @param EntityManagerInterface $entityManager
     * @param UserRepository         $userRepository
     * @param UserManipulator        $userManipulator
     * @param RoleRepository         $roleRepository
     * @param KernelInterface        $kernel
     */
    public function __construct(
        LoggerInterface $logger,
        EntityManagerInterface $entityManager,
        UserRepository $userRepository,
        UserManipulator $userManipulator,
        RoleRepository $roleRepository,
        DefaultSettingsSetter $defaultSettingsSetter,
        KernelInterface $kernel
    ) {
        parent::__construct();

        $this->logger = $logger;
        $this->entityManager = $entityManager;
        $this->userRepository = $userRepository;
        $this->userManipulator = $userManipulator;
        $this->roleRepository = $roleRepository;
        $this->defaultSettingsSetter = $defaultSettingsSetter;
        $this->kernel = $kernel;
    }

    public function configure()
    {
        $this
            ->setName(self::COMMAND)
            ->setDescription('Create user')
            ->addOption('username', 'u', InputOption::VALUE_OPTIONAL)
            ->addOption('locale', 'l', InputOption::VALUE_OPTIONAL)
            ->addOption('role', 'r', InputOption::VALUE_OPTIONAL)
            ->setHelp('Example: fitatu:user:create -l en_GB -u testEN -r ROLE_PREMIUM');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->entityManager->getConfiguration()->setSQLLogger(null);

        $io = new SymfonyStyle($input, $output);
        $io->title('Create test users');

        $locale = $this->getLocale($io, $input);
        $userName = $this->getUsername($io, $input);
        $role = $this->getRole($io, $input);

        try {
            $user = $this->checkAndCreateUser($userName, $locale, $role, $io);
        } catch (Exception $e) {
            $io->error($e->getMessage());

            return 0;
        }

        $table = new Table($io);
        $table->setHeaders(['Locale', 'ID', 'Login', 'Password', 'e-mail']);

        $table->addRow(
            [
                $user->getLocale(),
                $user->getId(),
                $user->getUsername(),
                self::PASSWORD,
                $user->getEmail(),
            ]
        );

        $io->success('Finished');

        $table->render();

        $io->newLine();

        return 0;
    }

    /**
     * @param SymfonyStyle   $io
     * @param InputInterface $input
     * @return string
     */
    private function getLocale(SymfonyStyle $io, InputInterface $input): string
    {
        if ($locale = $input->getOption('locale')) {
            return $locale;
        }

        $question = new ChoiceQuestion('[1/3] Select a language:', static::SUPPORTED_LOCALE, 0);
        $question->setErrorMessage('Locale %s is invalid.');

        return $io->askQuestion($question);
    }

    /**
     * @param SymfonyStyle   $io
     * @param InputInterface $input
     * @return string
     */
    private function getUsername(SymfonyStyle $io, InputInterface $input): string
    {
        if ($username = $input->getOption('username')) {
            return $username;
        }

        return $io->ask('[2/3] Type username');
    }

    /**
     * @param SymfonyStyle   $io
     * @param InputInterface $input
     * @return string
     */
    private function getRole(SymfonyStyle $io, InputInterface $input): string
    {
        if ($role = $input->getOption('role')) {
            return $role;
        }

        $question = new ChoiceQuestion('[3/3] Select a role:', array_keys(static::ROLES_PREMIUM), 0);
        $question->setErrorMessage('Role %s is invalid.');

        return $io->askQuestion($question);
    }

    /**
     * @param string          $userName
     * @param string          $locale
     * @param string          $role
     * @param OutputInterface $io
     *
     * @return User
     */
    private function checkAndCreateUser(string $userName, string $locale, string $role, OutputInterface $io): User
    {
        $email = sprintf("%s%s", $userName, self::DOMAIN);

        $user = $this->userRepository->findOneBy(['username' => $userName, 'enabled' => true]);

        if ($user instanceof User) {
            throw new Exception(sprintf('User [%s] is already exists!', $userName));
        }

        $io->writeln(sprintf("Creating user [%s]", $userName));
        $user = $this->createUser($userName, $email, $locale, $role, $io);

        $io->writeln(sprintf("Creating default settings for user [%s]", $userName));

        $this->defaultSettingsSetter->setDefault($user);

        return $user;
    }

    /**
     * @param string          $userName
     * @param string          $email
     * @param string          $locale
     * @param string          $role
     * @param OutputInterface $output
     *
     * @return User
     * @throws Exception
     */
    private function createUser(
        string $userName,
        string $email,
        string $locale,
        string $role,
        OutputInterface $output
    ): User {
        /** @var User $user */
        $user = $this->userManipulator->create($userName, self::PASSWORD, $email, true, false);

        $roleUser = $this->roleRepository->findOneBy(['role' => 'ROLE_USER']);
        $roleLocale = $this->roleRepository->findOneBy(['role' => $user->formatRoleLocale($locale)]);

        $user->addRole($roleUser);
        $user->addRole($roleLocale);
        $user->setPartnerId(self::PARTNER_ID);
        $user->setCreatedAt(new DateTime());
        $user->setLocale($locale);
        $user->setSearchLocale($locale);
        $user->setTermsAccepted(true);
        $user->setMarketingAccepted(true);

        $this->userRepository->persist($user);

        $this->subscribeUser($user, $role, $output);

        return $user;
    }

    /**
     * @param User            $user
     * @param string          $role
     * @param OutputInterface $output
     */
    private function subscribeUser(User $user, string $role, OutputInterface $output)
    {
        $application = new Application($this->kernel);
        $application->setAutoExit(false);

        $planGroupId = static::ROLES_PREMIUM[$role];

        if (empty($planGroupId)) {
            return;
        }

        $input = new ArrayInput([
            'command' => 'fitatu:user:subscribe',
            'user'    => $user->getId(),
            '-g'      => $planGroupId,
        ]);

        $this->getApplication()->doRun($input, $output);

    }
}
