<?php

namespace Symetria\UserBundle\Controller\UserSettingsDiet;

use Fitatu\DatabaseBundle\Entity\Auth\Settings\SettingsDiet;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Put;
use League\Tactician\CommandBus;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symetria\ApiServerBundle\Util\Serializer\ObjectSerializer;
use Symetria\UserBundle\Controller\AbstractController;
use Symetria\UserBundle\Handler\Settings\Diet\Update\SettingsDietUpdateCommand;
use Symetria\UserBundle\Handler\Settings\Preferences\Update\PreferencesUserMealSchemaPostCommand;
use Symetria\UserBundle\Service\DietSettingsService;
use Symetria\UserBundle\Service\Preferences\MealSchemaConverter;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * @author    Lukasz Domanski
 * @copyright Fitatu Sp. z o.o.
 */
class UserSettingsDietController extends AbstractController
{
    /**
     * @var CommandBus
     */
    private $commandBus;

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @var ObjectSerializer
     */
    private $objectSerializer;

    /**
     * @var DietSettingsService
     */
    private $dietSettingsService;

    /**
     * @var MealSchemaConverter
     */
    private $mealSchemaConverter;

    /**
     * @param ContainerInterface  $container
     * @param RequestStack        $requestStack
     * @param CommandBus          $commandBus
     * @param ObjectSerializer    $objectSerializer
     * @param DietSettingsService $dietSettingsService
     * @param MealSchemaConverter $mealSchemaConverter
     */
    public function __construct(
        ContainerInterface $container,
        RequestStack $requestStack,
        CommandBus $commandBus,
        ObjectSerializer $objectSerializer,
        DietSettingsService $dietSettingsService,
        MealSchemaConverter $mealSchemaConverter
    )
    {
        $this->setContainer($container);

        $this->requestStack = $requestStack;
        $this->commandBus = $commandBus;
        $this->objectSerializer = $objectSerializer;
        $this->dietSettingsService = $dietSettingsService;
        $this->mealSchemaConverter = $mealSchemaConverter;
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  authentication=true,
     *  section="Settings",
     *  description="Save user's diet settings",
     *  method="PUT",
     *  uri="/api/diet-plan/{userId}/settings",
     *  tags={
     *      "stable" = "green",
     *      "tested" = "green"
     *  },
     *  requirements={
     *      {"name"="userId", "dataType"="int", "required"=true, "requirement"="\d+", "description"="User ID"}
     *  },
     *  parameters={
     *      {"name"="activityBase", "dataType"="float", "required"=false, "description"="Base activity [1.4, 1.5, 1.7, 2.0]"},
     *      {"name"="activityTraining", "dataType"="float", "required"=false, "description"="Training activity [1.4, 1.5, 1.7, 2.0]"},
     *      {"name"="activityEnergyInclusionMode", "dataType"="string", "required"=false, "description"="IGNORE_ENTIRELY_ACTIVITY_ENERGY, INCREASE_ENERGY_BY_ACTIVITY_TRAINING, INCLUDE_ADDED_ACTIVITIES_IN_ENERGY, INCLUDE_ADDED_ACTIVITIES_AND_INCREASE_ENERGY_BY_ACTIVITY_TRAINING"},
     *      {"name"="weightChangeSpeed", "dataType"="float", "required"=false, "description"="Speed of weight change [0.0 - 1.0]"},
     *      {"name"="weightChangeSpeedUnit", "dataType"="string", "required"=false, "description"="KG, ST_LB, LB"},
     *      {"name"="manualEnergyTarget", "dataType"="boolean", "required"=false, "description"="Manual energy settings", "format"="true|false"},
     *      {"name"="energy", "dataType"="int", "required"=false, "description"="Energy in kilocalories"},
     *      {"name"="proteinPercentage", "dataType"="int", "required"=false, "description"="% protein"},
     *      {"name"="proteinWeight", "dataType"="int", "required"=false, "description"="Weight of protein (g)"},
     *      {"name"="fatPercentage", "dataType"="int", "required"=false, "description"="% fat"},
     *      {"name"="fatWeight", "dataType"="int", "required"=false, "description"="Weight of fat (g)"},
     *      {"name"="carbohydratePercentage", "dataType"="int", "required"=false, "description"="% carbohydrate"},
     *      {"name"="carbohydrateWeight", "dataType"="int", "required"=false, "description"="Weight of carbohydrate (g)"},
     *      {"name"="mealSchema[0]", "dataType"="array", "required"=true, "description"="Full array of meal schema ex. 'breakfast'"},
     *      {"name"="nutritionSchema[]", "dataType"="array", "required"=false, "description"="Full array of daily targets/nutrition schema"}
     *  },
     *  statusCodes={
     *      200="Returned was successful",
     *      403="Access denied",
     *      400={
     *              "UserId is not defined",
     *              "Request value is not an integer",
     *              "The value must be between 0 and 1",
     *              "The value must be between 0 and 2",
     *              "Allowed values: [-1,0,1]",
     *              "The value must be greater than 0",
     *              "Allowed values: [0,1]",
     *              "The value must be between 0 and 100",
     *              "Meals schema should be a json array of meals",
     *              "MealSchema is undefined",
     *              "Nutrients must be defined (if manualEnergyTarget=1)",
     *              "Nutrients percentage must be 100 (if manualEnergyTarget=1)",
     *              "Energy must be defined (if manualEnergyTarget=1)",
     *              "User not found"
     *          }
     *  }
     * )
     *
     * @Put("/api/diet-plan/{userId}/settings")
     *
     * @return JsonResponse
     * @throws AccessDeniedHttpException
     */
    public function putDietSettingsAction()
    {
        $request = $this->requestStack->getCurrentRequest();

        $userId = (int)$request->get('userId');

        if ($this->checkAccess($userId) === false) {
            throw new AccessDeniedHttpException('Access denied.');
        }

        $command = new SettingsDietUpdateCommand($userId);
        $command->activityBase = $request->get('activityBase');
        $command->activityTraining = $request->get('activityTraining');

        $activityEnergyInclusionMode = $request->get('activityEnergyInclusionMode');

        if (!is_null($activityEnergyInclusionMode)) {
            $command->activityEnergyInclusionMode = SettingsDiet::activityEnergyInclusionModeLabelToValue(
                $activityEnergyInclusionMode
            );
        } else {
            $command->activityEnergyInclusionMode = SettingsDiet::IGNORE_ENTIRELY_ACTIVITY_ENERGY;
        }

        $command->weightChangeSpeed = $request->get('weightChangeSpeed');
        $command->weightChangeSpeedUnit = $request->get('weightChangeSpeedUnit');

        if ((int)$request->get('manualEnergyTarget', false) === 1) {
            $command->manualEnergyTarget = true;
            $command->energy = $request->get('energy');
        } else {
            $command->manualEnergyTarget = false;
        }

        $command->proteinPercentage = (int)$request->get('proteinPercentage');
        $command->proteinWeight = (int)$request->get('proteinWeight');
        $command->fatPercentage = (int)$request->get('fatPercentage');
        $command->fatWeight = (int)$request->get('fatWeight');
        $command->carbohydratePercentage = (int)$request->get('carbohydratePercentage');
        $command->carbohydrateWeight = (int)$request->get('carbohydrateWeight');

        $mealSchema = $request->get('mealSchema');
        $command->mealSchema = $mealSchema;

        $command->nutritionSchema = $request->get('nutritionSchema');

        try {
            $this->commandBus->handle($command);

            /* @todo Temporary disabled, needs further investigation about overwritten meal schema hours */
//            $mealSchemaCommand = new PreferencesUserMealSchemaPostCommand($this->getUser());
//            $mealSchemaCommand->mealSchema = $this->mealSchemaConverter->convert($mealSchema);
//            $this->commandBus->handle($mealSchemaCommand);
        } catch (\LogicException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        $settings = $this->dietSettingsService->getDietSettings($userId);

        return new JsonResponse($settings, Response::HTTP_OK);
    }


    /**
     * Example of response:
     *
     *      {
     *          userId: "989628",
     *          activityBase: 1.5,
     *          activityTraining: 1.5,
     *          activityEnergyInclusionMode: "INCLUDE_ADDED_ACTIVITIES_AND_INCREASE_ENERGY_BY_ACTIVITY_TRAINING",
     *          weightChangeSpeed: 0.1,
     *          weightChangeSpeedKg: 0.1,
     *          weightChangeSpeedUnit: "KG",
     *          weightChangeDirection: -1,
     *          manualEnergyTarget: false,
     *          energy: 2621,
     *          proteinPercentage: null,
     *          proteinWeight: null,
     *          fatPercentage: null,
     *          fatWeight: null,
     *          carbohydratePercentage: null,
     *          carbohydrateWeight: null,
     *          mealSchema: [
     *              "breakfast",
     *              "second_breakfast",
     *              "dinner",
     *              "supper"
     *          ],
     *          meatlessFridays: false,
     *          TMR: 2721,
     *          excludedProducts: [
     *              "Jaja kurze",
     *              "Bakłażan"
     *          ],
     *          mealSchemaPreferences: {
     *              breakfast: [
     *                  "mealName": "Śniadanko",
     *                  "mealTime": "07:30",
     *                  "items": [
     *                      "kanapki",
     *                      "musli"
     *                  ],
     *                  "preparationTime": 30,
     *                  "coldDish": true,
     *                  "warmDish": true,
     *                  "reheatDish": false,
     *              ],
     *              second_breakfast: [
     *                  "mealName": "II śniadanie",
     *                  "mealTime": "09:00",
     *                  "items": [],
     *                  "preparationTime": 30,
     *                  "coldDish": true,
     *                  "warmDish": true,
     *                  "reheatDish": false,
     *              ],
     *              lunch: [
     *                  "mealName": "Zapieksy",
     *                  "mealTime": "10:30",
     *                  "items": [
     *                      "owsianki",
     *                      "batony zbożowe"
     *                  ],
     *                  "preparationTime": 9999,
     *                  "coldDish": true,
     *                  "warmDish": true,
     *                  "reheatDish": false,
     *              ],
     *              dinner: [
     *                  "mealName": "Obiad",
     *                  "mealTime": "12:30",
     *                  "items": [],
     *                  "preparationTime": 9999,
     *                  "coldDish": true,
     *                  "warmDish": true,
     *                  "reheatDish": false,
     *              ],
     *              snack: [
     *                  "mealName": "Przękaski",
     *                  "mealTime": "14:30",
     *                  "items": [],
     *                  "preparationTime": 30,
     *                  "coldDish": true,
     *                  "warmDish": true,
     *                  "reheatDish": false,
     *              ],
     *              supper: [
     *                  "mealName": "Kolacja",
     *                  "mealTime": "19:00",
     *                  "items": [],
     *                  "preparationTime": 30,
     *                  "coldDish": true,
     *                  "warmDish": true,
     *                  "reheatDish": false,
     *              ]
     *          },
     *          dietGeneration": {
     *              "timeTable": {
     *                  "from": "2017-07-03",
     *                  "to": "2017-07-09",
     *                  "nextAt": "2017-07-03 08:12"
     *              }
     *          },
     *          nutritionSchema: {
     *              "wednesday": {
     *                "energy":1900,
     *                "carbohydratePercentage":60,
     *                "carbohydrateWeight":285,
     *                "fatPercentage":25,
     *                "fatWeight":53,
     *                "proteinPercentage":15,
     *                "proteinWeight":71
     *              }
     *          },
     *          calculatedTempo: 0.5,
     *      }
     *
     * @ApiDoc(
     *  resource=true,
     *  authentication=true,
     *  section="Settings",
     *  description="Get the latest user's diet settings",
     *  method="GET",
     *  uri="/api/diet-plan/{userId}/settings",
     *  tags={
     *      "stable" = "green",
     *      "tested" = "green"
     *  },
     *  requirements={
     *      {"name"="userId", "dataType"="int", "required"=true, "requirement"="\d+", "description"="User ID"}
     *  },
     *  statusCodes={
     *      200="Returned was successful",
     *      403="Access denied",
     *      400={
     *              "Not a valid date format. Allowed date format is YYYY-mm-dd"
     *          },
     *      404="User not found",
     *  }
     * )
     *
     * @Get("/api/diet-plan/{userId}/settings")
     *
     * @return JsonResponse
     * @throws AccessDeniedHttpException
     */
    public function getLatestDietPlanSettingsAction(): JsonResponse
    {
        $request = $this->requestStack->getCurrentRequest();
        $userId = (int)$request->get('userId');

        if ($this->checkAccess($userId) === false) {
            throw new AccessDeniedHttpException('Access denied.');
        }

        $settings = $this->dietSettingsService->getDietSettings($userId);

        return new JsonResponse($settings, Response::HTTP_OK);
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  authentication=true,
     *  section="Settings",
     *  description="Get the user's diet settings by day",
     *  method="GET",
     *  uri="/api/diet-plan/{userId}/settings/{date}",
     *  tags={
     *      "stable" = "green",
     *      "tested" = "green",
     *      "cached" = "green"
     *  },
     *  requirements={
     *      {"name"="userId", "dataType"="int", "required"=true, "requirement"="\d+", "description"="User ID"},
     *      {"name"="date", "dataType"="date", "required"=true, "requirement"="[0-9]{4}\-[0-9]{2}\-[0-9]{2}", "description"="Date ex. 2015-10-20"},
     *  },
     *  statusCodes={
     *      200="Returned was successful",
     *      403="Access denied",
     *      400={
     *              "Not a valid date format. Allowed data format is YYYY-mm-dd"
     *          },
     *      404={
     *              "User not found",
     *              "Settings nof found"
     *          },
     *  }
     * )
     *
     * @Get("/api/diet-plan/{userId}/settings/{date}")
     *
     * @return JsonResponse
     * @throws AccessDeniedHttpException
     */
    public function getDietPlanSettingsByDayAction()
    {
        $request = $this->requestStack->getCurrentRequest();
        $userId = (int)$request->get('userId');
        $date = $request->get('date');

        if ($this->checkAccess($userId) === false) {
            throw new AccessDeniedHttpException('Access denied.');
        }

        $settings = $this->dietSettingsService->getDietSettings($userId, $date);

        return new JsonResponse($settings, Response::HTTP_OK);
    }
}
