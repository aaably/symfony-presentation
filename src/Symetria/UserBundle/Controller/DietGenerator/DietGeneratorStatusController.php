<?php

namespace Symetria\UserBundle\Controller\DietGenerator;

use Fitatu\ApiClientBundle\Exception\InvalidIntranetTokenHttpException;
use Fitatu\ApiClientBundle\Security\IntranetTokenValidator;
use FOS\RestBundle\Controller\Annotations\Get;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symetria\UserBundle\Controller\AbstractController;
use Symetria\UserBundle\Diet\Generator\Stats\StatsProvider;
use Symetria\UserBundle\Diet\Generator\Stats\StatsSerializer;
use Symetria\UserBundle\Exception\PremiumServices\DietGeneratorSettingsNotFoundException;
use Symetria\UserBundle\Service\DietGeneratorService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class DietGeneratorStatusController extends AbstractController
{
    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @var DietGeneratorService
     */
    private $dietGeneratorService;

    /**
     * @var StatsProvider
     */
    private $statsProvider;

    /**
     * @var StatsSerializer
     */
    private $statsSerializer;

    /**
     * @var IntranetTokenValidator
     */
    private $intranetTokenValidator;

    /**
     * @param RequestStack           $requestStack
     * @param DietGeneratorService   $dietGeneratorService
     * @param StatsProvider          $statsProvider
     * @param StatsSerializer        $statsSerializer
     * @param IntranetTokenValidator $intranetTokenValidator
     */
    public function __construct(
        RequestStack $requestStack,
        DietGeneratorService $dietGeneratorService,
        StatsProvider $statsProvider,
        StatsSerializer $statsSerializer,
        IntranetTokenValidator $intranetTokenValidator
    ) {
        $this->requestStack = $requestStack;
        $this->dietGeneratorService = $dietGeneratorService;
        $this->statsProvider = $statsProvider;
        $this->statsSerializer = $statsSerializer;
        $this->intranetTokenValidator = $intranetTokenValidator;
    }

    /**
     *
     *      Example of response:
     *
     *      {
     *          isReviewed: true
     *      }
     *
     * @ApiDoc(
     *  resource=true,
     *  section="Diet Generator",
     *  authentication=true,
     *  description="Get Diet Generator Status",
     *  tags={
     *      "stable"="green",
     *      "tested"="green"
     *  },
     *  requirements={
     *      {"name"="userId", "dataType"="int", "requirement"="\d+", "description"="User ID"}
     *  },
     *  statusCodes={
     *      200="Successful user data response",
     *      401="Unauthorized - invalid or no token",
     *      403="Not authorized to retrieve user data of this user",
     *      404={
     *          "Returned when the user is not found",
     *          "Returned when required related user data is not found"
     *      }
     *  }
     * )
     *
     * @Get("/diet-generator/{userId}/status")
     *
     * @param int $userId
     *
     * @return JsonResponse
     */
    public function indexAction(int $userId): JsonResponse
    {
        if ($this->checkAccess($userId) === false) {
            throw new AccessDeniedHttpException('Access denied.');
        }

        try{
            $results = $this->dietGeneratorService->getStatusForUser($userId);
            return new JsonResponse($results, Response::HTTP_OK);
        } catch(DietGeneratorSettingsNotFoundException $e) {
            return new JsonResponse([
                'error' => $e->getMessage()
            ], Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  section="Diet Generator",
     *  authentication=true,
     *  description="Diet generator stats and issues",
     *  tags={
     *      "stable"="green",
     *      "tested"="red"
     *  },
     *  statusCodes={
     *      200="Successful user data response",
     *      401="Unauthorized - invalid or no token"
     *  },
     *  output="Symetria\UserBundle\Diet\Generator\Stats\Response\StatsApiResponse"
     * )
     *
     * @Get("/diet-generator/stats")
     *
     * @return JsonResponse
     *
     * @throws InvalidIntranetTokenHttpException
     */
    public function statsAction(): JsonResponse
    {
        $this->validateIntranetToken(
            $this->getIntranetTokenFromRequest()
        );

        $stats = $this->statsProvider->getStats();

        return new JsonResponse(
            json_decode(
                $this->statsSerializer->serialize($stats),
                true
            )
        );
    }

    /**
     * @return string
     */
    private function getIntranetTokenFromRequest(): string
    {
        $request = $this->requestStack->getCurrentRequest();

        return (string)$request->get('intranetToken');
    }

    /**
     * @param string $intranetToken
     *
     * @return void
     *
     * @throws InvalidIntranetTokenHttpException
     */
    private function validateIntranetToken(string $intranetToken): void
    {
        if (!$this->intranetTokenValidator->validate($intranetToken)) {
            throw new InvalidIntranetTokenHttpException();
        }
    }
}
