<?php

namespace Symetria\UserBundle\Controller\DietGenerator;

use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Patch;
use League\Tactician\CommandBus;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symetria\ApiServerBundle\Util\Serializer\ObjectSerializer;
use Symetria\UserBundle\Controller\AbstractController;
use Symetria\UserBundle\Service\DietGeneratorService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class DietGeneratorController extends AbstractController
{
    /**
     * @var DietGeneratorService
     */
    private $dietGeneratorService;

    /**
     * @var CommandBus
     */
    private $commandBus;

    /**
     * @var ObjectSerializer
     */
    private $objectSerializer;

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @param RequestStack         $requestStack
     * @param CommandBus           $commandBus
     * @param ObjectSerializer     $objectSerializer
     * @param DietGeneratorService $dietGeneratorService
     */
    public function __construct(
        RequestStack $requestStack,
        CommandBus $commandBus,
        ObjectSerializer $objectSerializer,
        DietGeneratorService $dietGeneratorService
    ) {
        $this->commandBus = $commandBus;
        $this->objectSerializer = $objectSerializer;
        $this->dietGeneratorService = $dietGeneratorService;
        $this->requestStack = $requestStack;
    }

    /**
     *
     *      Example of response:
     *
     *      {
     *          locale: "fr_FR",
     *          meal: "breakfast",
     *          from: "2017-12-08",
     *          to: "2018-01-14",
     *          nextAt: "2017-12-08 09:07",
     *          isReviewed: true,
     *          remainingDates: {
     *              2017-03-30: {
     *                  breakfast: {
     *                      key: breakfast,
     *                      name: Śniadanie,
     *                      time: 08:10
     *                  }
     *              },
     *              2017-03-31: {
     *                  breakfast: {
     *                      key: breakfast,
     *                      name: Śniadanie,
     *                      time: 08:10
     *                  }
     *              }, ...
     *          },
     *      }
     *
     * @ApiDoc(
     *  resource=true,
     *  section="Diet Generator",
     *  authentication=true,
     *  description="Get Diet Generator Settings",
     *  tags={
     *      "stable"="green",
     *      "tested"="green"
     *  },
     *  requirements={
     *      {"name"="userId", "dataType"="int", "requirement"="\d+", "description"="User ID"}
     *  },
     *  statusCodes={
     *      200="Successful user data response",
     *      401="Unauthorized - invalid or no token",
     *      403="Not authorized to retrieve user data of this user",
     *      404={
     *          "Returned when the user is not found",
     *          "Returned when required related user data is not found"
     *      }
     *  }
     * )
     *
     * @Get("/diet-generator/{userId}/settings")
     *
     * @param int $userId
     *
     * @return JsonResponse
     */
    public function getAction(int $userId): JsonResponse
    {
        if ($this->checkPremiumAccess($userId) === false) {
            throw new AccessDeniedHttpException('Access denied.');
        }
        $results = $this->dietGeneratorService->getFormattedForUser($userId);

        return new JsonResponse($results, Response::HTTP_OK);
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  section="Diet Generator",
     *  authentication=true,
     *  description="Update Diet Generator Settings",
     *  tags={
     *      "stable"="green",
     *      "tested"="green"
     *  },
     *  requirements={
     *      {"name"="userId", "dataType"="int", "requirement"="\d+", "description"="User ID"}
     *  },
     *  parameters={
     *      {"name"="locale", "dataType"="string", "required"=false, "description"="Generation language"},
     *      {"name"="mealName", "dataType"="string", "required"=false, "description"="Breakpoint meal name", "default"="breakfast"},
     *      {"name"="isReviewed", "dataType"="boolean", "required"=false, "description"="Have user seen default settings?", "default"="false"},
     *      {"name"="requestAt", "dataType"="datetime", "required"=false, "description"="Diet generation datetime e.g. 2017-03-21 12:00"},
     * },
     *  statusCodes={
     *      200="Successful user data response",
     *      401="Unauthorized - invalid or no token",
     *      403="User has no active subscription",
     *      404={
     *          "Returned when the user is not found",
     *          "Returned when required related user data is not found"
     *      }
     *  }
     * )
     *
     * @Patch("/diet-generator/{userId}/settings")
     *
     * @param int $userId
     *
     * @return JsonResponse
     */
    public function updateAction(int $userId): JsonResponse
    {
        if ($this->checkAccess($userId) === false) {
            throw new AccessDeniedHttpException('Access denied.');
        }
        $data = $this->requestStack->getCurrentRequest()->request->all();

        $results = $this->dietGeneratorService
            ->update($userId, $data)
            ->toArray();

        return new JsonResponse($results, Response::HTTP_OK);
    }
}
