<?php

namespace Symetria\UserBundle\Controller\SettingsPremium;

use FOS\RestBundle\Controller\Annotations\Patch;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symetria\UserBundle\Controller\AbstractController;
use Symetria\UserBundle\Service\SettingsPremiumService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class SettingsPremiumController extends AbstractController
{
    /**
     * @var RequestStack
     */
    private $requestStack;
    /**
     * @var SettingsPremiumService
     */
    private $settingsPremiumService;

    /**
     * @param RequestStack           $requestStack
     * @param SettingsPremiumService $settingsPremiumService
     */
    public function __construct(
        RequestStack $requestStack,
        SettingsPremiumService $settingsPremiumService
    ) {
        $this->requestStack = $requestStack;
        $this->settingsPremiumService = $settingsPremiumService;
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  section="Premium",
     *  authentication=true,
     *  description="Updates premium settings",
     *  tags={
     *      "stable"="green",
     *      "tested"="green"
     *  },
     *  requirements={
     *      {"name"="userId", "dataType"="int", "requirement"="\d+", "description"="User ID"}
     *  },
     *  parameters={
     *      {"name"="locale", "dataType"="string", "required"=false, "description"="Generation language"},
     *      {"name"="matchingProcess", "dataType"="boolean", "required"=false, "description"="Should display matching process?", "default"="true"},
     * },
     *  statusCodes={
     *      200="Successful user data response",
     *      401="Unauthorized - invalid or no token",
     *      403="User has no active subscription",
     *      404={
     *          "Returned when the user is not found",
     *          "Returned when required related user data is not found"
     *      }
     *  }
     * )
     *
     * @Patch("/users/{userId}/settings/premium")
     *
     * @param int $userId
     *
     * @return JsonResponse
     */
    public function updateSettingsPremiumAction(int $userId)
    {
        if ($this->checkAccess($userId) === false) {
            throw new AccessDeniedHttpException('Access denied.');
        }
        $data = $this->requestStack->getCurrentRequest()->request->all();

        return new JsonResponse(
            $this->settingsPremiumService->update($userId, $data)->toArray(),
            Response::HTTP_OK
        );
    }
}
