<?php

namespace Symetria\UserBundle\Controller\UserPreferences;

use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use League\Tactician\CommandBus;
use LogicException;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symetria\ApiServerBundle\Util\Serializer\ObjectSerializer;
use Fitatu\BillingBundle\Service\SubscriptionService;
use Symetria\UserBundle\Controller\AbstractController;
use Symetria\UserBundle\Handler\Settings\Preferences\Update\PreferencesUserMealSchemaPostCommand;
use Symetria\UserBundle\Service\Preferences\MealSchemaService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * @author Lukasz Domanski
 * @copyright Fitatu Sp. z o.o.
 */
class MealSchemaController extends AbstractController
{
    /**
     * @var CommandBus
     */
    private $commandBus;

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @var ObjectSerializer
     */
    private $objectSerializer;

    /**
     * @var MealSchemaService
     */
    private $mealSchemaService;

    /**
     * @var SubscriptionService
     */
    private $subscriptionService;

    /**
     * @param RequestStack        $requestStack
     * @param CommandBus          $commandBus
     * @param ObjectSerializer    $objectSerializer
     * @param MealSchemaService   $mealSchemaService
     * @param SubscriptionService $subscriptionService
     */
    public function __construct(
        RequestStack $requestStack,
        CommandBus $commandBus,
        ObjectSerializer $objectSerializer,
        MealSchemaService $mealSchemaService,
        SubscriptionService $subscriptionService
    ) {
        $this->requestStack = $requestStack;
        $this->commandBus = $commandBus;
        $this->objectSerializer = $objectSerializer;
        $this->mealSchemaService = $mealSchemaService;
        $this->subscriptionService = $subscriptionService;
    }

    /**
     *
     *      Example of response with USER_PREMIUM Role:
     *      
     *      {
     *          breakfast: {
     *              selected": true,
     *              label": "breakfast",
     *              mealName": "Breakfast",
     *              mealTime": "06:00",
     *              required: true,
     *          },
     *          second_breakfast: {
     *              selected: true,
     *              label: "second_breakfast",
     *              mealName: "Second breakfast",
     *              mealTime: "09:00",
     *              required: false,
     *          },
     *          lunch: {
     *              selected: true,
     *              label: "lunch",
     *              mealName: "Lunch",
     *              mealTime: "12:00",
     *              required: true,
     *          },
     *          dinner: {
     *              selected: true,
     *              label: "dinner",
     *              mealName: "Dinner",
     *              mealTime: "15:00",
     *              required: true,
     *          },
     *          snack: {
     *              selected: false,
     *              label: "snack",
     *              mealName: "Snack",
     *              mealTime: "18:00",
     *              required: false,
     *          },
     *          supper: {
     *              selected: true,
     *              label: "supper",
     *              mealName: "Supper",
     *              mealTime: "21:00",
     *              required: false,
     *          }
     *      }
     *
     *      Example of response with no USER_PREMIUM Role:
     *      {
     *          breakfast: {
     *              selected": true,
     *              label": "breakfast",
     *              mealName": "Breakfast",
     *              mealTime": "",
     *              required: true,
     *          },
     *          second_breakfast: {
     *              selected: true,
     *              label: "second_breakfast",
     *              mealName: "Second breakfast",
     *              mealTime: "",
     *              required: true,
     *          },
     *      }
     *
     *
     *
     * @ApiDoc(
     *  resource=true,
     *  authentication=true,
     *  section="Settings",
     *  description="Get user's latest meal schema preferences",
     *  method="GET",
     *  uri="/api/diet-plan/{userId}/settings/preferences/meal-schema",
     *  tags={
     *      "stable" = "green",
     *      "tested" = "green"
     *  },
     *  requirements={
     *      {"name"="userId", "dataType"="int", "required"=true, "requirement"="\d+", "description"="User ID"}
     *  },
     *  filters={
     *      {"name"="weekDay", "dataType"="int", "required"=false, "format"="[1-7]", "description"="Number of week day from 1-Monday to 7-Sunday. Default is 1."}
     *  },
     *  statusCodes={
     *      200="Successful response",
     *      403="Access denied",
     *      400="Not a valid weekDay format",
     *      404={
     *              "User not found",
     *              "WeekDay not found"
     *          },
     *  }
     * )
     *
     * @Get("/api/diet-plan/{userId}/settings/preferences/meal-schema")
     *
     * @return JsonResponse
     * @throws AccessDeniedHttpException
     */
    public function getLatestDietMealSchemaPreferencesAction(): JsonResponse
    {
        $request = $this->requestStack->getCurrentRequest();
        $userId = (int)$request->get('userId');
        $weekDay = $request->get('weekDay', 1); // todo In a future we'll have all days configured

        if ($this->checkAccess($userId) === false) {
            throw new AccessDeniedHttpException('Access denied.');
        }

        if (!empty($weekDay) && !in_array($weekDay, range(1, 7))) {
            throw new BadRequestHttpException('Invalid weekDay format. [1-7] accepted.');
        }

        $hasRolePremium = $this->subscriptionService->isActive($userId);
        $settings = $this->mealSchemaService->getLatest($userId, $weekDay, $hasRolePremium);

        return new JsonResponse($settings, Response::HTTP_OK);
    }

    /**
     * Example of input data:
     *
     *      {
     *          mealSchema: [
     *              breakfast: {
     *                  selected: true,
     *                  mealName: "Buła na śniadanie",
     *                  mealTime: "10:20"
     *              },
     *              second_breakfast: {
     *                  selected: true,
     *                  mealName: "II śniadanie",
     *                  mealTime: "11:40"
     *              },
     *              lunch: {
     *                  selected: false
     *              },
     *              dinner: {
     *                  selected: true,
     *                  mealName: "Pizza",
     *                  mealTime: "17:00"
     *              },
     *              snack: {
     *                  selected: true,
     *                  mealName: "Przegrycha",
     *                  mealTime: "19:00"
     *              },
     *              supper: {
     *                  selected: true,
     *                  mealName: "Kolacyjka",
     *                  mealTime: "22:30"
     *              },
     *          ]
     *      }
     *
     * @ApiDoc(
     *  resource=true,
     *  authentication=true,
     *  section="Settings",
     *  description="Save user's meal schema preferences",
     *  method="POST",
     *  uri="/api/diet-plan/{userId}/settings/preferences/meal-schema",
     *  tags={
     *      "stable" = "green",
     *      "tested" = "green"
     *  },
     *  requirements={
     *      {"name"="userId", "dataType"="int", "required"=true, "requirement"="\d+", "description"="User ID"},
     *      {"name"="mealSchema[breakfast]", "dataType"="array", "required"=true, "description"="Array of selected meal schema (breakfast)."},
     *      {"name"="mealSchema[lunch]", "dataType"="array", "required"=true, "description"="Array of selected meal schema (lunch). "},
     *  },
     *  statusCodes={
     *      200="Successful response",
     *      403="Access denied",
     *      400="Invalid meal schema format",
     *  }
     * )
     *
     * @Post("/api/diet-plan/{userId}/settings/preferences/meal-schema")
     *
     * @return JsonResponse
     * @throws AccessDeniedHttpException
     * @throws BadRequestHttpException
     */
    public function postDietMealSchemaPreferencesAction(): JsonResponse
    {
        $request = $this->requestStack->getCurrentRequest();

        $userId = (int)$request->get('userId');

        if ($this->checkAccess($userId) === false) {
            throw new AccessDeniedHttpException('Access denied.');
        }

        $command = new PreferencesUserMealSchemaPostCommand($this->getUser());
        $command->mealSchema = $request->get('mealSchema') ?: json_decode($request->getContent());

        try {
            $response = $this->commandBus->handle($command);
        } catch (LogicException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        return new JsonResponse($response, Response::HTTP_OK);
    }
}
