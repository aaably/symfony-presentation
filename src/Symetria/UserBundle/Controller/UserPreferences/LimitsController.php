<?php

namespace Symetria\UserBundle\Controller\UserPreferences;

use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Patch;
use League\Tactician\CommandBus;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symetria\UserBundle\Controller\AbstractController;
use Symetria\UserBundle\Service\Preferences\LimitsService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class LimitsController extends AbstractController
{
    /**
     * @var CommandBus
     */
    private $commandBus;

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @var LimitsService
     */
    private $limitsService;

    /**
     * @param RequestStack  $requestStack
     * @param CommandBus    $commandBus
     * @param LimitsService $limits
     */
    public function __construct(
        RequestStack $requestStack,
        CommandBus $commandBus,
        LimitsService $limits
    ) {
        $this->requestStack = $requestStack;
        $this->commandBus = $commandBus;
        $this->limitsService = $limits;
    }

    /**
     *
     *      Example of response:
     *
     *      {
     *       "selected": {
     *           "timeId": 2,
     *           "typeIds": [
     *               "1"
     *           ]
     *       },
     *       "required": {
     *           "timeId": 1,
     *           "typeIds": [
     *               1
     *           ]
     *       },
     *       "limits": {
     *           "1": {
     *               "label": "do 10 min",
     *               "value": 10
     *           },
     *           "2": {
     *               "label": "do 30 min",
     *               "value": 30
     *           },
     *           "3": {
     *               "label": "bez limitu",
     *               "value": 9999
     *           }
     *       },
     *       "types": {
     *           "1": {
     *               "label": "Na zimno, bez gotowania",
     *               "inTimeLimit": [
     *                   1,
     *                   2,
     *                   3
     *               ]
     *           },
     *           "2": {
     *               "label": "Na zimno, wcześniej ugotowane",
     *               "inTimeLimit": [
     *                   1,
     *                   2,
     *                   3
     *               ]
     *           },
     *           "3": {
     *               "label": "Na ciepło",
     *               "inTimeLimit": [
     *                   1,
     *                   2,
     *                   3
     *               ]
     *           }
     *       },
     *       "options": {
     *           "1": [
     *               {
     *                   "label": "koktajle",
     *                   "typeId": 1,
     *                   "inLimit": [
     *                       1,
     *                       2,
     *                       3
     *                   ]
     *               },...
     *           ],
     *           "2": [
     *               {
     *                   "label": "jaja",
     *                   "typeId": 2,
     *                   "inLimit": [
     *                       1,
     *                       2,
     *                       3
     *                   ]
     *               }, ...
     *           ],
     *           "3": [
     *               {
     *                   "label": "omlety",
     *                   "typeId": 3,
     *                   "inLimit": [
     *                       1,
     *                       2,
     *                       3
     *                   ]
     *               }, ...
     *           ]
     *       }
     *   }
     *
     * @ApiDoc(
     *  resource=true,
     *  authentication=true,
     *  section="Settings",
     *  description="Get user's limits for diet generator",
     *  method="GET",
     *  uri="/api/diet-plan/{userId}/settings/preferences/limits",
     *  tags={
     *      "stable" = "green",
     *      "tested" = "green"
     *  },
     *  requirements={
     *      {"name"="userId", "dataType"="int", "required"=true, "requirement"="\d+", "description"="User ID"}
     *  },
     *  parameters={
     *      {"name"="mealName", "dataType"="string|null|array", "required"=false, "description"="Meal name (breakfast|second_breakfast|lunch|dinner|snack|supper)"}
     *  },
     *  statusCodes={
     *      200="Successful response",
     *      403="Access denied",
     *      404="User not found",
     *      422="Wrong meal name given",
     *
     *  }
     * )
     *
     * @Get("/api/diet-plan/{userId}/settings/preferences/limits")
     *
     * @return JsonResponse
     * @throws AccessDeniedHttpException
     */
    public function getLimitsAction(): JsonResponse
    {
        $request = $this->requestStack->getCurrentRequest();
        $userId = (int)$request->get('userId');
        $mealName = $request->get('mealName');

        if ($this->checkAccess($userId) === false) {
            throw new AccessDeniedHttpException();
        }

        try {
            $results = $this->limitsService->getLimitsForUser($userId, $mealName);
        } catch (\Exception $e) {
            return new JsonResponse(
                ['message' => $e->getMessage()],
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        return new JsonResponse($results, Response::HTTP_OK);
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  authentication=true,
     *  section="Settings",
     *  description="Get user's limits for diet generator",
     *  method="PATCH",
     *  uri="/api/diet-plan/{userId}/settings/preferences/limits",
     *  tags={
     *      "stable" = "green",
     *      "tested" = "green"
     *  },
     *  requirements={
     *      {"name"="userId", "dataType"="int", "required"=true, "requirement"="\d+", "description"="User ID"},
     *      {"name"="payload", "dataType"="array", "required"=true, "requirement"="\d+", "description"="Meal limitations array"},
     *  },
     *  statusCodes={
     *      200="Successful response",
     *      403="Access denied",
     *      404="User not found",
     *      422="Wrong meal name given",
     *
     *  }
     * )
     *
     * @Patch("/api/diet-plan/{userId}/settings/preferences/limits")
     *
     * @return JsonResponse
     * @throws AccessDeniedHttpException
     */
    public function patchLimitsAction(): JsonResponse
    {
        $request = $this->requestStack->getCurrentRequest();
        $userId = (int)$request->get('userId');
        $payload = (array)$request->get('payload');

        if ($this->checkAccess($userId) === false) {
            throw new AccessDeniedHttpException();
        }
        try {
            $limits = $this->limitsService->setUserLimits($userId, $payload);
        } catch (\Exception $e) {
            return new JsonResponse(
                ['message' => $e->getMessage()],
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        return new JsonResponse($limits, Response::HTTP_OK);
    }
}
