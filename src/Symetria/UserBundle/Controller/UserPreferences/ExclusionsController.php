<?php

namespace Symetria\UserBundle\Controller\UserPreferences;

use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Put;
use League\Tactician\CommandBus;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symetria\UserBundle\Controller\AbstractController;
use Symetria\UserBundle\Service\Preferences\ExcludedProductsService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class ExclusionsController extends AbstractController
{
    /**
     * @var CommandBus
     */
    private $commandBus;

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @var ExcludedProductsService
     */
    private $excludedProductsService;

    /**
     * @param RequestStack            $requestStack
     * @param CommandBus              $commandBus
     * @param ExcludedProductsService $excludedProductsService
     */
    public function __construct(
        RequestStack $requestStack,
        CommandBus $commandBus,
        ExcludedProductsService $excludedProductsService
    ) {
        $this->requestStack = $requestStack;
        $this->commandBus = $commandBus;
        $this->excludedProductsService = $excludedProductsService;
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  authentication=true,
     *  section="Settings",
     *  description="Get user's excluded products",
     *  method="GET",
     *  uri="/api/diet-plan/{userId}/settings/preferences/exclusions",
     *  tags={
     *      "stable" = "green",
     *      "tested" = "green"
     *  },
     *  requirements={
     *      {"name"="userId", "dataType"="int", "required"=true, "requirement"="\d+", "description"="User ID"}
     *  },
     *  statusCodes={
     *      200="Successful response",
     *      403="Access denied",
     *      404="User not found",
     *  }
     * )
     *
     * @Get("/api/diet-plan/{userId}/settings/preferences/exclusions")
     *
     * @return JsonResponse
     * @throws AccessDeniedHttpException
     */
    public function getExcludedProductsAction(): JsonResponse
    {
        $request = $this->requestStack->getCurrentRequest();
        $userId = (int)$request->get('userId');

        if ($this->checkAccess($userId) === false) {
            throw new AccessDeniedHttpException();
        }

        $exclusions = $this->excludedProductsService->getExcludedProductsWithNamesForUser($userId);

        return new JsonResponse($exclusions, Response::HTTP_OK);
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  authentication=true,
     *  section="Settings",
     *  description="Exclude products from diet generator",
     *  method="PUT",
     *  uri="/api/diet-plan/{userId}/settings/preferences/exclusions",
     *  tags={
     *      "stable" = "green",
     *      "tested" = "green"
     *  },
     *  requirements={
     *      {"name"="userId", "dataType"="int", "required"=true, "requirement"="\d+", "description"="User ID"},
     *      {"name"="ids", "dataType"="array", "required"=true, "requirement"="\d+", "description"="Excluded products ids"}
     *  },
     *  statusCodes={
     *      200="Successful response",
     *      403="Access denied",
     *      404="User not found",
     *
     *     422="Too many exclusions (10 max)"
     *  }
     * )
     *
     * @Put("/api/diet-plan/{userId}/settings/preferences/exclusions")
     *
     * @return JsonResponse
     * @throws AccessDeniedHttpException
     */
    public function putExcludedProductsAction(): JsonResponse
    {
        $request = $this->requestStack->getCurrentRequest();
        $userId = (int)$request->get('userId');
        $ids = (array)$request->get('ids');

        if ($this->checkAccess($userId) === false) {
            throw new AccessDeniedHttpException();
        }
        
        try {
            $setting = $this->excludedProductsService->setExcludedForUser($userId, $ids);
        } catch(\Exception $e) {
            return new JsonResponse(
                ['message' => $e->getMessage()],
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        return new JsonResponse($setting->getExcludedProducts(), Response::HTTP_OK);
    }
}
