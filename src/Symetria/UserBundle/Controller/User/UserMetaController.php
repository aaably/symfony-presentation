<?php

namespace Symetria\UserBundle\Controller\User;

use Fitatu\DatabaseBundle\Entity\Auth\User\User;
use Fitatu\DatabaseBundle\Entity\Auth\User\UserMeta;
use Fitatu\Local\Repository\Auth\User\UserMetaRepository;
use FOS\RestBundle\Controller\Annotations\Patch;
use League\Tactician\CommandBus;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symetria\UserBundle\Controller\AbstractController;
use Symetria\UserBundle\Handler\User\Get\UserGetCommand;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @author Lukasz Domanski
 * @copyright Fitatu Sp. z o.o.
 */
class UserMetaController extends AbstractController
{
    const STORE_RATE = 'storeRate';
    const GOAL_ACHIEVEMENT = 'goalAchievement';

    private $availableFields = [self::STORE_RATE, self::GOAL_ACHIEVEMENT];

    /**
     * @var CommandBus
     */
    private $commandBus;

    /**
     * @var UserMetaRepository
     */
    private $userMetaRepository;

    /**
     * @param CommandBus         $commandBus
     * @param UserMetaRepository $userMetaRepository
     */
    public function __construct(CommandBus $commandBus, UserMetaRepository $userMetaRepository)
    {
        $this->commandBus = $commandBus;
        $this->userMetaRepository = $userMetaRepository;
    }

    /**
     * @ApiDoc(
     *     resource=true,
     *     section="Users",
     *     authentication=true,
     *     description="Update some user data",
     *     tags={
     *          "stable"="green",
     *          "tested"="green"
     *     },
     *     requirements={
     *          {"name"="userId", "dataType"="int", "requirement"="\d+", "description"="User ID"}
     *     },
     *     parameters={
     *          {"name"="storeRate", "dataType"="json", "required"=false, "description"="Store rate with status and updated_date ex. {'status':'NEVER', 'updatedDate':'2016-12-14'}"},
     *     },
     *     statusCodes={
     *          200="Returned when successful",
     *          400="Data validation error. Details in validation field of JSON response.",
     *          401="Unauthorized - invalid or no token",
     *          403="Access denied",
     *          404="User with this ID could not be found"
     *     }
     * )
     *
     * @Patch("/users/{userId}/meta")
     *
     * @param int $userId
     *
     * @return JsonResponse
     *
     * @throws AccessDeniedHttpException
     * @throws NotFoundHttpException
     */
    public function patchUserMetaAction($userId)
    {
        if (!$this->checkAccess($userId)) {
            throw new AccessDeniedHttpException('Access denied.');
        }

        $user = $this->getUserById($userId);

        if (!$user instanceof User) {
            throw new NotFoundHttpException(
                sprintf('User #%d not found', $userId)
            );
        }

        /** @var Request $request */
        $request = $this->get('request_stack')->getMasterRequest();

        foreach ($this->availableFields as $parameter) {
            $requestValue = $request->get($parameter);

            if (!is_null($requestValue)) {
                $this->userMetaRepository->updateMeta($user, $parameter, $requestValue);
            }
        }

        return new JsonResponse(
            $this->getMeta($user),
            Response::HTTP_OK
        );
    }

    /**
     * @param int $userId
     *
     * @return null|User
     */
    private function getUserById($userId)
    {
        if ($this->isGranted('ROLE_USER') && $this->getUser()->getId() == $userId) {
            return $this->getUser();
        }

        return $this->commandBus->handle(
            new UserGetCommand($userId)
        );
    }

    /**
     * @param User $user
     * @return \ArrayObject
     */
    private function getMeta(User $user): \ArrayObject
    {
        $metaData = [];

        /** @var UserMeta $meta */
        foreach ($user->getMeta() as $meta) {
            $metaData[$meta->getField()] = $meta->getValue();
        }

        return new \ArrayObject($metaData);
    }
}
