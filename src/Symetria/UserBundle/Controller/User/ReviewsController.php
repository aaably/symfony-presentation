<?php

namespace Symetria\UserBundle\Controller\User;

use Doctrine\ORM\EntityNotFoundException;
use Fitatu\DatabaseBundle\Entity\Auth\Review;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Patch;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symetria\UserBundle\Controller\AbstractController;
use Symetria\UserBundle\Exception\PremiumServices\ReviewNotFoundException;
use Symetria\UserBundle\Service\Preferences\ReviewsService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class ReviewsController extends AbstractController
{
    /**
     * @var ReviewsService
     */
    private $reviewsService;
    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @param ReviewsService $reviewsService
     * @param RequestStack   $requestStack
     */
    public function __construct(ReviewsService $reviewsService, RequestStack $requestStack)
    {
        $this->reviewsService = $reviewsService;
        $this->requestStack = $requestStack;
    }

    /**
     * Display reviews list
     *
     *      {
     *          "locales": {
     *              "nl_NL": 12,
     *              "fr_FR": 11
     *          },
     *          "count": 54,
     *          "reviews": [
     *              {
     *                  "id": "87a93675-9f6c-11e7-8eb8-0242ac120005",
     *                  "nickname": null,
     *                  "rate": null,
     *                  "locale": "pl_PL",
     *                  "content": null,
     *                  "updatedAt": "2017-09-22 08:03:33",
     *                  "createdAt": "2017-09-22 08:03:33",
     *                  "userId": "1",
     *                  "isVisible": true
     *              },
     *          ],
     *          "rating": {
     *              "1": 12,
     *              "2": 4,
     *              "3": 9,
     *              "4": 11,
     *              "5": 17
     *          }
     *      }
     *
     * @ApiDoc(
     *     resource=true,
     *     section="Reviews",
     *     authentication=true,
     *     description="Query reviews for given resource",
     *     uri="/reviews/{reviewedItem}",
     *     tags={
     *          "stable" = "green",
     *          "tested" = "green"
     *     },
     *     requirements={
     *          {"name"="reviewedItem", "dataType"="string", "description"="Item name diet|product|recipe or single uuid"},
     *     },
     *  parameters={
     *      {"name"="page", "dataType"="int", "required"=false, "description"="Page number"},
     *      {"name"="limit", "dataType"="int", "required"=false, "description"="Reviews per page"},
     *      {"name"="locale", "dataType"="string", "required"=false, "description"="Localization ex. pl_PL, en_GB"},
     *      {"name"="isVisible", "dataType"="boolean", "required"=false, "description"="Visibility content"},
     *  },
     *     statusCodes={
     *          200="Ok",
     *          401="Unauthorized - invalid or no token",
     *          403="Access denied",
     *          404="Review not found",
     *     }
     * )
     *
     * @Get("/reviews/{reviewedItem}")
     *
     * @param string $reviewedItem
     * @return JsonResponse
     * @throws EntityNotFoundException
     */
    public function getReviewsAction(string $reviewedItem)
    {
        /** @var Request $request */
        $request = $this->requestStack->getCurrentRequest();
        $page = (int)$request->get('page') ?: 1;
        $limit = (int)$request->get('limit') ?: 20;
        $locale = $request->get('locale');
        $isVisible = $request->get('isVisible');

        if ($this->reviewsService->isUuid($reviewedItem)) {
            /** @var Review $review */
            $review = $this->reviewsService->get($reviewedItem);

            if (!$review instanceof Review) {
                throw new ReviewNotFoundException($reviewedItem);
            }

            return new JsonResponse(
                $review->toArray(),
                Response::HTTP_OK
            );
        }

        $visibilityMode = !$this->checkModeratorAccess() ? true : (is_null($isVisible) ? null : $isVisible === 'true');

        $reviews = $this->reviewsService
            ->paginate($reviewedItem, $locale, $page, $limit, $visibilityMode);

        return new JsonResponse(
            $reviews,
            Response::HTTP_OK
        );
    }

    /**
     * Check moderator access to hidden content
     *
     * @return bool
     */
    private function checkModeratorAccess(): bool
    {
        return $this->isGranted('ROLE_ADMIN');
    }

    /**
     * @ApiDoc(
     *     resource=true,
     *     section="Reviews",
     *     authentication=true,
     *     description="Create new resource",
     *     uri="/reviews/{reviewedItem}",
     *     tags={
     *          "stable" = "green",
     *          "tested" = "green"
     *     },
     *     requirements={
     *          {"name"="reviewedItem", "dataType"="string", "description"="Item name diet|product|recipe"},
     *          {"name"="locale", "dataType"="string", "description"="Locale ex. pl_PL"},
     *     },
     *     parameters={
     *          {"name"="rate", "dataType"="integer", "required"=false, "description"="Rating for given resource"},
     *          {"name"="content", "dataType"="string", "required"=false, "description"="Review message"},
     *     },
     *     statusCodes={
     *          201="Created",
     *          401="Unauthorized - invalid or no token",
     *          403="Access denied",
     *          422="Missing review data",
     *     }
     * )
     *
     * @Post("/reviews/{reviewedItem}")
     *
     * @param string $reviewedItem
     * @return JsonResponse
     */
    public function postReviewAction(string $reviewedItem)
    {
        if (!$this->reviewsService->isAllowedToReview($this->getUser())) {
            throw new AccessDeniedHttpException('You are not allowed to review.');
        }

        /** @var Request $request */
        $request = $this->requestStack->getCurrentRequest();
        $attributes = [];
        $fields = $this->reviewsService->getEditableFields();

        foreach ($fields as $field) {
            $attributes[$field] = $request->get($field);
        }
        $attributes['type'] = $reviewedItem;

        try {
            $review = $this->reviewsService->create(
                $this->getUser(),
                $attributes
            );
        } catch (\Exception $e) {
            return new JsonResponse(
                ['message' => $e->getMessage()],
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        return new JsonResponse(
            $review->toArray(),
            Response::HTTP_CREATED
        );
    }

    /**
     *      {
     *          "id": "87a93675-9f6c-11e7-8eb8-0242ac120005",
     *          "nickname": null,
     *          "rate": null,
     *          "locale": "pl_PL",
     *          "content": null,
     *          "updatedAt": "2017-09-22 08:03:33",
     *          "createdAt": "2017-09-22 08:03:33",
     *          "userId": "1",
     *          "isVisible": true
     *      }
     *
     * @ApiDoc(
     *     resource=true,
     *     section="Reviews",
     *     authentication=true,
     *     description="Update single review",
     *     uri="/reviews/{uuid}",
     *     tags={
     *          "stable" = "green",
     *          "tested" = "green"
     *     },
     *     requirements={
     *          {"name"="uuid", "dataType"="string", "description"="Review identification as uuid"},
     *     },
     *     parameters={
     *          {"name"="locale", "dataType"="string", "required"=false, "description"="Locale ex. pl_PL"},
     *          {"name"="rate", "dataType"="integer", "required"=false, "description"="Rating for given resource"},
     *          {"name"="content", "dataType"="string", "required"=false, "description"="Review message"},
     *          {"name"="type", "dataType"="string", "required"=false, "description"="Review type"},
     *          {"name"="isVisible", "dataType"="boolean", "required"=false, "description"="Review visibility"},
     *     },
     *     statusCodes={
     *          200="Ok",
     *          401="Unauthorized - invalid or no token",
     *          403="Access denied",
     *          404="Review not found",
     *     }
     * )
     *
     * @Put("/reviews/{uuid}")
     *
     * @param string $uuid
     * @return JsonResponse
     * @throws EntityNotFoundException
     */
    public function putReviewAction(string $uuid)
    {
        /** @var Request $request */
        $request = $this->requestStack->getCurrentRequest();
        /** @var Review $review */
        $review = $this->reviewsService->get($uuid);

        if ($this->checkAccess($review->getUser()->getId()) === false) {
            throw new AccessDeniedHttpException('Access denied.');
        }

        if (!$review instanceof Review) {
            throw new ReviewNotFoundException($uuid);
        }

        $attributes = [];
        $fields = $this->reviewsService->getEditableFields(
            $this->isGranted('ROLE_ADMIN')
        );

        foreach ($fields as $field) {
            $attributes[$field] = $request->get($field);
        }

        try {
            $review = $this->reviewsService->updateReview(
                $review, $attributes
            );
        } catch (\Exception $e) {
            return new JsonResponse(
                ['message' => $e->getMessage()],
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        return new JsonResponse(
            $review->toArray()
        );
    }

    /**
     * Updating existing review
     *
     *      {
     *          "id": "87a93675-9f6c-11e7-8eb8-0242ac120005",
     *          "nickname": null,
     *          "rate": null,
     *          "locale": "pl_PL",
     *          "content": null,
     *          "updatedAt": "2017-09-22 08:03:33",
     *          "createdAt": "2017-09-22 08:03:33",
     *          "userId": "1",
     *          "isVisible": true
     *      }
     *
     * @ApiDoc(
     *     resource=true,
     *     section="Reviews",
     *     authentication=true,
     *     description="Update single review",
     *     uri="/reviews/{uuid}",
     *     tags={
     *          "stable" = "green",
     *          "tested" = "green"
     *     },
     *     requirements={
     *          {"name"="uuid", "dataType"="string", "description"="Review identification as uuid"},
     *     },
     *     parameters={
     *          {"name"="locale", "dataType"="string", "required"=false, "description"="Locale ex. pl_PL"},
     *          {"name"="rate", "dataType"="integer", "required"=false, "description"="Rating for given resource"},
     *          {"name"="content", "dataType"="string", "required"=false, "description"="Review message"},
     *          {"name"="type", "dataType"="string", "required"=false, "description"="Review type"},
     *          {"name"="isVisible", "dataType"="boolean", "required"=false, "description"="Review visibility"},
     *     },
     *     statusCodes={
     *          200="Ok",
     *          401="Unauthorized - invalid or no token",
     *          403="Access denied",
     *          404="Review not found",
     *          422="Unprocessable Entity"
     *     }
     * )
     *
     * @Patch("/reviews/{uuid}")
     *
     * @param string $uuid
     * @return JsonResponse
     * @throws EntityNotFoundException
     */
    public function patchReviewAction(string $uuid)
    {
        /** @var Request $request */
        $request = $this->requestStack->getCurrentRequest();
        /** @var Review $review */
        $review = $this->reviewsService->get($uuid);

        if ($this->checkAccess($review->getUser()->getId()) === false) {
            throw new AccessDeniedHttpException('Access denied.');
        }

        if (!$review instanceof Review) {
            throw new ReviewNotFoundException($uuid);
        }
        $body = json_decode($request->getContent(), true);
        if (!is_array($body)) {
            throw new UnprocessableEntityHttpException(Response::$statusTexts[Response::HTTP_UNPROCESSABLE_ENTITY]);
        }

        $attributes = [];
        $fields = $this->reviewsService->getEditableFields(
            $this->isGranted('ROLE_ADMIN')
        );
        foreach ($body as $key => $value) {
            if (in_array($key, $fields)) {
                $attributes[$key] = $body[$key];
            }
        }

        try {
            $review = $this->reviewsService->updateReview(
                $review, $attributes
            );
        } catch (\Exception $e) {
            return new JsonResponse(
                ['message' => $e->getMessage()],
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        return new JsonResponse(
            $review->toArray()
        );
    }


}
