<?php

namespace Symetria\UserBundle\Controller\User;

use FOS\RestBundle\Controller\Annotations\Delete;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symetria\ApiClientBundle\ApiClient\CacheManager;
use Symetria\UserBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @author Lukasz Domanski
 * @copyright Fitatu Sp. z o.o.
 */
class UserCacheController extends AbstractController
{
    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @var CacheManager
     */
    private $cacheManger;

    /**
     * @param RequestStack $requestStack
     * @param CacheManager $cacheManger
     */
    public function __construct(RequestStack $requestStack, CacheManager $cacheManger)
    {
        $this->requestStack = $requestStack;
        $this->cacheManger = $cacheManger;
    }


    /**
     * @ApiDoc(
     *  resource=true,
     *  section="Users",
     *  authentication=false,
     *  description="Remove user cache",
     *  tags={
     *      "stable"="green",
     *      "tested"="green",
     *      "internal"="brown"
     *  },
     *  requirements={
     *      {"name"="userId", "dataType"="int", "required"=true, "requirement"="\d+", "description"="User ID"}
     *  },
     *  parameters={
     *      {"name"="intranetToken", "dataType"="string", "required"=true, "description"="Secret intranet API token"},
     *      {"name"="key", "dataType"="string|array", "required"=true, "description"="Redis key/s"},
     *  },
     *  statusCodes={
     *      200="Successful removed user cache",
     *      400="No secret token",
     *      403="Invalid secret token",
     *      404="Redis key not found",
     *  }
     * )
     *
     * @Delete("/users/{userId}/cache")
     *
     * @param int $userId
     *
     * @return JsonResponse
     */
    public function deleteUserCacheAction(int $userId): JsonResponse
    {
        $request = $this->requestStack->getCurrentRequest();
        $intranetToken = $request->get('intranetToken');
        $key = $request->get('key');

        if (empty($intranetToken)) {
            throw new BadRequestHttpException('Intranet token not found');
        }

        if ($intranetToken != $this->getParameter('secret_intranet_api_token')) {
            throw new AccessDeniedHttpException('Invalid secret intranet token');
        }

        if (empty($key)) {
            throw new NotFoundHttpException('Redis key not found');
        }

        if (!is_array($key)) {
            $key = [$key];
        }

        $this->cacheManger->deleteByKeys($key);

        return new JsonResponse(
            ['deleted' => true, 'userId' => $userId],
            Response::HTTP_OK
        );
    }
}