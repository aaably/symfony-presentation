<?php

namespace Symetria\UserBundle\Controller\User;

use DateTime;
use Fitatu\BillingBundle\Service\SubscriptionService;
use Fitatu\DatabaseBundle\Entity\Auth\Settings\SettingsDiet;
use Fitatu\DatabaseBundle\Entity\Auth\User\User;
use Fitatu\Local\Repository\Auth\Settings\SettingsUserRepository;
use Fitatu\Local\Repository\Auth\User\UserMetaRepository;
use Fitatu\Local\Repository\Auth\User\UserRepository;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Patch;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Model\UserManagerInterface;
use FOS\UserBundle\Util\TokenGeneratorInterface;
use League\Tactician\CommandBus;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTManagerInterface;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symetria\ApiServerBundle\Exception\InternalServerErrorHttpException;
use Symetria\ApiServerBundle\Util\Serializer\ObjectSerializer;
use Symetria\SecurityBundle\Exception\EmailSendingException;
use Symetria\SharedBundle\Model\UnitType;
use Symetria\UserBundle\Controller\AbstractController;
use Symetria\UserBundle\Event\User\UserRequestedActivationMailEvent;
use Symetria\UserBundle\Exception\EmailChange\AlreadyConfirmedEmailChangeException;
use Symetria\UserBundle\Exception\EmailChange\EmailIsTakenEmailChangeException;
use Symetria\UserBundle\Exception\EmailChange\EmailNotChangedEmailChangeException;
use Symetria\UserBundle\Exception\PasswordChange\InvalidCurrentPasswordException;
use Symetria\UserBundle\Exception\PremiumServices\UserNotFoundException;
use Symetria\UserBundle\Handler\Settings\Diet\Update\SettingsDietUpdateCommand;
use Symetria\UserBundle\Handler\Settings\User\Update\SettingsUserUpdateCommand;
use Symetria\UserBundle\Handler\User\Create\CreateUserCommand;
use Symetria\UserBundle\Handler\User\Get\UserGetCommand;
use Symetria\UserBundle\Handler\User\Update\ChangePassword\UserChangePasswordCommand;
use Symetria\UserBundle\Handler\User\Update\ConfirmEmailChange\ConfirmEmailChangeCommand;
use Symetria\UserBundle\Handler\User\Update\UserUpdateCommand;
use Symetria\UserBundle\Service\DietSettingsService;
use Symetria\UserBundle\Service\UserService;
use Symetria\UserBundle\Service\UserSettingsService;
use Symetria\UserBundle\UserEvents;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

/**
 * @author    Nikodem Osmialowski
 * @copyright Fitatu Sp. z o.o.
 */
class UserController extends AbstractController
{
    /**
     * @var SettingsUserRepository
     */
    private $settingsUserRepository;

    /**
     * @var CommandBus
     */
    private $commandBus;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var UserMetaRepository
     */
    private $userMetaRepository;

    /**
     * @var ObjectSerializer
     */
    private $objectSerializer;

    /**
     * @var UserManagerInterface
     */
    private $userManager;

    /**
     * @var JWTManagerInterface
     */
    private $jwtManager;

    /**
     * @var UserService
     */
    private $userService;

    /**
     * @var UserSettingsService
     */
    private $userSettingsService;

    /**
     * @var DietSettingsService
     */
    private $dietSettingsService;

    /**
     * @var SubscriptionService
     */
    private $subscriptionService;

    /**
     * @param UserRepository $userRepository
     * @param SettingsUserRepository $settingsUserRepository
     * @param CommandBus $commandBus
     * @param ObjectSerializer $objectSerializer
     * @param UserManagerInterface $userManager
     * @param JWTManagerInterface $jwtManager
     * @param UserService $userService
     * @param UserSettingsService $userSettingsService
     * @param DietSettingsService $dietSettingsService
     * @param SubscriptionService $subscriptionService
     * @param UserMetaRepository $userMetaRepository
     */
    public function __construct(
        UserRepository $userRepository,
        SettingsUserRepository $settingsUserRepository,
        CommandBus $commandBus,
        ObjectSerializer $objectSerializer,
        UserManagerInterface $userManager,
        JWTManagerInterface $jwtManager,
        UserService $userService,
        UserSettingsService $userSettingsService,
        DietSettingsService $dietSettingsService,
        SubscriptionService $subscriptionService,
        UserMetaRepository $userMetaRepository
    ) {
        $this->userRepository = $userRepository;
        $this->settingsUserRepository = $settingsUserRepository;
        $this->commandBus = $commandBus;
        $this->objectSerializer = $objectSerializer;
        $this->userManager = $userManager;
        $this->jwtManager = $jwtManager;
        $this->userService = $userService;
        $this->userSettingsService = $userSettingsService;
        $this->dietSettingsService = $dietSettingsService;
        $this->subscriptionService = $subscriptionService;
        $this->userMetaRepository = $userMetaRepository;
    }

    /**
     *
     *      Example of response:
     *
     *      {
     *          "id": "989628",
     *          "username": "rck",
     *          "nickname": "adam.kowalski",
     *          "email": "lukasz.domanski81@gmail.com",
     *          "roles": [
     *              "ROLE_SUPER_ADMIN",
     *              "ROLE_LOCALE_PL_PL",
     *              "ROLE_PREMIUM",
     *              "ROLE_USER",
     *              "ROLE_LOCALE_FR_FR"
     *          ],
     *          "accessControl": [
     *              "ROLE_SUPER_ADMIN",
     *              "ROLE_LOCALE_PL_PL",
     *              "ROLE_PREMIUM",
     *              "ROLE_USER",
     *              "ROLE_LOCALE_FR_FR",
     *              "ROLE_ADMIN_MANAGER",
     *              "ROLE_DIETICIAN",
     *              "ROLE_ADMIN",
     *              "ROLE_TRAINER",
     *              "ROLE_PSYCHOLOGIST",
     *              "ROLE_ALLOWED_TO_SWITCH",
     *              "ROLE_FEATURE_ALERT"
     *          ],
     *          "sex": 2,
     *          "enabled": true,
     *          "createdAt": "2016-05-17T11:46:59+00:00",
     *          "hasDietSettings": true,
     *          "hasUserSettings": true,
     *          "hasActivityEnergyInclusionMode": true,
     *          "demo": false,
     *          "locale": "fr_FR",
     *          "searchLocale": "pl_PL",
     *          "meta": {},
     *          "showGoalAchievement": false,
     *          "hasActivityTraining": true,
     *          "appConfig": {
     *              "searchDeviationRatePercentage": 0.5,
     *              "caloriesLimitDietGeneration": 3000
     *          },
     *          "facebookId": null,
     *          "hasPassword": true,
     *          "requestedEmailChange": null,
     *          "isWeightMeasurementRequired": false,
     *          "weightUnit": "KG",
     *          "sizeUnit": "CM",
     *          "systemInfo": "ANDROID",
     *          "systemVersion": "7.1.2",
     *          "appVersion": "2.4.16",
     *          "dietGeneration": {
     *              "timetable": {
     *                  "next": {
     *                      "from": "2017-07-10",
     *                      "to": "2017-07-16",
     *                      "nextAt": "2017-07-07 10:00"
     *                  },
     *                  "current": {
     *                      "id": 20170706050700,
     *                      "from": "2017-07-06",
     *                      "to": "2017-07-10"
     *                  }
     *              }
     *          },
     *      }
     *
     * @ApiDoc(
     *  resource=true,
     *  section="Users",
     *  authentication=true,
     *  description="Retrieve profile data of user by ID (getUserInfo)",
     *  tags={
     *      "stable"="green",
     *      "tested"="green"
     *  },
     *  requirements={
     *      {"name"="userId", "dataType"="int", "requirement"="\d+", "description"="User ID"}
     *  },
     *  statusCodes={
     *      200="Successful user data response",
     *      401="Unauthorized - invalid or no token",
     *      403="Not authorized to retrieve user data of this user",
     *      404={
     *          "Returned when the user is not found",
     *          "Returned when required related user data is not found"
     *      }
     *  }
     * )
     *
     * @Get("/users/{userId}")
     *
     * @param int $userId
     *
     * @return JsonResponse
     */
    public function getUserAction(int $userId): JsonResponse
    {
        $userId = (int)$userId;

        // Retrieve user by $userId
        $user = $this->getUserById($userId);

        if (!$this->hasRightOrIsOwner($userId)) {
            throw new AccessDeniedHttpException('Access denied.');
        }

        return new JsonResponse(
            $this->userService->getUserInfoCached(
                $user,
                $userId,
                $this->getUser()->getId(),
                $this->isGranted('ROLE_SUPER_ADMIN')
            ),
            Response::HTTP_OK
        );
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  section="Users",
     *  authentication=true,
     *  description="Register/create new user. Operation sends email for ",
     *  tags={
     *      "stable"="green",
     *      "tested"="green"
     *  },
     *  parameters={
     *      {"name"="email", "dataType"="string", "required"=true, "description"="E-mail address"},
     *      {"name"="password", "dataType"="string", "required"=true, "description"="Password"},
     *      {"name"="acceptTerms", "dataType"="boolean", "required"=true, "description"="User accepted terms"},
     *      {"name"="marketingAccepted", "dataType"="boolean", "required"=true, "description"="User accepted marketing terms"},
     *      {"name"="partnerId", "dataType"="int", "required"=false, "description"="Partner ID: 1 - Fitatu"},
     *      {"name"="locale", "dataType"="string", "required"=true, "description"="Localization ex. pl_PL, en_GB"}
     *  },
     *  statusCodes={
     *      201="Created - user created",
     *      400={
     *          "Bad Request - validation failed",
     *          "The email is not a valid email",
     *          "Please provide e-mail address",
     *          "Please provide password",
     *          "Your password must be at least 5 characters long",
     *          "Your password cannot be longer than 32 characters",
     *          "You have to accept the terms and conditions",
     *          "PartnerId is not an integer",
     *          "The value must be greater than 0"
     *      },
     *      409="Conflict - address e-mail already exists",
     *  }
     * )
     *
     * @return JsonResponse
     */
    public function postUserAction()
    {
        /** @var Request $request */
        $request = $this->get('request_stack')->getMasterRequest(); // TODO Dependency injection

        if ($this->userRepository->findByUsernameOrEmail($request->get('email'))) {
            throw new ConflictHttpException('User already exists');
        }

        $termsAccepted = $request->get('acceptTerms');
        $marketingAccepted = $request->get('marketingAccepted');

        $command = new CreateUserCommand(
            $request->get('email'),
            $request->get('password')
        );
        $command->termsAccepted = in_array($termsAccepted, ['false', '0'], true) ? false : (bool)$termsAccepted;
        $command->marketingAccepted = in_array(
            $marketingAccepted,
            ['false', '0'],
            true
        ) ? false : (bool)$marketingAccepted;

        if (!empty($request->get('partnerId'))) {
            // FIXME We haven't decided how to recognize a partner yet
            $command->partnerId = (int)$request->get('partnerId');
        }

        $command->locale = $request->get('locale');

        /** @var User $user */
        $user = $this->commandBus->handle($command);

        $data = [
            'id'       => (int)$user->getId(),
            'username' => $user->getUsername(),
            'email'    => $user->getEmail(),
            'roles'    => $user->getRoles(),
            'locale'   => $user->getLocale()
        ];

        return new JsonResponse(
            $data,
            Response::HTTP_CREATED
        );
    }


    /**
     * @ApiDoc(
     *  resource=true,
     *  section="Users",
     *  authentication=true,
     *  description="Delete user (if not activated) or send a link to confirm account deletion",
     *  tags={
     *      "stable"="green",
     *      "tested"="green"
     *  },
     *  statusCodes={
     *      202="Confirmation email was sent",
     *      204="User deleted",
     *      400={
     *          "Bad Request - validation failed"
     *      },
     *      401="Unauthorized user",
     *      403="Access denied",
     *  }
     * )
     *
     * @Delete("/users/{userId}")
     *
     * @param int         $userId
     * @return JsonResponse
     */
    public function deleteUserAction(int $userId): JsonResponse
    {
        if (!$this->hasRightOrIsOwner($userId)) {
            throw new AccessDeniedHttpException('Access denied.');
        }

        try {
            $user = $this->userService->getUser($userId);
        } catch (UserNotFoundException $e) {
            throw new NotFoundHttpException($e->getMessage());
        }

        if (!$user->isEnabled()) {
            $this->userService->deleteUser($userId);

            return new JsonResponse(
                'User deleted',
                Response::HTTP_NO_CONTENT
            );
        } else {
            $this->userService->sendUserDeletionEmail($userId);

            return new JsonResponse(
                [
                    'success' => true,
                    'token'   => 'Confirmation email was sent',
                ],
                Response::HTTP_ACCEPTED
            );
        }
    }



    /**
     * Deletes an user account.
     *
     * @ApiDoc(
     *  resource=true,
     *  section="Users",
     *  authentication=false,
     *  description="Delete user by confirmation token generated by endpoint: DELETE /users/{userId}",
     *  tags={
     *      "stable"="green",
     *      "tested"="green"
     *  },
     *  requirements={
     *      {"name"="userId", "dataType"="int", "description"="User ID"},
     *      {"name"="token", "dataType"="string", "description"="Confirmation token"}
     *  },
     *  statusCodes={
     *      204="User deleted",
     *      400="Invalid confirmationToken",
     *      404="User not found",
     *  }
     * )
     *
     * @Delete("/users/{userId}/action/delete/{token}")
     *
     * @param int    $userId
     * @param string $token
     *
     * @return JsonResponse
     */
    public function deleteActivateUserAction($userId, $token): JsonResponse
    {
        try {
            $this->userService->deleteUserByConfirmationToken($userId, $token);
        } catch (\InvalidArgumentException $e) {
            throw new BadRequestHttpException('Invalid confirmationToken');
        }

        return new JsonResponse(
            null,
            Response::HTTP_NO_CONTENT
        );
    }

    /**
     * Activates an user account after registration. It also generates a new JSON Web Token and attaches it
     * before outputting the response.
     *
     * @ApiDoc(
     *  resource=true,
     *  section="Users",
     *  authentication=false,
     *  description="Activate user after registration by confirmation token",
     *  tags={
     *      "stable"="green",
     *      "tested"="green"
     *  },
     *  requirements={
     *      {"name"="userId", "dataType"="int", "description"="User ID"},
     *      {"name"="token", "dataType"="string", "description"="Confirmation token"}
     *  },
     *  statusCodes={
     *      200="User activated",
     *      404="User not found",
     *      409={
     *          "User was already activated",
     *          "Conflict - e-mail address already exists"
     *      },
     *      422="This e-mail address is taken"
     *  }
     * )
     *
     * @Put("/users/{userId}/action/activate/{token}")
     *
     * @param int    $userId
     * @param string $token
     *
     * @return JsonResponse
     */
    public function putActivateUserAction($userId, $token)
    {
        /** @var User $user */
        $user = $this->userManager->findUserByConfirmationToken($token);

        $this->cancelActivationForUnauthorizedUser($userId, $user);

        $user->setEnabled(true);
        $user->setConfirmationToken(null);

        $requestedEmailChange = $user->getRequestedEmailChange();

        if (!empty($requestedEmailChange)) {
            // Check if email is taken
            $emailTaken = $this->userManager->findUserByEmail($requestedEmailChange);

            if ($emailTaken instanceof User && $emailTaken->getId() != $user->getId()) {
                throw new UnprocessableEntityHttpException(
                    sprintf('E-mail address "%s" is taken', $emailTaken->getEmail())
                );
            }

            if ($user->getUsername() == $user->getEmail()) {
                $user->setUsername($requestedEmailChange);
            }

            if ($requestedEmailChange != $user->getEmail()) {
                $user->setEmail($requestedEmailChange);
            }

            $user->setRequestedEmailChange(null);
        }

        $this->userRepository->persist($user);

        $data = [
            'success' => true,
            'token'   => $this->jwtManager->create($user),
        ];

        return new JsonResponse(
            $data,
            Response::HTTP_OK
        );
    }

    /**
     * @param int       $userId
     * @param User|null $user
     * @throws ConflictHttpException
     * @throws NotFoundHttpException
     */
    private function cancelActivationForUnauthorizedUser(int $userId, User $user = null)
    {
        if (false === $user instanceof User) {
            $userById = $this->userManager->findUserBy(['id' => $userId]);

            if ($userById instanceof User && $userById->isEnabled()) {
                throw new ConflictHttpException(sprintf('User account #%d is already active.', $userId));
            }
        }

        if (false === $user instanceof User || $user->getId() != $userId) {
            throw new NotFoundHttpException(sprintf('User #%d not found', $userId));
        }
    }

    /**
     * Confirms user e-mail change request. Returns relevant errors if confirmationToken is invalid etc.
     * It also generates a new JSON Web Token after a successful e-mail change confirmation and returns it
     * so the client can update it and read current user data from JWT payload and automatically authenticate
     * user if he haven't had an active session.
     *
     * @ApiDoc(
     *  resource=true,
     *  section="Users",
     *  authentication=false,
     *  description="Confirms user email change request",
     *  tags={
     *      "stable" = "green",
     *      "tested" = "green"
     *  },
     *  requirements={
     *      {"name"="userId", "dataType"="int", "description"="User ID"},
     *      {"name"="token", "dataType"="string", "description"="Confirmation token"}
     *  },
     *  statusCodes={
     *      200="User activated",
     *      404="User not found",
     *      409={
     *          "E-mail change was already confirmed",
     *          "Requested e-mail is the same as current e-mail",
     *          "Conflict - address e-mail already exists"
     *      },
     *      422="This e-mail address is taken"
     *  }
     * )
     *
     * @Put("/users/{userId}/action/confirm-email-change/{token}")
     *
     * @param int    $userId
     * @param string $token
     *
     * @return JsonResponse
     */
    public function putConfirmEmailChangeAction($userId, $token)
    {
        /** @var User $user */
        $user = $this->userManager->findUserByConfirmationToken($token);

        if (false === $user instanceof User || $user->getId() != $userId) {
            throw new NotFoundHttpException(sprintf('User #%d not found', $userId));
        }

        $command = new ConfirmEmailChangeCommand($user, $user->getRequestedEmailChange());

        try {
            $user = $this->commandBus->handle($command);
        } catch (AlreadyConfirmedEmailChangeException $exception) {
            throw new ConflictHttpException('E-mail change was already confirmed');
        } catch (EmailNotChangedEmailChangeException $exception) {
            throw new ConflictHttpException('Requested e-mail is the same as current e-mail');
        } catch (EmailIsTakenEmailChangeException $exception) {
            throw new UnprocessableEntityHttpException('E-mail address is taken');
        } catch (\Exception $exception) {
            throw new InternalServerErrorHttpException();
        }

        $data = [
            'success' => true,
            'token'   => $this->jwtManager->create($user),
        ];

        return new JsonResponse(
            $data,
            Response::HTTP_OK
        );
    }

    /**
     * Used for changing user password or setting a password if user hasn't got one (signed up using
     * Facebook account).
     *
     * @ApiDoc(
     *     resource=true,
     *     section="Users",
     *     authentication=true,
     *     description="Set/change user password",
     *     tags={
     *          "stable" = "green",
     *          "tested" = "green"
     *     },
     *     requirements={
     *          {"name"="userId", "dataType"="int", "requirement"="\d+", "description"="User ID"}
     *     },
     *     parameters={
     *          {"name"="currentPassword", "dataType"="string", "required"=false, "description"="Current password"},
     *          {"name"="newPassword", "dataType"="string", "required"=true, "description"="New password"}
     *     },
     *     statusCodes={
     *          200="Password updated",
     *          400={
     *              "Missing current password (if user account has a password)",
     *              "Missing new password",
     *              "New password doesn't meet the requirements"
     *          },
     *          401="Unauthorized - invalid or no token",
     *          403="Access denied",
     *          404="User with this ID could not be found"
     *     }
     * )
     *
     * @Put("/users/{userId}/action/change-password")
     *
     * @param int $userId
     *
     * @return JsonResponse
     */
    public function putPasswordAction($userId)
    {
        if (!$this->hasRightOrIsOwner($userId)) {
            throw new AccessDeniedHttpException('Access denied.');
        }

        $user = $this->getUserById($userId);

        $request = $this->get('request_stack')->getMasterRequest(); // TODO Dependency injection
        $command = new UserChangePasswordCommand($user, $request->get('newPassword'), $request->get('currentPassword'));

        try {
            $success = $this->commandBus->handle($command);
        } catch (InvalidCurrentPasswordException $exception) {
            $success = false;
        }

        return new JsonResponse(
            [
                'success' => $success,
            ],
            $success ? Response::HTTP_OK : Response::HTTP_UNPROCESSABLE_ENTITY
        );
    }

    /**
     * Client is not required to send all parameters in single request. There are different scenarios like
     * user settings in web app version where settings form is divided in a couple of separate forms - one
     * for e-mail change, one for password change etc. Therefore not all fields are required
     *
     * @ApiDoc(
     *     resource=true,
     *     section="Users",
     *     authentication=true,
     *     description="Update some user data",
     *     tags={
     *          "stable"="green",
     *          "tested"="green"
     *     },
     *     requirements={
     *          {"name"="userId", "dataType"="int", "requirement"="\d+", "description"="User ID"}
     *     },
     *     parameters={
     *          {"name"="email", "dataType"="string", "required"=false, "description"="E-mail address"},
     *          {"name"="searchLocale", "dataType"="string", "required"=false, "description"="Search locale for food searching"},
     *          {"name"="systemInfo", "dataType"="string", "required"=false, "description"="ANDROID|IOS|WINDOWS|UNKNOWN"},
     *          {"name"="systemVersion", "dataType"="string", "required"=false, "description"="Operating system version ex. 6.3"},
     *          {"name"="appVersion", "dataType"="string", "required"=false, "description"="APP Version ex. 2.4.5"}
     *     },
     *     statusCodes={
     *          200="Returned when successful",
     *          400="Data validation error. Details in validation field of JSON response.",
     *          401="Unauthorized - invalid or no token",
     *          403="Access denied",
     *          404="User with this ID could not be found",
     *          422="E-mail address is taken"
     *     }
     * )
     *
     * @Patch("/users/{userId}")
     *
     * @param int $userId
     *
     * @return JsonResponse
     *
     * @throws AccessDeniedHttpException
     * @throws NotFoundHttpException
     */
    public function patchUserAction($userId)
    {
        if (!$this->hasRightOrIsOwner($userId)) {
            throw new AccessDeniedHttpException('Access denied.');
        }

        $user = $this->getUserById($userId);

        $command = new UserUpdateCommand($user);

        $fields = ['email', 'locale', 'storageLocale', 'searchLocale', 'systemInfo', 'systemVersion', 'appVersion', 'timezone', 'nickname'];

        foreach ($fields as $field) {
            $command = $this->setCommandProperty($command, $field);
        }

        // Validate if email is not taken
        if (!empty($command->email)) {
            $emailTaken = $this->userManager->findUserByEmail($command->email);

            if ($emailTaken instanceof User && $emailTaken->getId() != $user->getId()) {
                throw new UnprocessableEntityHttpException('E-mail address is taken');
            }
        }

        $result = $this->commandBus->handle($command);

        return new JsonResponse(
            $this->userService->getUserInfoCached(
                $result,
                $userId,
                $this->getUser()->getId(),
                $this->isGranted('ROLE_SUPER_ADMIN')
            ),
            Response::HTTP_OK
        );
    }

    /**
     * @param UserUpdateCommand $command
     * @param string            $name
     *
     * @return UserUpdateCommand
     */
    private function setCommandProperty(UserUpdateCommand $command, string $name): UserUpdateCommand
    {
        $request = $this->get('request_stack')->getMasterRequest(); // TODO Dependency injection

        if (!is_null($request->get($name))) {
            $command->$name = trim($request->get($name));
            $command->fieldsToUpdate[] = $name;
        }

        return $command;
    }

    /**
     * This endpoint is used for sending an activation link by e-mail. It won't work if user was activated
     * earlier.
     *
     * @ApiDoc(
     *     resource=true,
     *     section="Users",
     *     authentication=true,
     *     description="Re-sends the activation e-mail for an inactive user",
     *     tags={
     *          "stable" = "green",
     *          "tested" = "green"
     *     },
     *     requirements={
     *          {"name"="userId", "dataType"="int", "description"="User ID"},
     *     },
     *     statusCodes={
     *          200="Activation e-mail was sent",
     *          401="Unauthorized - invalid or no token",
     *          403="Access denied",
     *          404="User not found",
     *          422="User is active"
     *     }
     * )
     *
     * @Put("/users/{userId}/action/resend-activation-mail")
     *
     * @param int $userId
     *
     * @return JsonResponse
     */
    public function putResendActivationMailAction($userId)
    {
        if (!$this->hasRightOrIsOwner($userId)) {
            throw new AccessDeniedHttpException('Access denied');
        }

        $user = $this->getUserById($userId);

        if ($user->isEnabled()) {
            throw new UnprocessableEntityHttpException(sprintf('User #%d is already active', $userId));
        }

        // TODO Anti-spam protection

        $event = new UserRequestedActivationMailEvent($user);
        $this->get('event_dispatcher')->dispatch(UserEvents::USER_REQUESTED_ACTIVATION_MAIL, $event); // TODO Dependency injection

        return new JsonResponse(
            [
                'success' => true,
                'email'   => $user->getRequestedEmailChange() ?: $user->getEmail(),
            ],
            Response::HTTP_OK
        );
    }

    /**
     * @param int $userId
     *
     * @return User
     * @throws NotFoundHttpException
     */
    private function getUserById(int $userId): User
    {
        if ($this->isGranted('ROLE_USER') && $this->getUser()->getId() == $userId) {
            return $this->getUser();
        }

        $user = $this->commandBus->handle(new UserGetCommand($userId));

        if (!$user instanceof User) {
            throw new NotFoundHttpException(
                sprintf('User #%d not found', $userId)
            );
        }

        return $user;
    }

    /**
     * Example of response
     *
     *      {
     *          userId: 989628,
     *          sex: 1,
     *          birthDate: "1981-04-10",
     *          height: 183,
     *          heightUnit: "CM",
     *          heightCm: 183,
     *          weightStart: 77,
     *          weightStartUnit: "KG",
     *          weightStartKg: 77,
     *          weightCurrent: 77,
     *          weightCurrentUnit: "KG",
     *          weightCurrentKg: 77,
     *          weightTarget: 70,
     *          weightTargetUnit: "KG",
     *          weightTargetKg: 70,
     *          age: 35,
     *          bmr: 1756
     *      }
     *
     * @ApiDoc(
     *  resource=true,
     *  section="Users",
     *  authentication=true,
     *  description="Get user settings",
     *  tags={
     *      "stable"="green",
     *      "tested"="green",
     *      "cached"="green"
     *  },
     *  requirements={
     *      {"name"="userId", "dataType"="int", "requirement"="\d+", "description"="User ID"}
     *  },
     *  parameters={
     *      {"name"="userId", "dataType"="int", "required"=true, "description"="User ID"},
     *      {"name"="sex", "dataType"="int", "required"=false, "description"="User gender"},
     *      {"name"="birthDate", "dataType"="string", "required"=false, "description"="Birthday date"},
     *      {"name"="height", "dataType"="int", "required"=false, "description"="Height in a given unit"},
     *      {"name"="heightUnit", "dataType"="string", "required"=false, "description"="Height unit"},
     *      {"name"="heightCm", "dataType"="int", "required"=false, "description"="Height in centimetres"},
     *      {"name"="weightStart", "dataType"="int", "required"=false, "description"="Start weight - deprecated"},
     *      {"name"="weightStartUnit", "dataType"="string", "required"=false, "description"="Start weight unit - deprecated"},
     *      {"name"="weightStartKg", "dataType"="int", "required"=false, "description"="Start weight in kilograms - deprecated"},
     *      {"name"="weightCurrent", "dataType"="int", "required"=false, "description"="Current weight"},
     *      {"name"="weightCurrentUnit", "dataType"="string", "required"=false, "description"="Current weight unit"},
     *      {"name"="weightCurrentKg", "dataType"="int", "required"=false, "description"="Current weight in kilograms"},
     *      {"name"="weightTarget", "dataType"="int", "required"=false, "description"="Target weight"},
     *      {"name"="weightTargetUnit", "dataType"="string", "required"=false, "description"="Target weight unit"},
     *      {"name"="weightTargetKg", "dataType"="int", "required"=false, "description"="Target weight in kilograms"},
     *      {"name"="age", "dataType"="int", "required"=false, "description"="User age"},
     *      {"name"="bmr", "dataType"="int", "required"=false, "description"="BMR"}
     *  },
     *  statusCodes={
     *      200="Returned when successful",
     *      401="Unauthorized - invalid or no token",
     *      403="Not authorized to retrieve user data of this user",
     *      404="Not Found - No setting for that user"
     *  }
     * )
     *
     * @param  int $userId
     *
     * @return Response
     *
     * @throws AccessDeniedHttpException
     */
    public function getUserSettingsAction(int $userId)
    {
        if (!$this->hasRightOrIsOwner($userId)) {
            throw new AccessDeniedHttpException('Access denied');
        }

        return new JsonResponse(
            $this->userSettingsService->getFormattedUserSettingsCached($userId),
            Response::HTTP_OK
        );
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  section="Users",
     *  authentication=true,
     *  method="GET",
     *  uri="/api/users/{userId}/userSettingsAndDietSettingsAndUserInfo",
     *  description="Get user settings, diet settings and user info",
     *  tags={
     *      "stable"="green",
     *      "tested"="red",
     *      "cached"="green"
     *  },
     *  requirements={
     *      {"name"="userId", "dataType"="int", "requirement"="\d+", "description"="User ID"}
     *  },
     *  statusCodes={
     *      200="Returned when successful",
     *      401="Unauthorized - invalid or no token",
     *      403="Access denied",
     *      404= {
     *          "User not found",
     *          "Not Found - No setting for that user",
     *      }
     *  }
     * )
     *
     * @Get("/users/{userId}/userSettingsAndDietSettingsAndUserInfo")
     *
     * @param int $userId
     *
     * @return Response
     * @throws AccessDeniedHttpException
     */
    public function getUserSettingsAndDietSettingsAndUserInfoAction(int $userId)
    {
        if (!$this->hasRightOrIsOwner($userId)) {
            throw new AccessDeniedHttpException('Access denied');
        }

        $user = $this->getUserById($userId);

        $response = [
            'userSettings' => $this->userSettingsService->getFormattedUserSettingsCached($userId),
            'dietSettings' => $this->dietSettingsService->getFormattedDietSettingsCached($userId),
            'userInfo'     => $this->userService->getUserInfoCached(
                $user,
                $userId,
                $this->getUser()->getId(),
                $this->isGranted('ROLE_SUPER_ADMIN')
            )
        ];
            $response['subscription'] = $this->subscriptionService->get($userId)->toArray();

        return new JsonResponse(
            $response,
            Response::HTTP_OK
        );
    }


    /**
     * @ApiDoc(
     *  resource=true,
     *  section="Users",
     *  authentication=true,
     *  description="Set user settings",
     *  tags={
     *      "stable"="green",
     *      "tested"="green"
     *  },
     *  requirements={
     *      {"name"="userId", "dataType"="int", "requirement"="\d+", "description"="User ID"},
     *      {"name"="sex", "dataType"="int", "requirement"="\d+", "description"="Sex, 1 - male, 2 - female"},
     *      {"name"="height", "dataType"="int", "description"="User height"},
     *      {"name"="heightUnit", "dataType"="string", "description"="CM, FT_IN, IN"},
     *      {"name"="birthDate", "dataType"="string", "description"="Date of birth"},
     *      {"name"="weightCurrent", "dataType"="double", "description"="Current weight"},
     *      {"name"="weightCurrentUnit", "dataType"="string", "description"="KG, ST_LB, LB"},
     *      {"name"="weightStart", "dataType"="double", "description"="Start weight"},
     *      {"name"="weightStartUnit", "dataType"="string", "description"="KG, ST_LB, LB"},
     *      {"name"="weightTarget", "dataType"="double", "description"="Target weight"},
     *      {"name"="weightTargetUnit", "dataType"="string", "description"="KG, ST_LB, LB"}
     *  },
     *  statusCodes={
     *      200="Successful update of user data",
     *      401="Unauthorized - invalid or no token",
     *      403="Not authorized to retrieve user data of this user"
     *  }
     * )
     *
     * @param  int     $userId
     * @param  Request $request
     *
     * @return Response
     */
    public function putUserSettingsAction($userId, Request $request)
    {
        if (!$this->hasRightOrIsOwner($userId)) {
            throw new AccessDeniedHttpException('Access denied.');
        }

        $userDietSettings = $this->dietSettingsService->getDietSettings($userId);
        $oldUserSettings = $this->userSettingsService->getFormattedUserSettingsCached($userId);

        $userSettingsCommand = new SettingsUserUpdateCommand($userId);
        $userSettingsCommand->sex = $request->get('sex');
        $userSettingsCommand->height = $request->get('height');
        $userSettingsCommand->heightUnit = $request->get('heightUnit');
        $userSettingsCommand->birthDate = $request->get('birthDate');
        $userSettingsCommand->weightCurrent = $request->get('weightCurrent');
        $userSettingsCommand->weightCurrentUnit = $request->get('weightCurrentUnit');
        $userSettingsCommand->weightStart = $request->get('weightStart');
        $userSettingsCommand->weightStartUnit = $request->get('weightStartUnit');
        $userSettingsCommand->weightTarget = $request->get('weightTarget');
        $userSettingsCommand->weightTargetUnit = $request->get('weightTargetUnit');


        if (!is_null($oldUserSettings) && $userSettingsCommand->weightTarget != $oldUserSettings['weightTarget']) {
            $this->userMetaRepository->updateMeta($this->getUser(), UserMetaController::GOAL_ACHIEVEMENT, 'show');
        }

        $userSettings = $this->commandBus->handle($userSettingsCommand);

        $userSettings = $this->objectSerializer->toArrayByGroup($userSettings, 'userSettings');
        unset($userSettings['user']);

        $userSettings['birthDate'] = (new DateTime($userSettings['birthDate']))->format('Y-m-d');

        if ($this->shouldUpdateDietSettings($userDietSettings, $userSettings, $oldUserSettings)) {
            $userDietSettingsCommand = new SettingsDietUpdateCommand($userId);
            $userDietSettingsCommand->activityBase = $userDietSettings['activityBase'];
            $userDietSettingsCommand->activityTraining = $userDietSettings['activityTraining'];
            $userDietSettingsCommand->activityEnergyInclusionMode = $userDietSettings['activityEnergyInclusionMode'];
            $userDietSettingsCommand->activityEnergyInclusionMode = SettingsDiet::activityEnergyInclusionModeLabelToValue(
                $userDietSettings['activityEnergyInclusionMode']
            );
            $userDietSettingsCommand->weightChangeSpeed = $userDietSettings['weightChangeSpeed'];
            $userDietSettingsCommand->weightChangeSpeedUnit = $userDietSettings['weightChangeSpeedUnit'];
            $userDietSettingsCommand->manualEnergyTarget = $userDietSettings['manualEnergyTarget'];
            $userDietSettingsCommand->energy = $userDietSettings['energy'];
            $userDietSettingsCommand->proteinPercentage = $userDietSettings['proteinPercentage'];
            $userDietSettingsCommand->proteinWeight = $userDietSettings['proteinWeight'];
            $userDietSettingsCommand->fatPercentage = $userDietSettings['fatPercentage'];
            $userDietSettingsCommand->fatWeight = $userDietSettings['fatWeight'];
            $userDietSettingsCommand->carbohydratePercentage = $userDietSettings['carbohydratePercentage'];
            $userDietSettingsCommand->carbohydrateWeight = $userDietSettings['carbohydrateWeight'];
            $userDietSettingsCommand->mealSchema = $userDietSettings['mealSchema'];
            $this->commandBus->handle($userDietSettingsCommand);
        }

        $response = new JsonResponse(
            UnitType::formatUnitIdsToLabelsInArray($userSettings),
            Response::HTTP_OK
        );

        return $response;
    }

    /**
     * @param array $userDietSettings
     * @param array $userSettings
     * @param array $oldUserSettings
     * @return bool
     */
    private function shouldUpdateDietSettings(array $userDietSettings, array $userSettings, array $oldUserSettings): bool
    {
        $weightChangeDirection = DietSettingsService::getWeightChangeDirection(
            $userSettings['weightCurrent'],
            $userSettings['weightTarget']
        );

        if ((!is_null($userDietSettings['weightChangeDirection']) && $userDietSettings['weightChangeDirection'] != $weightChangeDirection) ||
            (!is_null($oldUserSettings['birthDate']) && $userSettings['birthDate'] !== $oldUserSettings['birthDate']) ||
            (!is_null($oldUserSettings['sex']) && $userSettings['sex'] !== $oldUserSettings['sex'])
        ) {
            return true;
        }
        return false;
    }

    /**
     * @param int $id
     *
     * @return bool
     */
    private function hasRightOrIsOwner($id)
    {
        if (($this->isGranted('ROLE_USER') && $this->getUser()->getId() == $id) || $this->isGranted('ROLE_ADMIN')) {
            return true;
        }

        return false;
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  section="Authentication",
     *  authentication=false,
     *  description="Send email to given user with reset link",
     *  method="POST",
     *  uri="/api/users/action/reset-password/send-email",
     *  tags={
     *      "stable"="green",
     *      "tested"="red"
     *  },
     *  parameters={
     *      {"name"="email", "dataType"="string", "required"=true, "description"="User email or login"}
     *  },
     *  statusCodes={
     *      200="Successful send email to user",
     *      400="Bad Request",
     *      404="Invalid user data"
     *  }
     * )
     *
     * @Post("/users/action/reset-password/send-email")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function sendEmailOnPasswordResetAction(Request $request)
    {
        $email = $request->get('email');

        /** @var $user UserInterface */
        $userManager = $this->get('fos_user.user_manager'); // TODO Dependency injection
        $user = $userManager->findUserByUsernameOrEmail($email);

        if (empty($user)) {
            return new JsonResponse(
                [
                    'status' => 'error',
                    'msg'    => 'invalid_username',
                ],
                Response::HTTP_NOT_FOUND
            );
        }

        if (empty($email)) {
            return new JsonResponse(
                [
                    'status' => 'error',
                    'msg'    => 'invalid_email',
                ],
                Response::HTTP_NOT_FOUND
            );
        }

        if (null === $user->getConfirmationToken()) {
            /** @var TokenGeneratorInterface $tokenGenerator */
            $tokenGenerator = $this->get('fos_user.util.token_generator'); // TODO Dependency injection
            $user->setConfirmationToken($tokenGenerator->generateToken());
        }

        try {
            $this->get('fos_user.mailer')->sendResettingEmailMessage($user); // TODO Dependency injection
        } catch (EmailSendingException $exception) {
            return new JsonResponse(
                ['message' => 'Could not send e-mail message'],
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }

        $user->setPasswordRequestedAt(new \DateTime());
        $this->get('fos_user.user_manager')->updateUser($user); // TODO Dependency injection

        return new JsonResponse(
            [
                'status' => 'ok',
                'user'   => $user->getId(),
            ],
            Response::HTTP_OK
        );
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  section="Authentication",
     *  authentication=false,
     *  description="Confirm password reset",
     *  method="GET",
     *  uri="/api/users/action/reset-password/token/{token}",
     *  tags={
     *      "stable"="green",
     *      "tested"="red"
     *  },
     *  parameters={
     *      {"name"="token", "dataType"="string", "required"=true, "description"="User confirmation token"}
     *  },
     *  statusCodes={
     *      200="Password reset successful",
     *      400="Bad Request",
     *      404="Invalid user data"
     *  }
     * )
     *
     * @Get("/users/action/reset-password/token/{token}")
     *
     * @param string $token
     *
     * @return JsonResponse
     */
    public function getUserDataForResetAction($token)
    {
        /** @var UserManagerInterface $userManager */
        $userManager = $this->get('fos_user.user_manager'); // TODO Dependency injection

        $user = $userManager->findUserByConfirmationToken($token);

        if (empty($user)) {
            return new JsonResponse(
                [
                    'status' => 'error',
                    'msg'    => 'invalid_username',
                ], Response::HTTP_NOT_FOUND
            );
        }

        return new JsonResponse(
            [
                'status' => 'ok',
                'token'  => $token,
                'user'   => $user->getEmail(),
            ], Response::HTTP_OK
        );
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  section="Authentication",
     *  authentication=false,
     *  description="Confirm password reset",
     *  method="POST",
     *  uri="/api/users/action/reset-password",
     *  tags={
     *      "stable"="green",
     *      "tested"="red"
     *  },
     *  parameters={
     *      {"name"="password", "dataType"="string", "required"=true, "description"="User password"},
     *      {"name"="token", "dataType"="string", "required"=true, "description"="User confirmation token"}
     *  },
     *  statusCodes={
     *      200="Password reset successful",
     *      400="Bad Request",
     *      404="Invalid user data"
     *  }
     * )
     *
     * @Post("/users/action/reset-password")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function changePasswordAction(Request $request)
    {
        $password = $request->get('password');
        $token = $request->get('token');

        if (empty($token)) {
            return new JsonResponse(
                [
                    'status' => 'error',
                    'msg'    => 'invalid_token',
                ], Response::HTTP_NOT_FOUND
            );
        }
        /** @var UserManagerInterface $userManager */
        $userManager = $this->get('fos_user.user_manager'); // TODO Dependency injection

        $user = $userManager->findUserByConfirmationToken($token);

        if (empty($user)) {
            return new JsonResponse(
                [
                    'status' => 'error',
                    'msg'    => 'invalid_username',
                ], Response::HTTP_NOT_FOUND
            );
        }
        $user->setPlainPassword($password);
        $user->setConfirmationToken(null);
        $user->setPasswordRequestedAt(null);
        $userManager->updateUser($user);

        return new JsonResponse(
            [
                'status' => 'ok',
            ], Response::HTTP_OK
        );
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  section="Diet Plan",
     *  method="GET",
     *  authentication=true,
     *  description="Gets user calculation details",
     *  uri="/users/{userId}/calculationDetails",
     *  tags={
     *      "stable"="green",
     *      "tested"="green"
     *  },
     *  statusCodes={
     *      200="OK"
     *  }
     * )
     *
     * @Get("/users/{userId}/calculationDetails")
     *
     * @param int    $userId
     *
     * @return JsonResponse
     */
    public function userCalculationDetailsAction(int $userId)
    {
        $result = $this->userService->getCalculationDetails($userId);

        return new JsonResponse(
            $result,
            Response::HTTP_OK
        );
    }
}
