<?php

namespace Symetria\UserBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Fitatu\SharedUserBundle\Model\Role\RoleInterface;

/**
 * @author    Marcin Budzinski
 * @copyright Fitatu Sp. z o.o.
 */
class AbstractController extends FOSRestController
{
    /**
     * Check user access to private data
     *
     * @param int $userId
     * @return bool
     */
    protected function checkAccess(int $userId): bool
    {
        if (
            ($this->isGranted('ROLE_USER') && $this->getUser()->getId() == $userId)
            || $this->isGranted('ROLE_ADMIN')
        ) {
            return true;
        }

        return false;
    }

    /**
     * @param int $userId
     * @return bool
     */
    public function checkPremiumAccess(int $userId): bool
    {
        if ($this->isGranted(RoleInterface::ROLE_ADMIN)) {
            return true;
        }

        return $this->checkAccess($userId) && $this->isGranted(RoleInterface::ROLE_PREMIUM);
    }
}
