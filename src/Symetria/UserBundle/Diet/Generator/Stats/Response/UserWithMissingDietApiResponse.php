<?php

namespace Symetria\UserBundle\Diet\Generator\Stats\Response;

use JMS\Serializer\Annotation as Serializer;

/**
 * @author    Nikodem Osmialowski
 * @copyright Fitatu Sp. z o.o.
 */
class UserWithMissingDietApiResponse
{
    public const STATUS_UNKNOWN = 0;
    public const STATUS_MISSING_QUEUE = 1;
    public const STATUS_NOT_GENERATED = 2;
    public const STATUS_NOT_ACTIVATED = 3;
    public const STATUS_GENERATION_TOO_LONG = 4;
    private const DATETIME_FORMAT = 'Y-m-d H:i:s';

    private const STATUSES = [
        self::STATUS_UNKNOWN             => 'Unknown status',
        self::STATUS_MISSING_QUEUE       => 'Missing queue record',
        self::STATUS_NOT_GENERATED       => 'Not generated',
        self::STATUS_NOT_ACTIVATED       => 'Not activated',
        self::STATUS_GENERATION_TOO_LONG => 'Generation is taking too long',
    ];

    /**
     * @var int
     * @Serializer\Type("integer")
     * @Serializer\Accessor(getter="getId")
     */
    private $id;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Serializer\Accessor(getter="getUsername")
     */
    private $username;

    /**
     * Locale (pl_PL, en_GB etc.)
     *
     * @var string
     * @Serializer\Type("string")
     * @Serializer\Accessor(getter="getLocale")
     */
    private $locale;

    /**
     * Status description
     *
     * @var int
     * @Serializer\Type("string")
     * @Serializer\Accessor(getter="getStatus")
     */
    private $status;

    /**
     * When was the last request for diet generation placed
     *
     * @var \DateTimeInterface|null
     * @Serializer\Type("string")
     * @Serializer\Accessor(getter="getLastRequestedAt")
     */
    private $lastRequestedAt;

    /**
     * When did the last diet generation process end
     *
     * @var \DateTimeInterface|null
     * @Serializer\Type("string")
     * @Serializer\Accessor(getter="getLastGeneratedAt")
     */
    private $lastGeneratedAt;

    /**
     * Diet generation length in seconds
     *
     * @var int
     * @Serializer\Type("integer")
     * @Serializer\Accessor(getter="getGenerationTimeInSeconds")
     */
    private $generationTimeInSeconds;

    /**
     * When is this plan supposed to be published for user
     *
     * @var \DateTimeInterface|null
     * @Serializer\Type("string")
     * @Serializer\Accessor(getter="getRequestAt")
     */
    private $requestAt;

    /**
     * @param int                     $id
     * @param string                  $username
     * @param string                  $locale
     * @param int                     $status
     * @param \DateTimeInterface|null $lastRequestedAt
     * @param \DateTimeInterface|null $lastGeneratedAt
     * @param \DateTimeInterface|null $requestAt
     */
    public function __construct(int $id, string $username, string $locale, int $status, \DateTimeInterface $lastRequestedAt = null, \DateTimeInterface $lastGeneratedAt = null, \DateTimeInterface $requestAt = null)
    {
        if (!isset(static::STATUSES[$status])) {
            throw new \InvalidArgumentException('Invalid status argument given for '.__METHOD__);
        }

        $this->id = $id;
        $this->username = $username;
        $this->locale = $locale;
        $this->status = $status;
        $this->lastRequestedAt = $lastRequestedAt;
        $this->lastGeneratedAt = $lastGeneratedAt;
        $this->requestAt = $requestAt;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @return string
     */
    public function getLocale(): string
    {
        return $this->locale;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return static::STATUSES[$this->status];
    }

    /**
     * @param \DateTimeInterface|null $date
     *
     * @return string
     */
    private function dateTimeToString(?\DateTimeInterface $date): string
    {
        if (!$date instanceof \DateTimeInterface) {
            return '';
        }

        return $date->format(static::DATETIME_FORMAT);
    }

    /**
     * @return int
     */
    public function getGenerationTimeInSeconds(): int
    {
        if (!$this->lastRequestedAt instanceof \DateTimeInterface) {
            return 0; // TODO Throw?
        }

        $lastGeneratedAtOrNow = ($this->lastGeneratedAt < $this->lastRequestedAt) ? new \DateTime() : $this->lastGeneratedAt;

        return $lastGeneratedAtOrNow->getTimestamp() - $this->lastRequestedAt->getTimestamp();
    }

    /**
     * @return string
     */
    public function getLastRequestedAt(): string
    {
        return $this->dateTimeToString($this->lastRequestedAt);
    }

    /**
     * @return string
     */
    public function getLastGeneratedAt(): string
    {
        return $this->dateTimeToString($this->lastGeneratedAt);
    }

    /**
     * @return string
     */
    public function getRequestAt(): string
    {
        return $this->dateTimeToString($this->requestAt);
    }
}
