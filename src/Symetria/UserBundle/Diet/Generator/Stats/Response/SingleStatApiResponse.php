<?php

namespace Symetria\UserBundle\Diet\Generator\Stats\Response;

use Fitatu\SharedBundle\Model\Date\DateRange;
use JMS\Serializer\Annotation as Serializer;

/**
 * @author    Nikodem Osmialowski
 * @copyright Fitatu Sp. z o.o.
 */
class SingleStatApiResponse
{
    /**
     * Date range presented as an object/array with two properties: from and to
     *
     * @var DateRange
     * @Serializer\Type("array")
     * @Serializer\Accessor(getter="getPeriod")
     */
    private $period;

    /**
     * Number of generated diets in given date range
     *
     * @var int
     * @Serializer\Type("integer")
     * @Serializer\Accessor(getter="getGenerated")
     */
    private $generated;

    /**
     * Number of activated (published) diets in given date range
     *
     * @var int
     * @Serializer\Type("integer")
     * @Serializer\Accessor(getter="getActivated")
     */
    private $activated;

    /**
     * @param DateRange $period
     * @param int       $generated
     * @param int       $activated
     */
    public function __construct(DateRange $period, int $generated, int $activated)
    {
        $this->period = $period;
        $this->generated = $generated;
        $this->activated = $activated;
    }

    /**
     * @return string[]
     */
    public function getPeriod(): array
    {
        $format = 'Y-m-d H:i';

        return [
            'from' => $this->period->getFrom()->format($format),
            'to'   => $this->period->getTo()->format($format),
        ];
    }

    /**
     * @return int
     */
    public function getGenerated(): int
    {
        return $this->generated;
    }

    /**
     * @return int
     */
    public function getActivated(): int
    {
        return $this->activated;
    }
}
