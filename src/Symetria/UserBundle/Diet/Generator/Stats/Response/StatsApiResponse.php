<?php

namespace Symetria\UserBundle\Diet\Generator\Stats\Response;

use JMS\Serializer\Annotation as Serializer;

/**
 * @author    Nikodem Osmialowski
 * @copyright Fitatu Sp. z o.o.
 */
class StatsApiResponse
{
    /**
     * Number of diet plans waiting in generation queue
     *
     * @var int
     * @Serializer\Type("integer")
     * @Serializer\Accessor(getter="getQueued")
     */
    private $queued;

    /**
     * Number of users who should have received their diet plans but did not
     *
     * @var int
     * @Serializer\Type("integer")
     * @Serializer\Accessor(getter="getExpired")
     */
    private $expired;

    /**
     * @var SingleStatApiResponse[]
     * @Serializer\Type("ArrayCollection<Symetria\UserBundle\Diet\Generator\Stats\Response\SingleStatApiResponse>")
     * @Serializer\Accessor(getter="getStats")
     */
    private $stats = [];

    /**
     * @var UserWithMissingDietApiResponse[]
     * @Serializer\Type("ArrayCollection<Symetria\UserBundle\Diet\Generator\Stats\Response\UserWithMissingDietApiResponse>")
     * @Serializer\Accessor(getter="getUsersWithMissingDiet")
     */
    private $usersWithMissingDiet = [];

    /**
     * @param int                              $queued
     * @param int                              $expired
     * @param SingleStatApiResponse[]          $stats
     * @param UserWithMissingDietApiResponse[] $usersWithMissingDiet
     */
    public function __construct(int $queued, int $expired, array $stats, array $usersWithMissingDiet)
    {
        $this->queued = $queued;
        $this->expired = $expired;
        $this->stats = $stats;
        $this->usersWithMissingDiet = $usersWithMissingDiet;
    }

    /**
     * @return int
     */
    public function getQueued(): int
    {
        return $this->queued;
    }

    /**
     * @return int
     */
    public function getExpired(): int
    {
        return $this->expired;
    }

    /**
     * @return SingleStatApiResponse[]
     */
    public function getStats(): array
    {
        return $this->stats;
    }

    /**
     * @return UserWithMissingDietApiResponse[]
     */
    public function getUsersWithMissingDiet(): array
    {
        return $this->usersWithMissingDiet;
    }
}
