<?php

namespace Symetria\UserBundle\Diet\Generator\Stats;

use Fitatu\Local\Repository\Auth\Settings\DietGeneratorRepository;
use Fitatu\Local\Repository\Auth\SubscriptionRepository;
use Fitatu\SharedBundle\Model\Date\DateRange;
use Symetria\UserBundle\Diet\Generator\Stats\Response\SingleStatApiResponse;
use Symetria\UserBundle\Diet\Generator\Stats\Response\StatsApiResponse;
use Symetria\UserBundle\Diet\Generator\Stats\Response\UserWithMissingDietApiResponse;

/**
 * @author    Nikodem Osmialowski
 * @copyright Fitatu Sp. z o.o.
 */
class StatsProvider
{
    private const HOURS_LIMIT = 3;

    /**
     * @var DietGeneratorRepository
     */
    private $dietGeneratorRepository;

    /**
     * @var SubscriptionRepository
     */
    private $subscriptionRepository;

    /**
     * @param DietGeneratorRepository $dietGeneratorRepository
     * @param SubscriptionRepository  $subscriptionRepository
     */
    public function __construct(
        DietGeneratorRepository $dietGeneratorRepository,
        SubscriptionRepository $subscriptionRepository
    ) {
        $this->dietGeneratorRepository = $dietGeneratorRepository;
        $this->subscriptionRepository = $subscriptionRepository;
    }

    /**
     * @return StatsApiResponse
     */
    public function getStats(): StatsApiResponse
    {
        return new StatsApiResponse(
            $this->countQueued(),
            $this->countExpired(),
            $this->getHourlyStats(),
            $this->getUsersWithMissingDiet()
        );
    }

    /**
     * @return int
     */
    private function countQueued(): int
    {
        return $this->dietGeneratorRepository->countQueued();
    }

    /**
     * @return int
     */
    private function countExpired(): int
    {
        return $this->dietGeneratorRepository->countExpired();
    }

    /**
     * @return SingleStatApiResponse[]
     */
    private function getHourlyStats(): array
    {
        $stats = [];

        for ($i = static::HOURS_LIMIT; $i >= 0; --$i) {
            $dateRange = new DateRange(
                $from = $this->getStartingHourDateTime(new \DateTime("-$i hours")),
                (clone $from)->modify('+59 minutes 59 seconds')
            );

            $stats[] = new SingleStatApiResponse(
                $dateRange,
                $this->dietGeneratorRepository->countGeneratedInDateRange($dateRange),
                $this->dietGeneratorRepository->countActivatedInDateRange($dateRange)
            );
        }

        return $stats;
    }

    /**
     * @param \DateTimeInterface $now
     *
     * @return \DateTimeInterface
     */
    private function getStartingHourDateTime(\DateTimeInterface $now): \DateTimeInterface
    {
        $format = 'Y-m-d H:0:0';
        $startingHour = \DateTime::createFromFormat($format, $now->format($format), $now->getTimezone());

        if (version_compare(PHP_VERSION, '7.1.0', '>=')) {
            $startingHour->setTime($now->format('G'), 0, 0, 0);
        } else {
            $startingHour->setTime($now->format('G'), 0, 0);
        }

        return $startingHour;
    }

    /**
     * @return UserWithMissingDietApiResponse[]
     */
    private function getUsersWithMissingDiet(): array
    {
        $users = [];
        $raw = [
            $this->getUsersWithMissingQueueRecord(),
            $this->getUsersWhosePlanGenerationIsTakingTooLong(),
            $this->getUsersWhoShouldAlreadyHaveGeneratedAndActivatedPlan(),
            $this->getUsersWhoShouldAlreadyHaveGeneratedPlan(),
        ];

        foreach ($raw as $rawUsers) {
            foreach ($rawUsers as $userId => $user) {
                if (isset($users[$userId])) {
                    continue;
                }

                $users[$userId] = $user;
            }
        }

        return array_values($users);
    }

    /**
     * @return UserWithMissingDietApiResponse[]
     */
    private function getUsersWithMissingQueueRecord(): array
    {
        $users = $this->subscriptionRepository->findUsersWithSubscriptionAndWithoutDietGeneratorRecord();
        $result = [];

        foreach ($users as $key => $user) {
            $result[$user['userId']] = new UserWithMissingDietApiResponse(
                $user['userId'],
                $user['username'],
                $user['locale'],
                UserWithMissingDietApiResponse::STATUS_MISSING_QUEUE,
                null,
                null,
                null
            );
        }

        return $result;
    }

    /**
     * Retrieves users whose diet generation has already started but is taking too long.
     *
     * @return UserWithMissingDietApiResponse[]
     */
    private function getUsersWhosePlanGenerationIsTakingTooLong(): array
    {
        $users = $this->subscriptionRepository->findUsersWithSubscriptionWhosePlanGenerationIsTakingTooLong();
        $result = [];

        foreach ($users as $user) {
            $result[$user['userId']] = $this->generateUserResponseObject(
                $user,
                UserWithMissingDietApiResponse::STATUS_GENERATION_TOO_LONG
            );
        }

        return $result;
    }

    /**
     * @param array[] $data
     * @param int     $status
     *
     * @return UserWithMissingDietApiResponse
     */
    private function generateUserResponseObject(array $data, int $status): UserWithMissingDietApiResponse
    {
        return new UserWithMissingDietApiResponse(
            $data['userId'],
            $data['username'],
            $data['locale'],
            $status,
            \DateTime::createFromFormat('Y-m-d H:i:s', $data['lastRequestedAt']) ?: null,
            \DateTime::createFromFormat('Y-m-d H:i:s', $data['lastGeneratedAt']) ?: null,
            \DateTime::createFromFormat('Y-m-d H:i:s', $data['requestAt']) ?: null
        );
    }

    /**
     * Retrieves users whose diet plans should have been already generated and activated but were not.
     *
     * @return UserWithMissingDietApiResponse[]
     */
    private function getUsersWhoShouldAlreadyHaveGeneratedAndActivatedPlan(): array
    {
        $users = $this->subscriptionRepository->findUsersWithSubscriptionWhoShouldAlreadyHaveGeneratedAndActivatedPlan();
        $result = [];

        foreach ($users as $user) {
            $result[$user['userId']] = $this->generateUserResponseObject(
                $user,
                UserWithMissingDietApiResponse::STATUS_NOT_ACTIVATED
            );
        }

        return $result;
    }

    /**
     * Retrieves users whose diet plans should have been already generated but were not.
     *
     * @return UserWithMissingDietApiResponse[]
     */
    private function getUsersWhoShouldAlreadyHaveGeneratedPlan(): array
    {
        $users = $this->subscriptionRepository->findUsersWithSubscriptionWhoShouldAlreadyHaveGeneratedPlan();
        $result = [];

        foreach ($users as $user) {
            $result[$user['userId']] = $this->generateUserResponseObject(
                $user,
                UserWithMissingDietApiResponse::STATUS_NOT_GENERATED
            );
        }

        return $result;
    }
}
