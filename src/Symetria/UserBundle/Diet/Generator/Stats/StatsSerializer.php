<?php

namespace Symetria\UserBundle\Diet\Generator\Stats;

use Symetria\UserBundle\Diet\Generator\Stats\Response\StatsApiResponse;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * @author    Nikodem Osmialowski
 * @copyright Fitatu Sp. z o.o.
 */
class StatsSerializer
{
    /**
     * @param StatsApiResponse $stats
     *
     * @return string
     */
    public function serialize(StatsApiResponse $stats): string
    {
        $serializer = new Serializer(
            [new GetSetMethodNormalizer()],
            [new JsonEncoder()]
        );

        return $serializer->serialize($stats, 'json');
    }
}
