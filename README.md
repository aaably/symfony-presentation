Fitatu Api Auth
===============

[Dokumentacja na dokuwiki](http://dokuwiki.dev.fitatu.com/doku.php?id=fitatuapi:apiauth)

Spis treści
-----------

1. [Narzędzia](#narzędzia)
2. [Odpalanie testów](#odpalanie-testów)
3. [Uruchomienie projektu lokalnie przez Docker](#uruchomienie-projektu-lokalnie-przez-docker)

Narzędzia
---------

* [Jenkins (Continuous Integration)](http://217.168.143.190:9999/job/Fitatu%20API%20Planner%20Resources/)
* [SonarQube (analiza statyczna kodu)](http://192.168.1.12:9010/)

Odpalanie testów
----------------

```bash
make spec-test
make unit-test
make functional-test
```

Opcjonalnie, ze zrzucaniem raportów w formatach HTML i Clover:

```bash
make unit-report
make functional-report
```

I raporty + format --tap:

```bash
make unit-report-tap
make functional-report-tap
```

Uruchomienie projektu lokalnie przez Docker
-------------------------------------------

Potrzebne:

* docker
* docker-compose
* docker-machine
* docker-machine-nfs - w przypadku OSX, fix na uprawnienia do katalogów

Pliki konfiguracyjne dla Nginx i PHP znajdują się w katalogu **./docker**.

Odpalanie kontenera w tle:

```bash
docker-compose up -d
```

Jeśli coś nie działa to warto odpalić kontener na pierwszym planie - wówczas wyświetla znacznie więcej informacji:

```bash
docker-compose up
```

Jeśli wszystko zadziałało to projekt zobaczymy pod adresem: [auth.api.fitatu.docker:8080/app_dev.php/doc](http://auth.api.fitatu.docker:8080/app_dev.php/doc)
Domenę należy dopisać do hostów sprawdzając najpierw adres IP naszego kontenera. Można to zrobić tak (dla maszyny o nazwie default): `docker-machine ip default`

Jak już odpalimy dockera to możemy wykonywać pojedyncze polecenia na bashu kontenera. Możemy wykorzystać to do postawienia sobie bazy:

```bash
docker exec -it fitatuapiauth_php56_1 php bin/console doctrine:database:create
docker exec -it fitatuapiauth_php56_1 php bin/console doctrine:schema:create
docker exec -it fitatuapiauth_php56_1 php bin/console fos:user:create
```
